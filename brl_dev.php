<?php

/*
brl_dev::aff_variables();
brl_dev::sizeofvar();
brl_dev::memory_stat();
brl_dev::analytics('', '_all');
brl_dev::analytics($_GET, '_all');
*/
class brl_dev
{
	static function analytics($message, $extfile = '', $verbeux = true) {
		$date = date("Y-m-d H:i:s");
		$nomLogfile = "analytics_$extfile.txt"; // Y-m-d_H-i-s

		if ($_SERVER['REMOTE_ADDR'] == '78.218.222.81') {
			//return false;
		}
		
		$fp = fopen($nomLogfile, 'a');
		if ($verbeux) {
			fwrite($fp, "\n$date [". $_SERVER['REMOTE_ADDR'] . "] REQUEST_URI: " . $_SERVER['REQUEST_URI'] . ' HTTP_USER_AGENT: ' . $_SERVER['HTTP_USER_AGENT'] . ' MESSAGE : ' . print_r($message, true));
		} else {
			fwrite($fp, "\n[$date] : " . print_r($message, true));
		}
		fclose($fp);
	}
	
	// convertion d'un nombre d'octet en kB, MB, GB
	static function convert_SIZE($size)
	{
		$unite = array('B','kB','MB','GB');
		return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unite[$i];
	}

	// liste toutes les variables active et leur taille m�moire 
	static function aff_variables() 
	{
		$return = "";
		global $datas ;
		foreach($GLOBALS as $Key => $Val) {
			if ($Key != 'GLOBALS')  {   
				$return .= ' '. $Key .' � '.brl_dev::sizeofvar( $Val ); // � &asymp;
			}
		}
		return $return;
	}

	//affiche l'empreinte m�moire  d'une variable
	static function sizeofvar($var) 
	{

	  $start_memory = memory_get_usage();   
	  $temp =unserialize(serialize($var ));   
	  $taille = memory_get_usage() - $start_memory;
	  return brl_dev::convert_SIZE($taille) ;
	}

	//affiche des info sur l'espace m�moire du script PHP 
	static function memory_stat()
	{
	   return  'M�moire -- Utilis� : '. brl_dev::convert_SIZE(memory_get_usage(false)) .
	   ' || Allou� : '.
	   brl_dev::convert_SIZE(memory_get_usage(true)) .
	   ' || MAX Utilis�  : '.
	   brl_dev::convert_SIZE(memory_get_peak_usage(false)).
	   ' || MAX Allou�  : '.
	   brl_dev::convert_SIZE(memory_get_peak_usage(true)).
	   ' || MAX autoris� : '.
	   ini_get('memory_limit') ;  ;
	}
}

