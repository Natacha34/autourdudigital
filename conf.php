<?php

if (!defined('_URL_BASE_')) {
	define('_URL_BASE_', ($_SERVER['HTTP_HOST'] == BRLConfig::$http_host_prod || $_SERVER['HTTP_HOST'] == BRLConfig::$http_host_prod2)? BRLConfig::$url_prod : BRLConfig::$url_dev);
}

class BRLConfig {
	public static $url_dev 			= 'localhost/autour_du_digital/';
	public static $url_prod 		= 'https://op1.autour-du-digital.fr/';
	public static $http_host_prod 	= 'op1.autour-du-digital.fr';
	public static $http_host_prod2 	= 'op1.autour-du-digital.fr';
	public static $version      	= "1.0.0";
	public static $bd_name_dev 		= 'projets_dev';
	public static $bd_name_prod 	= 'projets';
	public static $bd_host 			= 'localhost';
	public static $bd_login 		= 'root';
	public static $bd_mdp 			= '';
	public static $dir_dev 			= '/var/www/autour_du_digital/';
	public static $dir_prod			= '/var/www/html/op1/';
	
	public static $nbr_elm_page	= '150';
	
	static function isProd() {
		return ($_SERVER['HTTP_HOST'] == BRLConfig::$http_host_prod || $_SERVER['HTTP_HOST'] == BRLConfig::$http_host_prod2);
	} 
	
	static function getDirGedeas() {
		return ($_SERVER['HTTP_HOST'] == BRLConfig::$http_host_prod || $_SERVER['HTTP_HOST'] == BRLConfig::$http_host_prod2) ? BRLConfig::$dir_prod : BRLConfig::$dir_dev;
	}
}

