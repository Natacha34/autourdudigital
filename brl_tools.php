<?php

/**
 * Class brl_tools | fichier brl_tools.php
 *
 * Description of the class: 
 * - functions that may be useful
 *
 * @package CRM Autour du Digital Project
 * @author Emmanuel Berlese - Jessy and Natacha
 * @copyright BRL entreprise
 * @version Test v1.0
 */

class brl_tools {
	
	/**
	 * static method for parameter text in request
	 */
	static function pSql($string) {
	    $string = addslashes($string);
		$string = trim($string);
		$string = preg_replace('!\s+!', ' ', $string);
	    return $string;
	}
	
	/**
	 * static method for parameter text in request
	 */
	static function pSqlTextarea($string) {
	    $string = addslashes($string);
		$string = trim($string);
	    return $string;
    }
	

	/**
	 * static method who display the list of prestations for a specific devis
	 */
	static function returnTablePrestationsDevis($idDevis) {
		// get the list of devis_prestations with the right id devis
		$lstPrestationsDevis = brl::getLstPrestationsDevis($idDevis);
		// get the max order for position
		$maxOrdrePrestationsDevis = brl::maxOrdrePrestationsDevis($idDevis);
		// echo "<pre>"; print_r($maxOrdrePrestationsDevis);echo "</pre>";exit ;

		$table = '';

		$table .= '<table style="width:100%;margin-top: 50px;"> 
						<tr class="brl-lst-el">
							<th> Ordre </th>
							<th> Type </th>
							<th> Désignation </th>
							<th> Quantité </th>
							<th> Unité </th>
							<th> Prix Unitaire </th>
							<th> Prix HT </th>
							<th> Action </th>
						</tr>';
		foreach ($lstPrestationsDevis as $infoTmp) {
			$table .= '<tr class="brl-lst-el">
						<td class="d-flex">
						<span>'.$infoTmp['ordre'].'</span>';
				if ($infoTmp['ordre'] == 1) {
					$table .= '<span class="up_row_prestationsdevis d-flex justify-content-start" style="margin-right:15px; width:10px"></span>
								<span class="down_row_prestationsdevis d-flex justify-content-end" data-id="'.$infoTmp['id'].'" style="margin-right:15px; cursor: pointer;"><i class="fas fa-sort-down"></i></span>';
				} else if ($infoTmp['ordre'] >= $maxOrdrePrestationsDevis['maxOrdre']) {
					$table .= '<span class="up_row_prestationsdevis d-flex justify-content-start" data-id="'.$infoTmp['id'].'" style="margin-right:15px; cursor: pointer;"><i class="fas fa-sort-up"></i></span>';
				} else {
					$table .= '<span class="up_row_prestationsdevis d-flex justify-content-start" data-id="'.$infoTmp['id'].'" style="margin-right:15px; cursor: pointer;"><i class="fas fa-sort-up"></i></span>
								<span class="down_row_prestationsdevis d-flex justify-content-end" data-id="'.$infoTmp['id'].'" style="margin-right:15px; cursor: pointer;"><i class="fas fa-sort-down"></i></span>';
				}
				$table .= '</td>';
			if ($infoTmp['type']==1) { 
				$table .= '<td>
							H1
						</td>';
			} else if ($infoTmp['type']==2) { 
				$table .= '<td>
							H2
						</td>';
			} else if ($infoTmp['type']==3) { 
				$table .= '<td>
							H3
						</td>';
			} else if ($infoTmp['type']==10) { 
				$table .= '<td>
							C1
						</td>';
			} else if ($infoTmp['type']==11) { 
				$table .= '<td>
							C2
						</td>';
			} else if ($infoTmp['type']==12) { 
				$table .= '<td>
							C3
						</td>';
			} else if ($infoTmp['type']==20) { 
				$table .= '<td>
							R1 €
						</td>';
			} else if ($infoTmp['type']==21) { 
				$table .= '<td>
							R1 %
						</td>';
			} else { 
				$table .= '<td>
							'.$infoTmp['type'].'
						</td>';
			}    
			$table .= '<td>
							'.$infoTmp['titre'].'
						</td>
						<td>
							'.$infoTmp['quantite'].'
						</td>
						<td>
							'.$infoTmp['unite'].'
						</td>
						<td>
							'.$infoTmp['prix_unitaire'].'
						</td>';
						if ($infoTmp['type']==21) { 
				$table .= '<td>
							'.$prixHT = $infoTmp['prix_unitaire']*$infoTmp['quantite'].' %
						</td>';
						} else {
				$table .= '<td>
							'.$prixHT = $infoTmp['prix_unitaire']*$infoTmp['quantite'].' €
						</td>';
						}
				$table .= '<td> 
							<span class="update_row_prestationsdevis" data-id="'.$infoTmp['id'].'" style="color: blue;margin-right:15px; cursor: pointer;"><i class="fas fa-pen"></i></span>
							<span class="delete_row_prestationsdevis" data-id="'.$infoTmp['id'].'" style="color: #ff0727; cursor: pointer;"><i class="fas fa-times-circle"></i></span>
						</td>
					</tr>';
		}

		$table .= '</table>';

		return $table;
	}
	
	/**
	 * static method for return the form of devis_prestations
 	*/
	static function returnFormPrestationsDevis($idPrestationDevis) {
		// get the right infos pour devisprestations
		$infoPrestationsDevis 	= brl::getPrestationsDevis($idPrestationDevis);
		// get the list of prestations
		$lstPrestations 		= brl::LstPrestations();
		// get the max order for the position 
		$maxOrdre 				= brl::maxPrestationsDevis($idPrestationDevis);
		// echo "<pre>"; print_r($maxOrdre);echo "</pre>"; exit ;

		$type			= (!empty($infoPrestationsDevis['selectType'])) ? $infoPrestationsDevis['selectType'] : '';
		$titre			= (!empty($infoPrestationsDevis['titre'])) ? $infoPrestationsDevis['titre'] : '';
		$quantite		= (!empty($infoPrestationsDevis['quantite'])) ? $infoPrestationsDevis['quantite'] : '';
		$unite			= (!empty($infoPrestationsDevis['unite'])) ? $infoPrestationsDevis['unite'] : '';
		$prixUnitaire	= (!empty($infoPrestationsDevis['prix_unitaire'])) ? $infoPrestationsDevis['prix_unitaire'] : '';
		
		$form = '';

		$form .= '<div class="row">
					<div class="inputs_form d-flex">
						<div class="col-md-6 border-right border-secondary" style="margin-top: 20px;">
							<div class="text-center mb-3 font-weight-bold">Prestations sur mesure</div>
								<label>Type :</label> <select name="selectType" id="selectType" class="brl_input" style="width: 270px;">';
								if ($infoPrestationsDevis['type']==1) { 
									$form .= '<option value="1" selected>H1</option>
									<option value="2">H2</option>
									<option value="3">H3</option>
									<option value="10">C1</option>
									<option value="11">C2</option>
									<option value="12">C3</option>
									<option value="20">R1 €</option>
									<option value="21">R1 %</option>'; 
								} else if ($infoPrestationsDevis['type']==2) {
									$form .= '<option value="1">H1</option>
									<option value="2" selected>H2</option>
									<option value="3">H3</option>
									<option value="10">C1</option>
									<option value="11">C2</option>
									<option value="12">C3</option>
									<option value="20">R1 €</option>
									<option value="21">R1 %</option>';
								} else if ($infoPrestationsDevis['type']==3) {
									$form .= '<option value="1">H1</option>
									<option value="2">H2</option>
									<option value="3" selected>H3</option>
									<option value="10">C1</option>
									<option value="11">C2</option>
									<option value="12">C3</option>
									<option value="20">R1 €</option>
									<option value="21">R1 %</option>';
								} else if ($infoPrestationsDevis['type']==10) {
									$form .= '<option value="1">H1</option>
									<option value="2">H2</option>
									<option value="3">H3</option>
									<option value="10" selected>C1</option>
									<option value="11">C2</option>
									<option value="12">C3</option>
									<option value="20">R1 €</option>
									<option value="21">R1 %</option>';
								} else if ($infoPrestationsDevis['type']==11) {
									$form .= '<option value="1">H1</option>
									<option value="2">H2</option>
									<option value="3">H3</option>
									<option value="10">C1</option>
									<option value="11" selected>C2</option>
									<option value="12">C3</option>
									<option value="20">R1 €</option>
									<option value="21">R1 %</option>';
								} else if ($infoPrestationsDevis['type']==12) {
									$form .= '<option value="1">H1</option>
									<option value="2">H2</option>
									<option value="3">H3</option>
									<option value="10">C1</option>
									<option value="11">C2</option>
									<option value="12" selected>C3</option>
									<option value="20">R1 €</option>
									<option value="21">R1 %</option>';
								} else if ($infoPrestationsDevis['type']==20) {
									$form .= '<option value="1">H1</option>
									<option value="2">H2</option>
									<option value="3">H3</option>
									<option value="10">C1</option>
									<option value="11">C2</option>
									<option value="12">C3</option>
									<option value="20" selected>R1 €</option>
									<option value="21">R1 %</option>';
								} else if ($infoPrestationsDevis['type']==21) {
									$form .= '<option value="1">H1</option>
									<option value="2">H2</option>
									<option value="3">H3</option>
									<option value="10">C1</option>
									<option value="11">C2</option>
									<option value="12">C3</option>
									<option value="20">R1 €</option>
									<option value="21" selected>R1 %</option>';
								} else {
									$form .= '<option value="" selected>Choisir une catégorie</option>
									<option value="1">H1</option>
									<option value="2">H2</option>
									<option value="3">H3</option>
									<option value="10">C1</option>
									<option value="11">C2</option>
									<option value="12">C3</option>
									<option value="20">R1 €</option>
									<option value="21">R1 %</option>';
								}
								$form .= '</select><br>
								<label>Désignation :</label> 
								<input type="text" id="titre" name="titre" class="brl_input" style="width: 270px;" placeholder="Titre" value="'.$titre.'" autocomplete="off"><br>
								<label>Quantité :</label> 
								<input type="number" step="0.1" id="quantite" name="quantite" class="brl_input" style="width: 270px;" placeholder="Quantité" value="'.$quantite.'" autocomplete="off"><br>
								<label>Unité :</label> 
								<input type="text" id="unite" name="unite" class="brl_input" style="width: 270px;" placeholder="Unité" value="'.$unite.'" autocomplete="off"><br>
								<label>Prix Unitaire :</label> 
								<input type="number" step="0.1" id="prix_unitaire" name="prix_unitaire" class="brl_input" style="width: 270px;" placeholder="Prix Unitaire" value="'.$prixUnitaire.'" autocomplete="off"><br>';
					if(isset($idPrestationDevis) && !empty($idPrestationDevis)) {
						$form .= '<button class="btn btn-secondary" id="updatePrestationsDevis" data-id="'.$idPrestationDevis.'"><i class="fa fa-plus"></i> Modifier la prestation</button>';
					} else {
						$form .= '<button class="btn btn-secondary" id="insertPrestationsDevis"><i class="fa fa-plus"></i> Ajouter la prestation</button>';
					}
				$form .= '</div>
						<div class="col-md-6 d-flex flex-column justify-content-between" style="margin-top: 20px;">
							<div class="text-center mb-3 font-weight-bold">Prestations pré-remplies</div>
							<div>
								<label>Prestations :</label> <select name="selectPresta" id="selectPresta" class="brl_input" style="width: 280px;">
								<option value="0">Liste des prestations</option>';
						foreach ($lstPrestations as $infoTmp) { 
							$form .= '<option value="'.$infoTmp['id'].'">'.$infoTmp['label'].'</option>';
						}
						$form .= '</select>
							</div>
							<div>
								<button class="btn btn-secondary" id="insertPrestations"><i class="fa fa-plus"></i> Ajouter la prestation</button>
							</div>
						</div>
					</div>
				</div>';

		return $form;
	}

	/**
	 * static method who display the list of detail_prestations for a specific prestation
	 */
	static function returnTableDetailPrestations($idPrestation) {
		$lstDetailPrestations  	= brl::LstDetailPrestations($idPrestation);
		$maxOrdre  				= brl::getMaxOrdreDetailPrestations($idPrestation);
		
		// echo "<pre>"; print_r($maxOrdre);echo "</pre>";exit ;

		$table = '';

		$table .= '<table style="width:100%;margin-top: 50px;"> 
						<tr class="brl-lst-el">
							<th> Ordre </th>
							<th> Type </th>
							<th> Désignation </th>
							<th> Quantité </th>
							<th> Unité </th>
							<th> Prix Unitaire </th>
							<th> Prix HT </th>
							<th> Action </th>
						</tr>';
		foreach ($lstDetailPrestations as $infoTmp) {
			$table .= '<tr class="brl-lst-el">
						<td class="d-flex">
						<span>'.$infoTmp['ordre'].'</span>';
				if ($infoTmp['ordre'] == 1) {
					$table .= '<span class="up_row_detailprestation d-flex justify-content-start" style="margin-right:15px; width:10px"></span>
								<span class="down_row_detailprestation d-flex justify-content-end" data-id="'.$infoTmp['id'].'" style="margin-right:15px; cursor: pointer;"><i class="fas fa-sort-down"></i></span>';
				} else if ($infoTmp['ordre'] == $maxOrdre['maxOrdre']) {
					$table .= '<span class="up_row_detailprestation d-flex justify-content-start" data-id="'.$infoTmp['id'].'" style="margin-right:15px; cursor: pointer;"><i class="fas fa-sort-up"></i></span>';
				} else {
					$table .= '<span class="up_row_detailprestation d-flex justify-content-start" data-id="'.$infoTmp['id'].'" style="margin-right:15px; cursor: pointer;"><i class="fas fa-sort-up"></i></span>
								<span class="down_row_detailprestation d-flex justify-content-end" data-id="'.$infoTmp['id'].'" style="margin-right:15px; cursor: pointer;"><i class="fas fa-sort-down"></i></span>';
				}
				$table .= '</td>';
			if ($infoTmp['type']==1) { 
				$table .= '<td>
							H1
						</td>';
			} else if ($infoTmp['type']==2) { 
				$table .= '<td>
							H2
						</td>';
			} else if ($infoTmp['type']==3) { 
				$table .= '<td>
							H3
						</td>';
			} else if ($infoTmp['type']==10) { 
				$table .= '<td>
							C1
						</td>';
			} else if ($infoTmp['type']==11) { 
				$table .= '<td>
							C2
						</td>';
			} else if ($infoTmp['type']==12) { 
				$table .= '<td>
							C3
						</td>';
			} else if ($infoTmp['type']==20) { 
				$table .= '<td>
							R1 €
						</td>';
			} else if ($infoTmp['type']==21) { 
				$table .= '<td>
							R1 %
						</td>';
			} else { 
				$table .= '<td>
							'.$infoTmp['type'].'
						</td>';
			}    
			$table .= '<td>
							'.$infoTmp['titre'].'
						</td>
						<td>
							'.$infoTmp['quantite'].'
						</td>
						<td>
							'.$infoTmp['unite'].'
						</td>
						<td>
							'.$infoTmp['prix_unitaire'].'
						</td>';
						if ($infoTmp['type']==21) { 
				$table .= '<td>
							'.$prixHT = $infoTmp['prix_unitaire']*$infoTmp['quantite'].' %
						</td>';
						} else {
				$table .= '<td>
							'.$prixHT = $infoTmp['prix_unitaire']*$infoTmp['quantite'].' €
						</td>';
						}
				$table .= '<td> 
							<span class="update_row_detailprestation" data-id="'.$infoTmp['id'].'" style="color: blue;margin-right:15px; cursor: pointer;"><i class="fas fa-pen"></i></span>
							<span class="delete_row_detailprestation" data-id="'.$infoTmp['id'].'" style="color: #ff0727; cursor: pointer;"><i class="fas fa-times-circle"></i></span>
						</td>
					</tr>';
		}

		$table .= '</table>';

		return $table;
	}

	/**
	 * static method for return the form of detail_prestations
 	*/
	static function returnFormDetailPrestations($idDetailPrestation) {
		$InfoDetailPrestations  = brl::getDetailPrestations($idDetailPrestation);
		// echo "<pre>"; print_r($InfoDetailPrestations);echo "</pre>"; exit ;

		$type			= (!empty($InfoDetailPrestations['selectTypePrest'])) ? $InfoDetailPrestations['selectTypePrest'] : '';
		$titre			= (!empty($InfoDetailPrestations['titre'])) ? $InfoDetailPrestations['titre'] : '';
		$quantite		= (!empty($InfoDetailPrestations['quantite'])) ? $InfoDetailPrestations['quantite'] : '';
		$unite			= (!empty($InfoDetailPrestations['unite'])) ? $InfoDetailPrestations['unite'] : '';
		$prixUnitaire	= (!empty($InfoDetailPrestations['prix_unitaire'])) ? $InfoDetailPrestations['prix_unitaire'] : '';
		
		$form = '';

		$form .= '<div class="inputs_form" style="margin-top: 20px;">
					<label>Type :</label> <select name="selectTypePrest" id="selectTypePrest" class="brl_input" style="width: 270px;">';
					if ($InfoDetailPrestations['type']==1) { 
						$form .= '<option value="1" selected>H1</option>
						<option value="2">H2</option>
						<option value="3">H3</option>
						<option value="10">C1</option>
						<option value="11">C2</option>
						<option value="12">C3</option>
						<option value="20">R1 €</option>
						<option value="21">R1 %</option>'; 
					} else if ($InfoDetailPrestations['type']==2) {
						$form .= '<option value="1">H1</option>
						<option value="2" selected>H2</option>
						<option value="3">H3</option>
						<option value="10">C1</option>
						<option value="11">C2</option>
						<option value="12">C3</option>
						<option value="20">R1 €</option>
						<option value="21">R1 %</option>';
					} else if ($InfoDetailPrestations['type']==3) {
						$form .= '<option value="1">H1</option>
						<option value="2">H2</option>
						<option value="3" selected>H3</option>
						<option value="10">C1</option>
						<option value="11">C2</option>
						<option value="12">C3</option>
						<option value="20">R1 €</option>
						<option value="21">R1 %</option>';
					} else if ($InfoDetailPrestations['type']==10) {
						$form .= '<option value="1">H1</option>
						<option value="2">H2</option>
						<option value="3">H3</option>
						<option value="10" selected>C1</option>
						<option value="11">C2</option>
						<option value="12">C3</option>
						<option value="20">R1 €</option>
						<option value="21">R1 %</option>';
					} else if ($InfoDetailPrestations['type']==11) {
						$form .= '<option value="1">H1</option>
						<option value="2">H2</option>
						<option value="3">H3</option>
						<option value="10">C1</option>
						<option value="11" selected>C2</option>
						<option value="12">C3</option>
						<option value="20">R1 €</option>
						<option value="21">R1 %</option>';
					} else if ($InfoDetailPrestations['type']==12) {
						$form .= '<option value="1">H1</option>
						<option value="2">H2</option>
						<option value="3">H3</option>
						<option value="10">C1</option>
						<option value="11">C2</option>
						<option value="12" selected>C3</option>
						<option value="20">R1 €</option>
						<option value="21">R1 %</option>';
					} else if ($InfoDetailPrestations['type']==20) {
						$form .= '<option value="1">H1</option>
						<option value="2">H2</option>
						<option value="3">H3</option>
						<option value="10">C1</option>
						<option value="11">C2</option>
						<option value="12">C3</option>
						<option value="20" selected>R1 €</option>
						<option value="21">R1 %</option>';
					} else if ($InfoDetailPrestations['type']==21) {
						$form .= '<option value="1">H1</option>
						<option value="2">H2</option>
						<option value="3">H3</option>
						<option value="10">C1</option>
						<option value="11">C2</option>
						<option value="12">C3</option>
						<option value="20">R1 €</option>
						<option value="21" selected>R1 %</option>';
					} else {
						$form .= '<option value="0" selected>Choisir une catégorie</option>
						<option value="1">H1</option>
						<option value="2">H2</option>
						<option value="3">H3</option>
						<option value="10">C1</option>
						<option value="11">C2</option>
						<option value="12">C3</option>
						<option value="20">R1 €</option>
						<option value="21">R1 %</option>';
					}
					$form .= '</select><br>
					<label>Désignation :</label> 
					<input type="text" id="titre" name="titre" class="brl_input" style="width: 270px;" placeholder="Titre" value="'.$titre.'" autocomplete="off"><br>
					<label>Quantité :</label> 
					<input type="number" step="0.1" id="quantite" name="quantite" class="brl_input" style="width: 270px;" placeholder="Quantité" value="'.$quantite.'" autocomplete="off"><br>
					<label>Unité :</label> 
					<input type="text" id="unite" name="unite" class="brl_input" style="width: 270px;" placeholder="Unité" value="'.$unite.'" autocomplete="off"><br>
					<label>Prix Unitaire :</label> 
					<input type="number" step="0.1" id="prix_unitaire" name="prix_unitaire" class="brl_input" style="width: 270px;" placeholder="Prix Unitaire" value="'.$prixUnitaire.'" autocomplete="off"><br>';
		if(isset($idDetailPrestation) && !empty($idDetailPrestation)) {
			$form .= '<button class="btn btn-secondary" id="updateDetailPrestation" data-id="'.$idDetailPrestation.'"><i class="fa fa-plus"></i> Modifier la prestation</button>';
		} else {
			$form .= '<button class="btn btn-secondary" id="insertDetailPrestation"><i class="fa fa-plus"></i> Ajouter la prestation</button>';
		}
	$form .= '</div>';

		return $form;
	}
	
	
}



