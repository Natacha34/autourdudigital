<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
//set_time_limit(0);

session_start();
setlocale (LC_ALL, "fr_FR"); 

// the controller require all pages for doing his job
require_once 'conf.php';
require_once 'brl.php';
require_once 'brl_script_maj.php';
require_once 'brl_dev.php';
require_once 'brl_tools.php';
require_once 'brl_bd.php';
require_once 'template.php';

// the action variable is the name of the page
$action 		= (isset($_GET['action']))? $_GET['action'] : '';
// the option variable is the name of the parameter after the name page(namepage/option)
$option 		= (isset($_GET['option']))? $_GET['option'] : '';
$idAccount 		= (isset($_GET['idA']))? $_GET['idA'] : '';
$idCampaign		= (isset($_GET['idC']))? $_GET['idC'] : '';
$date			= (isset($_GET['date']))? $_GET['date'] : '';
$filtre			= (isset($_GET['filtre']))? $_GET['filtre'] : '';
$desi			= (isset($_GET['desi']))? $_GET['desi'] : '';


//echo "<pre>"; print_r($_GET); echo "</pre>"; exit;
//echo "<pre>"; print_r($_SESSION); echo "</pre>";

if ($action == 'contact') {
	displayPageHeader(!empty($_SESSION["USER"]["ID_USER"]));
	displayPageContact();
	displayPageFooter();
	exit;
}
if ($action == 'sendMail') {
	brl_tools::sendMail();
	header("Location:"._URL_BASE_."/");
	exit;
}
if ($action == 'connexion') {
    session_unset();
	$brl = new brl();
	
	if ($brl->connexion(addslashes($_POST['login']), addslashes($_POST['password']))) {
		header("Location:"._URL_BASE_."/");
	} else {
		$_SESSION['msg_erreur'] = "Email ou mot de passe incorrect.";
		header("Location:"._URL_BASE_."/");
	}
	exit;
}
if ($action == 'logout') {
    session_unset();
	header("Location:"._URL_BASE_."/");
	exit;
}


/* LOGIN REQUIRED */
// if the user has an account display all pages bellow
if (! empty($_SESSION["USER"]["ID_USER"])) {	
	if ($action == '...') {
		//...
		exit;
	}
	if ($action == 'displayAjax') {		
		if ($option == "...") {
			// ...
		}		
		exit;
	}

	/* CLIENT REQUIRED */
	$isClient = (isset($_SESSION["USER"]["IS_CLIENT"])) ? $_SESSION["USER"]["IS_CLIENT"] : 0;
	// if the user is a client, give them the correct access
	if ($isClient) {
		
		// if the client go on the page 'suivistemps', display the page header, the page suivis temps and the footer
		if ($action == 'suivistemps') {
			displayPageHeader(true);
			displayPageContenuLstSuivisTemps($option);
			displayPageFooter();
			exit;
		} 
		// display the page header, the right home and the footer
		displayPageHeader(true);
		displayPageContenuAccueilClient();
		displayPageFooter();
		exit;
	}
	
	if ($action == 'suivistemps') {
		displayPageHeader(true);
		displayPageContenuLstSuivisTemps($option);
		displayPageFooter();
		exit;
	} 
	else if ($action == 'users') {
		displayPageHeader(true);
		displayPageContenuUsersComptes($option);
		displayPageFooter();
		exit;
	}
	else if ($action == 'projets') {
		displayPageHeader(true);
		displayPageContenuLstProjets($option);
		displayPageFooter();
		exit;
	} 
	else if ($action == 'prospects') {
		displayPageHeader(true);
		displayPageContenuLstProspects($option);
		displayPageFooter();
		exit;
	} 
	else if ($action == 'clients') {
		displayPageHeader(true);
		displayPageContenuLstClients($option);
		displayPageFooter();
		exit;
	} 
	else if ($action == 'typepresta') {
		displayPageHeader(true);
		displayPageContenuLstTypePresta($option);
		displayPageFooter();
		exit;
	}
	else if ($action == 'typeprojet') {
		displayPageHeader(true);
		displayPageContenuLstTypeProjet($option);
		displayPageFooter();
		exit;
	}
	else if ($action == 'parametrages') {
		displayPageHeader(true);
		displayPageContenuFactures();
		displayPageFooter();
		exit;
	}
	else if ($action == 'prestations') {
		displayPageHeader(true);
		displayPageContenuLstPrestations($option);
		displayPageFooter();
		exit;
	}
	else if ($action == 'devis') {
		displayPageHeader(true);
		displayPageContenuLstDevis($option);
		displayPageFooter();
		exit;
	}    
	
	/* ADMINISTRATOR REQUIRED */
	$is_admin = (isset($_SESSION["USER"]["IS_ADMIN"])) ? $_SESSION["USER"]["IS_ADMIN"] : 0;
	// if (isset($_SESSION["USER"]["IS_ADMIN"])) {
	// if you're an admin, display all pages bellow
	if ($is_admin) {
		if ($action == 'user') {
			displayPageHeader(true);
			displayPageContenuUser($option);
			displayPageFooter();
			exit;
		}
		if ($action == 'usersav') {
			$idAdd = brl::addUser($option);
			header("Location:"._URL_BASE_."/users");
			exit;
		}
		if ($action == 'userrm') {
			brl::rmUser($option, true);
			header("Location:"._URL_BASE_."/users");
			exit;
		}
		if ($action == 'activite') {
			displayPageHeader(true);
			displayPageContenuActivite($option);
			displayPageFooter();
			exit;
		}
		if ($action == 'activitesav') {
			$idAdd = brl::addActivites($option);
			header("Location:"._URL_BASE_."/activites");
			exit;
		}
		if ($action == 'activiterm') {
			brl::rmActivites($option, true);
			header("Location:"._URL_BASE_."/activites");
			exit;
		}
		if ($action == 'projetsvue') {
			displayPageHeader(true);
			displayPageContenuProjets($option);
			displayPageFooter();
			exit;
		}
		if ($action == 'projetssav') {
			$idAdd = brl::addProjet($option);
			header("Location:"._URL_BASE_."/projets");
			exit;
		}
		if ($action == 'projetsrm') {
			brl::rmProjet($option, true);
			header("Location:"._URL_BASE_."/projets");
			exit;
		}
		if ($action == 'prospectsvue') {
			displayPageHeader(true);
			displayPageContenuProspects($option);
			displayPageFooter();
			exit;
		}
		if ($action == 'prospectssav') {
			$idAdd = brl::addProspect($option);
			header("Location:"._URL_BASE_."/prospects");
			exit;
		}
		if ($action == 'prospecttoclient') {
			$idAdd = brl::addProspectToClient($option);
			header("Location:"._URL_BASE_."/clientsvue/".$idAdd);
			exit;
		}
		if ($action == 'prospectsrm') {
			brl::rmProspect($option, true);
			header("Location:"._URL_BASE_."/prospects");
			exit;
		}
		if ($action == 'suiviprospect-add') {
			brl::addProspect($option);
			brl::addSuiviProspect($option);
			echo brl_tools::returnFormSuiviProspect('');
			echo brl_tools::returnTableSuiviProspect($option);
			exit;
		}
		if ($action == 'suiviprospect-update') {
			brl::addProspect($option);
			brl::updateSuiviProspect($_GET['idSuiviProspect']);
			echo brl_tools::returnFormSuiviProspect($option);
			echo brl_tools::returnTableSuiviProspect($option);
			exit;
		}
		if ($action == 'suiviprospect-get') {
			echo brl_tools::returnFormSuiviProspect($option);
			exit;
		}
		if ($action == 'suiviprospect-rm') {
			brl::rmSuiviProspect($option);
			echo brl_tools::returnFormSuiviProspect('');
			echo brl_tools::returnTableSuiviProspect($_GET['idProspect']);
			exit;
		}
		if ($action == 'clientsvue') {
			displayPageHeader(true);
			displayPageContenuClients($option);
			displayPageFooter();
			exit;
		}
		if ($action == 'clientssav') {
			$idAdd = brl::addClients($option);
			header("Location:"._URL_BASE_."/clients");
			exit;
		}
		if ($action == 'clientsrm') {
			brl::rmClients($option, true);
			header("Location:"._URL_BASE_."/clients");
			exit;
		}
		if ($action == 'typeprestavue') {
			displayPageHeader(true);
			displayPageContenuTypePresta($option);
			displayPageFooter();
			exit;
		}
		if ($action == 'typeprestasav') {
			$idAdd = brl::addTypePresta($option);
			header("Location:"._URL_BASE_."/typepresta");
			exit;
		}
		if ($action == 'typeprestarm') {
			brl::rmTypePresta($option, true);
			header("Location:"._URL_BASE_."/typepresta");
			exit;
		}
		if ($action == 'typeprojetvue') {
			displayPageHeader(true);
			displayPageContenuTypeProjet($option);
			displayPageFooter();
			exit;
		}
		if ($action == 'typeprojetsav') {
			$idAdd = brl::addTypeProjet($option);
			header("Location:"._URL_BASE_."/typeprojet");
			exit;
		}
		if ($action == 'typeprojetrm') {
			brl::rmTypeProjet($option, true);
			header("Location:"._URL_BASE_."/typeprojet");
			exit;
		}
		if ($action == 'suivitemps') {
			displayPageHeader(true);
			displayPageContenuSuiviTemps($option);
			displayPageFooter();
			exit;
		}
		if ($action == 'suivitempssav') {
			$idAdd = brl::addSuiviTemps($option);
			header("Location:"._URL_BASE_."/suivistemps");
			exit;
		}
		if ($action == 'suivitempsrm') {
			brl::rmSuiviTemps($option, true);
			header("Location:"._URL_BASE_."/suivistemps");
			exit;
		}
		// if the page is 'facturessav', call static function in class brl
		if ($action == 'facturessav') {
			brl::savConfiguration();
			header("Location:"._URL_BASE_."/parametrages");
			exit;
		}
		if ($action == 'prestationsvue') {
			displayPageHeader(true);
			displayPageContenuPrestations($option);
			displayPageFooter();
			exit;
		}
		// if the page is 'prestationssav', call static function in class brl
		if ($action == 'prestationssav') {
			$idAdd = brl::addPrestations($option);
			header("Location:"._URL_BASE_."/prestations");
			exit;
		}
		// if the page is 'prestationsrm', call static function in class brl
		if ($action == 'prestationsrm') {
			brl::rmPrestations($option, true);
			header("Location:"._URL_BASE_."/prestations");
			exit;
		}
		if ($action == 'devisvue') {
			displayPageHeader(true);
			displayPageContenuDevis($option);
			displayPageFooter();
			exit;
		}
		// generate devis
		if ($action == 'devisgenerate') {
			displayPageGenerateDevis($option);
			exit;
		}
		if ($action == 'devissav') {
			$idAdd = brl::addDevis($option);
			header("Location:"._URL_BASE_."/devis");
			exit;
		}
		if ($action == 'devisrm') {
			brl::rmDevis($option, true);
			header("Location:"._URL_BASE_."/devis");
			exit;
		}
		if ($action == 'prestationsdevis-add') {
			brl::addDevis($option);
			brl::addPrestationsDevis($option);
			echo brl_tools::returnTablePrestationsDevis($option);
			echo brl_tools::returnFormPrestationsDevis('');
			exit;
		}
		if ($action == 'prestations-add') {
			brl::getRightPrestations($option);
			echo brl_tools::returnTablePrestationsDevis($option);
			echo brl_tools::returnFormPrestationsDevis('');
			exit;
		}
		if ($action == 'prestationsdevis-update') {
			brl::addDevis($option);
			brl::updatePrestationsDevis($_GET['idPrestationDevis']);
			echo brl_tools::returnTablePrestationsDevis($option);
			echo brl_tools::returnFormPrestationsDevis($option);
			exit;
		}
		if ($action == 'prestationsdevis-get') {
			echo brl_tools::returnFormPrestationsDevis($option);
			exit;
		}
		if ($action == 'prestationsdevis-rm') {
			brl::rmPrestationsDevis($option);
			echo brl_tools::returnTablePrestationsDevis($_GET['idDevis']);
			echo brl_tools::returnFormPrestationsDevis('');
			exit;
		}
		
	} 
		// Accueil
	displayPageHeader(true);
	displayPageContenuAccueil();
	displayPageFooter();
	exit;
} else if ($action == 'displayAjax' || $action == 'getWidget') {
	echo "logout";
	exit;
}

displayPageHeader(false);
displayPageConnexion();
displayPageFooter();
exit;



