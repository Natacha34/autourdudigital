<?php

/**
 * Class brl_bd | fichier brl_bd.php
 *
 * Description of the class: 
 * - make the link between php and bdd.
 *
 * @package CRM Autour du Digital Project
 * @author Emmanuel Berlese 
 * @copyright BRL entreprise
 * @version Test v1.0
 */
class brl_bd {
	
	public $con_mysqli = '';
	private $result = '';

	function __construct() {
		$BD_host	= BRLConfig::$bd_host;
		$BD_login	= BRLConfig::$bd_login;
		$BD_mdp		= BRLConfig::$bd_mdp;
		$BD_name	= BRLConfig::isProd() ? BRLConfig::$bd_name_prod : BRLConfig::$bd_name_dev;
		$this->con_mysqli = mysqli_connect($BD_host, $BD_login, $BD_mdp, $BD_name);
	}

	function query($sql_query) {

		$this->result = mysqli_query($this->con_mysqli, $sql_query) or die (mysqli_error($this->con_mysqli));
		return $this->result;
	}

	function query_use_result($sql_query) {
		
		$this->result = mysqli_query($this->con_mysqli, $sql_query, MYSQLI_USE_RESULT) or die (mysqli_error($this->con_mysqli));
		return $this->result;
	}

	function close_result() {
	    $this->result->close();
	}

	function query_elm($sql_query) {
		
		$this->result = mysqli_query($this->con_mysqli, $sql_query) 
			or die (mysqli_error($this->con_mysqli));

		return mysqli_fetch_assoc($this->result);
	}

	function query_lst($sql_query, $cle = '') {
		
		$this->result = mysqli_query($this->con_mysqli, $sql_query) 
			or die (mysqli_error($this->con_mysqli));
		$lstResult = array();
		while ($row = mysqli_fetch_assoc($this->result)) {
			if (empty($cle)) {
				$lstResult[] = $row;
			} else {
				$id = $row[$cle];
				$lstResult[$id] = $row;
			}
		}
		
		return $lstResult;
	}
	
	function query_get_last_line() {
		return mysqli_fetch_assoc($this->result); // mysqli_fetch_row
	}

	function num_rows() {
		return $this->result->num_rows;
	}

	function getInsertId() {
		return mysqli_insert_id($this->con_mysqli);
	}
	
}