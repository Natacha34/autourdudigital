<?php

// function who display the header page with conditions
function displayPageHeader ($isConnected) {
	// verification if the user is an admin, a super admin or a client
	$isAdmin = (isset($_SESSION["USER"]["IS_ADMIN"])) ? $_SESSION["USER"]["IS_ADMIN"] : 0;
	$is_superAdmin = (isset($_SESSION["USER"]["IS_SUPER_ADMIN"])) ? $_SESSION["USER"]["IS_SUPER_ADMIN"] : 0;
	$isClient = (isset($_SESSION["USER"]["IS_CLIENT"])) ? $_SESSION["USER"]["IS_CLIENT"] : 0;

	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xml:lang="fr-fr" lang="fr-fr" dir="ltr" >
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Projets - Autour du digital</title>
		<meta name="author" content="Ludivine" />
		<meta name="description" content="" />
		<meta name="keywords"  content="" />
		<meta name="keywords"  content="" />
		<meta http-equiv="robots" name="Robots" content="all" />
		<meta name="robots" content="noindex,nofollow" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" type="image" href="/images/favicon.ico">
		
		<!-- jquery -->
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		
		<!-- Bootstrap -->
		<link rel="stylesheet" href="<?php echo _URL_BASE_; ?>/bootstrap/css/bootstrap.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo _URL_BASE_; ?>/bootstrap/css/bootstrap-theme.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo _URL_BASE_; ?>/bootstrap/css/bootstrap-select.css" type="text/css" />
		<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
		
		
		<link rel="stylesheet" href="<?php echo _URL_BASE_; ?>/font-awesome-5.3.1/css/all.min.css">
		<link rel="stylesheet" href="<?php echo _URL_BASE_; ?>/css/global.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo _URL_BASE_; ?>/css/print.css" type="text/css" media="print" />
		
		<!-- LIEN -->
		<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php echo _URL_BASE_; ?>/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="<?php echo _URL_BASE_; ?>/bootstrap/js/bootstrap-select.js"></script>
		<script type="text/javascript" src="<?php echo _URL_BASE_; ?>/js/datepicker-fr.js"></script>
		<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
		
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	
		<script type="text/javascript">
            google.charts.load('current', {'packages':['corechart']});
        </script>
		<!-- Include Required Prerequisites -->
		<!--script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script-->
		<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		 
		<!-- Include Date Range Picker -->
		<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
		<script type="text/javascript">
			google.charts.load('current', {'packages':['bar', 'corechart']});
		</script>
		<script type="text/javascript">
			var base_URL = "<?php echo _URL_BASE_; ?>" ;
		</script>
		
		<script type="text/javascript" src="<?php echo _URL_BASE_; ?>/js/global.js?1.0.1"></script>
		<script type="text/javascript" src="<?php echo _URL_BASE_; ?>/js/validator.js"></script>
		
	</head>
	<body id="body">
	
<!-- if the user isn't connected, display the connexion -->
<?php if (! $isConnected) : ?>
<aside class="sidebar">
	<nav id="navigation" role="navigation">
		<a href="<?php echo _URL_BASE_; ?>/" class="actif"><i class="fa fa-sign-in"></i> Connexion</a>
		<br>
		<a href="<?php echo _URL_BASE_; ?>/contact"><i class="fa fa-envelope"></i> Contact</a>
		<a href="http://www.brl.fr"><i class="fa fa-link"></i> www.brl.fr</a> <!-- changer nom contact -->
	</nav>
</aside>
<!-- else if the user is connected as an admin or a super admin, display the right navigation -->
<?php elseif ($isAdmin || $is_superAdmin) : ?>
<aside class="sidebar">
	<nav id="navigation" role="navigation">
		<a href="<?php echo _URL_BASE_; ?>/"><i class="fas fa-home"></i> Accueil</a>
		<a href="<?php echo _URL_BASE_; ?>/suivistemps"><i class="fas fa-stopwatch"></i> Suivi Temps</a>
		<a href="<?php echo _URL_BASE_; ?>/projets"><i class="fas fa-briefcase"></i> Projets</a>
		<a href="<?php echo _URL_BASE_; ?>/clients"><i class="fas fa-user-alt"></i> Clients</a>
		<a href="<?php echo _URL_BASE_; ?>/prospects"><i class="fas fa-user-alt"></i> Prospects</a>
		<a href="<?php echo _URL_BASE_; ?>/prestations"><i class="fas fa-gem"></i> Prestations</a>
		<a href="<?php echo _URL_BASE_; ?>/devis"><i class="fas fa-file-alt"></i> Devis</a>
		<br>
		<a href="<?php echo _URL_BASE_; ?>/typepresta"><i class="fas fa-star"></i> Type de prestation</a>
		<a href="<?php echo _URL_BASE_; ?>/typeprojet"><i class="fas fa-star"></i> Type de projet</a>
		<br>
		<a href="<?php echo _URL_BASE_; ?>/typeprojet"><i class="fas fa-clipboard"></i> Modèle de devis</a>
		<br>
		<a href="<?php echo _URL_BASE_; ?>/users"><i class="fa fa-users"></i> Utilisateurs</a>
		<a href="<?php echo _URL_BASE_; ?>/parametrages"><i class="fa fa-hourglass"></i> Paramétrages</a>
		<br>
		<a href="<?php echo _URL_BASE_; ?>/logout"><i class="fas fa-sign-out-alt"></i> Déconnexion</a>
        <br>
        <div style="color:white;">
            <!--Version <?php echo BRL_ScriptMAJ::getVersion(true);?>-->
            &nbsp;Version <?php echo BRLConfig::$version;?>
            
        </div>
	</nav>
</aside>
<!-- else if the user is connected as a client, display the right navigation -->
<?php elseif ($isClient) : ?>
<aside class="sidebar">
	<nav id="navigation" role="navigation">
		<a href="<?php echo _URL_BASE_; ?>/"><i class="fas fa-home"></i> Accueil</a>
		<a href="<?php echo _URL_BASE_; ?>/suivistemps"><i class="fas fa-stopwatch"></i> Suivi Temps</a>
		<br>
		<a href="<?php echo _URL_BASE_; ?>/logout"><i class="fas fa-sign-out-alt"></i> Déconnexion</a>
        <br>
        <div style="color:white;">
            <!--Version <?php echo BRL_ScriptMAJ::getVersion(true);?>-->
            &nbsp;Version <?php echo BRLConfig::$version;?>
            
        </div>
	</nav>
</aside>
<?php endif; ?>


<div class="wrapper">

	<div id="sidebar-button">
		<a href="#body" class="nav-button-open button-4" aria-label="open navigation"></a>
		<a href="#" class="nav-button-close button-4 actif" aria-label="close navigation"></a>
	</div>
	
		<div class="<?php echo BRLConfig::isProd()? "" : "site-dev"; ?>" id="cover-background">
		</div>
		
		<div id="page">
	
		
			<div id="menu-top">
			</div>
		
			<div id="logo">
				<a href="/"><img src="<?php echo _URL_BASE_; ?>/images/logo-autour-du-digital.png" alt="logo" /></a>
			</div>
		
			<!--div id="menu">
				<ul>
					<li><a href="#">Accueil</a></li>
					<li><a href="#">Annuaire</a></li>
					<li><a href="#">Annonces d'emploi</a></li>
					<li><a href="#">Documentations</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
				
			</div-->
			
			<div id="contenu">
<?php

	if (! empty($_SESSION['msg_confirmation'])) {
?>
				<div class="bloc-centre-50 bloc-vert centre">
					<?php echo $_SESSION['msg_confirmation']; ?>
				</div>
		
<?php
		$_SESSION['msg_confirmation'] = "";
	}
	if (! empty($_SESSION['msg_erreur'])) {
?>
				<div class="bloc-centre-50 bloc-rouge centre">
					<?php echo $_SESSION['msg_erreur']; ?>
				</div>
		
<?php
		$_SESSION['msg_erreur'] = "";
	}

}

function displayPageConnexion () {
	
?>
				<div class="brl-panel-bloc50" style="max-width: 600px;
    text-align: center;
    margin: 60px auto; 
    padding: 0px;
    position: initial;
    display: block;
    float: none;">
				
					<div class="titre-bloc-b">
						<div class="brl-title">Connexion</div>
					</div>
					<div id="curve_chart" style="width: 100%;">
					
						<form method="post" action="<?php echo _URL_BASE_; ?>/connexion" name="connexion" style="padding: 20px 0;">
							<input type="text" name="login" value="" class="brl_input" placeholder="login">
							<input type="password" name="password" value="" class="brl_input" placeholder="************">
							
							<div>
								<a class="fancybox fancybox.ajax" href="contact" style="text-decoration: none; padding-bottom: 20px; font-size: 13px; display: block;">Mot de passe oublié ?</a>
								
								<div class="bouton2 clear" style="text-align: center;">
									<a href="javascript:submitform();">SE CONNECTER</a>
								</div>
								<input type="submit" style="display:none">
							</div>
							<script type="text/javascript">
							function submitform()
							{
							  document.connexion.submit();
							}
							</script>
						</form>
					</div>
				</div>
				
				
				<!--div class="bloc-centre-50">
							<h2>Connexion</h2>
				</div-->
<?php

}

// Home Client with a simple message
function displayPageContenuAccueilClient () { ?>
	<div class="brl-panel-bloc100">
		<div class="brl-row left" style="text-align: center; padding-top: 40px;">
			<i class="fa fa-quote-left fa-2x" style="padding-right: 10px;"></i> Bienvenue sur votre espace client "De base de données"
			<p>&nbsp;</p>
			<div class="bloc-signature">&nbsp;</div>
		</div>
	</div>
<?php }

// Accueil Admin
function displayPageContenuAccueil () {
	// On récupère les stats
	$infoCloud = brl::getCloudInfo();
	$nbreClients = brl::statsClients();
	$nbreProjets = brl::statsProjets();
	$tempsSuiviTemps = brl::statsSuiviTemps();
	$nbreSuiviTemps = brl::getCompteurSuiviTemps('');

	// On récupère les projets suivis
	$lstSuiviProjets = brl::getLstSuiviProjets('1');
	// On récupère les projets non suivis
	$lstNonSuiviProjets = brl::getLstSuiviProjets('0');

	// On récupère les prospects à relancer (par statut)
	$lstProspectsRelanceDevisAFaire = brl::getLstProspectsRelanceByStatut(1); # DEVIS À FAIRE
	$lstProspectsRelanceAttenteRetourDevis = brl::getLstProspectsRelanceByStatut(2); # ATTENTE RETOUR DEVIS
	$infoDuUserCloud = brl::getLstDuUserCloud();
?>
				<!-- <div class="brl-panel-bloc100">
					<div class="brl-row left" style="text-align: center; padding-top: 40px;">
						<i class="fa fa-quote-left fa-2x" style="padding-right: 10px;"></i> Bienvenue sur l'outils "De base de données"
						<p>&nbsp;</p>
						<div class="bloc-signature">&nbsp;</div>
					</div>
				</div> -->
				
				
				<div id="idDivGlobal" class="displayNone" style="display: block;">
					<div class="brl-panel-bloc100 displayNone" id="widget_1564742" style="display: inline-block; opacity: 1;">
						<div class="brl-row left">
							<div class="brl-col-5">
								<span>&nbsp;</span>
								<div class="brl-tx-s3"><?php echo $nbreClients['nbrElm']; ?></div>
								<span>Clients</span>
							</div>
							<div class="brl-col-5">
								<span>&nbsp;</span>
								<div class="brl-tx-s3"><?php echo $nbreProjets['nbrElm']; ?></div>
								<span>Projets</span>
							</div>
							<div class="brl-col-5 brl-title">
								<span class="brl-title">&nbsp;</span>
								<div class="brl-tx-s3"><?php echo $nbreSuiviTemps['nbrElm']; ?></div>
								<span>Suivi Temps</span>
							</div>
							<div class="brl-col-5">
								<span>&nbsp;</span>
								<div class="brl-tx-s3"><?php echo round(($tempsSuiviTemps['tempsTotal'] / 60 / 24), 2) . ' jours'; ?></div>
								<span>Non facturé</span>
							</div>
							<div class="brl-col-5 brl-title">
								<span class="brl-title">Autour du cloud</span>
								<div class="brl-tx-s3" style="color:<?php echo $infoCloud['colorUse']; ?>;"><?php echo $infoCloud[0]; ?> (<?php echo $infoCloud[3]; ?>)</div>
								<span><?php echo $infoCloud[4]; ?></span>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="brl-panel-bloc33 main">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><i class="fa fa-folder-open"></i> <a href="/projets" style="color: white;">Suivi Projets</a></div>
						</div>
						<div style="padding: 15px;">
							<?php
							foreach ($lstSuiviProjets as $infoTmp) {
								$tempsPasse = brl::nbreMinutesProjet($infoTmp['id']);
								$client = brl::getClients($infoTmp['id_client']);
							?>
								<div class="row">
									<div class="col-md-8">
										<p><?php echo /*'<a href="clientsvue/'.$client['id'].'" target="_blank">' . $client['nom'] . '</a> - ' . */ '<a href="projetsvue/'.$infoTmp['id'].'" target="_blank">' . $infoTmp['nom'] . '</a>'; ?></p>
									</div>
									<div class="col-md-4">
										<p><?php echo round($tempsPasse['tempsTotal'] / 60, 2) . ' / ' . $infoTmp['nbre_heure_estime'] . 'h'; ?></p>
									</div>
									<div class="col-md-12">
										<div class="progress">
											<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo (round($tempsPasse['tempsTotal'] / 60, 2) /  $infoTmp['nbre_heure_estime']) * 100; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo (round($tempsPasse['tempsTotal'] / 60, 2) /  $infoTmp['nbre_heure_estime']) * 100; ?>%">
											<?php echo round((round($tempsPasse['tempsTotal'] / 60, 2) /  $infoTmp['nbre_heure_estime']) * 100, 0) . '%'; ?>
											</div>
										</div>
										<?php if (! empty($infoTmp['prct_avancement'])) { ?>
										<div class="progress">
											<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $infoTmp['prct_avancement']; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $infoTmp['prct_avancement']; ?>%">
											<?php echo $infoTmp['prct_avancement'] . '% d\'avancement'; ?>
											</div>
										</div>
										<?php } ?>
									</div>
								</div>
							<?php 
							} 
							?>
						</div>
					</div>

					<div style="margin-top: 25px;">
						<div class="titre-bloc-b">
							<div class="brl-title"><i class="fa fa-folder-open"></i> Suivi MAJ</div>
						</div>
						<div style="padding: 15px;">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-6">
											<p><a href="https://developer.joomla.org/security-centre.html" target="_blank">Joomla Security Centre</a></p>
											<p><a href="https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=Joomla&search_type=all&orderBy=publishDate&orderDir=desc&aliaspath=%2fvuln%2fSearch%2fResults" target="_blank">NVD Joomla flux</a></p>
											<p><a href="https://www.cvedetails.com/vulnerability-list/vendor_id-3496/Joomla.html" target="_blank">CVE Joomla flux</a></p>
											<p><a href="https://www.exploitalert.com/search-results.html?search=joomla" target="_blank">Exploit Alert Joomla flux</a></p>
										</div>
										<div class="col-md-6">
											<p><a href="https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=Prestashop&search_type=all" target="_blank">NVD Prestashop flux</a></p>
											<p><a href="https://www.cvedetails.com/vulnerability-list/vendor_id-8950/Prestashop.html" target="_blank">CVE Prestashop flux</a></p>
											<p><a href="https://www.exploitalert.com/search-results.html?search=prestashop" target="_blank">Exploit Alert Prestashop flux</a></p>
										</div>
										<div class="col-md-12">
										<p>
										</p>
										<hr>
										</div>
										
									</div>
									<?php 
										//  paramètre pour récupérer un id_client en particulier
										$idClient = (isset($_SESSION['USER']['ID_CLIENTS'])) ? $_SESSION['USER']['ID_CLIENTS'] : '';
										$lstProjets = brl::getLstingProjets(true, $idClient);

										$JM = brl::lastReleaseJoomla();
										$Ps16 = brl::lastReleasePS16();
										$Ps17 = brl::lastReleasePS17();
										$lastVersionToUpgradeJM = brl_tools::getVersionJoomlaFromXML('https://developer.joomla.org/security-centre.feed?type=rss');
										$lastVersionToUpgradeJM = str_replace('</p>','',$lastVersionToUpgradeJM);
										echo '<div class="row">
												<div class="col-md-4">
													<p>'.$JM.'</p>
													<p></p>
												</div>
												<div class="col-md-4">
													<p>'.$Ps16.'</p>
												</div>
												<div class="col-md-4">
													<p>'.$Ps17.'</p>
												</div>
												<div class="col-md-12">
												</div>
											</div>';
										foreach($lstProjets as $projet) {
											if(!empty($projet['report'])) {

												echo '<div class="row">
															<div class="col-md-6">
																<p><a href="'.$projet['lien_bo'].'" target="_blank">'.$projet['nom'].'</a></p>
															</div>';

												$variablesReport = brl_tools::getContentFileByCurl($projet['report']);
												$arrayVarReport = explode(" ||| ", $variablesReport);
												$PSOrJMVersion = $arrayVarReport[0];
												if(isset($arrayVarReport[1])) {
													$versionPHP = $arrayVarReport[1];
												} else {
													$versionPHP = '/';
												}
												$findme   = 'Joomla';
												$pos = strpos($PSOrJMVersion, $findme);

												if('7.2' < $versionPHP) {
													$show_version_color = 'green';
												} else {
													$show_version_color = 'red';
												}

												if ($pos === false) {
													if($JM == $PSOrJMVersion || $PSOrJMVersion == $Ps16 || $PSOrJMVersion == $Ps17 ) {
														echo '<div class="col-md-3">
																<p style="color: green">'.$PSOrJMVersion.'</p>
															</div>';
													} else {
														echo '<div class="col-md-3">
																<p style="color: red">'.$PSOrJMVersion.'</p>
															</div>';
													}
												} else {
													$PSOrJMVersionNumber = str_replace('Joomla ', '', brl_tools::getContentFileByCurl($projet['report']));
													if($JM == $PSOrJMVersion || $PSOrJMVersion == $Ps16 || $PSOrJMVersion == $Ps17 ) {
														echo '<div class="col-md-3">
																<p style="color: green">'.$PSOrJMVersion.'</p>
															</div>';
													} elseif($PSOrJMVersionNumber < $lastVersionToUpgradeJM) {
														echo '<div class="col-md-3">
																<p style="color: red">'.$PSOrJMVersion.'</p>
															</div>';
													} else {
														echo '<div class="col-md-3">
																<p style="color: orange">'.$PSOrJMVersion.'</p>
															</div>';
													}
												}
												echo '<div class="col-md-3">
														<p style="color: '.$show_version_color.'">PHP '. $versionPHP .'</p>
													</div>
												</div>';
											}
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="brl-panel-bloc33 main">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><i class="fa fa-folder-open"></i> <a href="/suivistemps" style="color: white;"> Suivi Temps clients</a></div>
							<div class="bouton3 clear" style="float: right;">
								<a href="https://op1.autour-du-digital.fr//suivitemps/0#contenu" style="height: 100%;margin-top: 5px"><i class="fa fa-plus bleu" aria-hidden="true"></i></a>
							</div>
						</div>
						<div style="padding: 15px;">
							<?php
							foreach ($lstNonSuiviProjets as $infoTmp) {
								$tempsPasse = brl::nbreMinutesProjet($infoTmp['id']);
								$client = brl::getClients($infoTmp['id_client']);
								if(round($tempsPasse['tempsTotal'] / 60, 2) > 0) {
							?>
								<div class="row">
									<div class="col-md-8">
										<p><?php echo '<a href="clientsvue/'.$client['id'].'" target="_blank">' . $client['nom'] . '</a> - <a href="projetsvue/'.$infoTmp['id'].'" target="_blank">' . $infoTmp['nom'] . '</a>'; ?></p>
									</div>
									<div class="col-md-4">
										<p><?php echo round($tempsPasse['tempsTotal'] / 60, 2). ' h'; ?></p>
									</div>
								</div>
							<?php 
								}
							} 
							?>
						</div>
					</div>
				</div>

				<div class="brl-panel-bloc33 main">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><i class="fa fa-folder-open"></i> <a href="/prospects" style="color: white;"> Relance Prospects</a></div>
						</div>
						<div style="padding: 15px;">
							<?php
							foreach ($lstProspectsRelanceDevisAFaire as $infoTmp) {
							?>
								<div class="row">
									<div class="col-md-8">
										<p style="margin: 0;"><?php echo '<a href="prospectsvue/'.$infoTmp['id'].'" target="_blank"><b>' . $infoTmp['nom'] . '</b> ('.date('d/m/Y', strtotime($infoTmp['date_relance'])).')</a>'; ?></p>
									</div>
									<div class="col-md-4">
										<p style="margin: 0;"><?php echo brl::getStatut($infoTmp['statut']); ?></p>
									</div>
									<div class="col-md-12">
										<?php if ($infoTmp['commentaire_relance'] != '') { ?>
										<p style="margin: 0;">Commentaire : <?php echo $infoTmp['commentaire_relance']; ?></p> 
										<?php } ?>
									</div>
								</div>
							<?php
							} 
							?>
						</div>
					</div>
				</div>

				<div class="brl-panel-bloc33 main">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><i class="fa fa-folder-open"></i> <a href="/prospects" style="color: white;"> Suivi Prospects</a></div>
						</div>
						<div style="padding: 15px;">
							<?php
							foreach ($lstProspectsRelanceAttenteRetourDevis as $infoTmp) {
							?>
								<div class="row">
									<div class="col-md-8">
										<p style="margin: 0;"><?php echo '<a href="prospectsvue/'.$infoTmp['id'].'" target="_blank"><b>' . $infoTmp['nom'] . '</b> ('.date('d/m/Y', strtotime($infoTmp['date_relance'])).')</a>'; ?></p>
									</div>
									<div class="col-md-4">
										<p style="margin: 0;"><?php echo brl::getStatut($infoTmp['statut']); ?></p>
									</div>
									<div class="col-md-12">
									<?php if ($infoTmp['commentaire_relance'] != '') { ?>
										<p style="margin: 0;">Commentaire : <?php echo $infoTmp['commentaire_relance']; ?></p> 
									<?php } ?>
									</div>
								</div>
							<?php
							} 
							?>
						</div>
					</div>
				</div>

				<div class="brl-panel-bloc33 main">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><i class="fa fa-folder-open"></i> <a href="/prospects" style="color: white;"> Suivi Autour du Cloud</a></div>
						</div>
						<div style="padding: 15px;">
							<?php echo $infoDuUserCloud; ?>
						</div>
					</div>
				</div>

				<div class="brl-panel-bloc33 main">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><i class="fa fa-folder-open"></i> <a href="/clients" style="color: white;">Suivi Contrats de maintenance</a></div>
							<?php
							$month = date("m");
							$mois_fr = array ( '01' => 'Janvier', '02' => 'Fevrier', '03' => 'Mars','04' => 'Avril','05' => 'Mai','06' => 'Juin','07' => 'Juillet','08' => 'Août','09' => 'Septembre','10' => 'Octobre','11' => 'Novembre','12' => 'Décembre');
							$monthSelected = (isset($_POST['chooseMonth'])) ? $_POST['chooseMonth'] : $month;
							?>	
							<form method="POST" style="float: right;margin-top: 3px;">
								<select name='chooseMonth' onchange='this.form.submit()' class="brl_input">
									<?php foreach($mois_fr as $key => $value): ?>
									<option value="<?= $key; ?>" <?= ($monthSelected == $key) ? 'selected="selected"' : ''; ?>><?= $value; ?></option>
									<?php endforeach; ?>
								</select>
							</form>
						</div>
						<div style="padding: 15px;">
							<?php
							echo '<h3 style="margin: 0;margin-bottom: 10px;">' . $mois_fr[$monthSelected] . '</h3>';
							$projetsNonSuivi = brl::getProjetsNonSuivi($monthSelected);
							foreach ($projetsNonSuivi as $key => $value) {
							?>
								<div class="row">
									<div class="col-md-8">
										<p><?php echo '<a href="clientsvue/'.$value['id'].'" target="_blank">' . $value['nom'] . '</a> : <br>' . $value['label_contrat']; ?></p>
									</div>
									<div class="col-md-4">
										<p><?php echo round($value['tempsTotal'] / 60, 2) . ' / ' . $value['duree_contrat'] . 'h'; ?></p>
									</div>
									<div class="col-md-12">
										<div class="progress">
											<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo (round($value['tempsTotal'] / 60, 2) /  $value['duree_contrat']) * 100; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo (round($value['tempsTotal'] / 60, 2) /  $value['duree_contrat']) * 100; ?>%">
											<?php echo round((round($value['tempsTotal'] / 60, 2) /  $value['duree_contrat']) * 100, 0) . '%'; ?>
											</div>
										</div>
									</div>
								</div>
							<?php 
							} 
							?>
						</div>
					</div>
				</div>

<?php
}

/* Page Liste Suivi Temps */
function displayPageContenuLstSuivisTemps($option) {
	
	// new object of brl_bd
	$brlBD = new brl_bd();
	
	// Search
	// Recovering of the search
	if(isset($_POST['rechercheAdh'])) {
		$_SESSION['SEARCH']['ADHERENTS'] = $_POST['rechercheAdh'];
		$option = "page-1";
	}

	// if the search is empty, returns an array
	$filtreAdh = (isset($_SESSION['SEARCH']['ADHERENTS'])) ? $_SESSION['SEARCH']['ADHERENTS'] : array();

	// Pagination
	if(isset($option) && !empty($option)) { 
		list($actionPage, $numPage) = explode('-', $option);
		if ($actionPage == 'page' && ((int)$numPage) > 0 ) {
			$_SESSION['PAGE']['ADHERENTS'] = $numPage;
		}
	}
	$numPage = (isset($_SESSION['PAGE']['ADHERENTS'])) ? $_SESSION['PAGE']['ADHERENTS'] : 1; 

	//  parameter for recover the id_client of the user in the session
	$idClient = (isset($_SESSION['USER']['ID_CLIENTS'])) ? $_SESSION['USER']['ID_CLIENTS'] : '';

	$filtreAdh['date_commencement'] = '';

	// Filter for clients profils
	// if you're a client (so you've an id different of 0)
	if(($_SESSION['USER']['ID_CLIENTS'])!=0) {
		// we put an array of the id client in the filter array of idclients
		$filtreAdh['idClients'] = array(($_SESSION['USER']['ID_CLIENTS']));
		// echo "<pre>"; print_r($filtreAdh);echo "</pre>";exit ;
		//List for searches
		$lstInfoSuiviTemps 	= brl::getLstSuiviTemps($brlBD, $filtreAdh, $numPage);
		$lstInfoSuiviTempsWithoutLimit 	= brl::getLstSuiviTemps($brlBD, $filtreAdh, $numPage, false);
		// List for recover clients infos
		$lstClients			= brl::getLstingClients($idClient);
	// if you're not a client
	} else if(($_SESSION['USER']['ID_CLIENTS'])==0) {
		//List for searches
		$lstInfoSuiviTemps 	= brl::getLstSuiviTemps($brlBD, $filtreAdh, $numPage);
		$lstInfoSuiviTempsWithoutLimit 	= brl::getLstSuiviTemps($brlBD, $filtreAdh, $numPage, false);
		// List for recover clients infos
		$lstClients			= brl::getLstingClients($idClient);
		// we put the id of the client in the filter array of idclients
		$filtreAdh['idClients'] = array($idClient);
		// echo "<pre>"; print_r($filtreAdh);echo "</pre>";exit ;
	}	

	$lstProjets			= brl::getLstingProjets(false, $idClient);
	$lstTypePresta		= brl::getLstingTypePresta();
	$compteur 			= brl::getCompteurSuiviTemps($filtreAdh);


	$totalMinutes = 0;
	$totalMinutesNonFacture = 0;
	foreach ($lstInfoSuiviTempsWithoutLimit as $row) {
		$totalMinutes += $row['temps'];
		if($row['validite'] == 1) {
			$totalMinutesNonFacture += $row['temps'];
		}
	}
	$totalHeures = round($totalMinutes / 60, 2);
	$totalHeuresNonFacture = round($totalMinutesNonFacture / 60, 2);
?>
				
				<!-- RECHERCHE -->
				<div class="brl-panel-bloc100" id="bloc_search">
					<div class="brl-lst">
						<form method="post" action="" id="filtreAdh">
						
							<div>
						
								<!--<input id="rechercheAllAdh" name="rechercheAdh[search]" value="<?php if (isset($filtreAdh['search'])) echo $filtreAdh['search']; ?>" class="brl_input-l" autocomplete="off" placeholder="Rechercher...">-->
								<input type="text" class="brl_input" name="rechercheAdh[date_commencement]" id="date_commencement" value="<?php if (isset($filtreAdh['date_commencement'])) echo $filtreAdh['date_commencement']; ?>" placeholder="Choisir une date"/>
								<select id="idProjet" name="rechercheAdh[idProjet][]" class="brl_input-s selectpicker" multiple title="Projets"> <!-- RECHERCHE Projet -->
									<?php foreach ($lstProjets as $infoTmp) { ?>
										<option value="<?php echo $infoTmp['id']; ?>" <?php if(isset($filtreAdh['idProjet']) && in_array($infoTmp['id'], $filtreAdh['idProjet'])) { echo "selected"; } ?>> <?php echo $infoTmp['nom']; ?> </option>
									<?php } ?>
								</select>
								<select id="idClients" name="rechercheAdh[idClients][]" class="brl_input-s selectpicker" multiple title="Clients"><!-- RECHERCHE CLIENTS -->
									<!--input type="hidden" id="filtrePays" name="filtrePays" value="<?php echo $filtreAdh['idClients']; ?>" /-->
									<?php foreach ($lstClients as $infoTmp) { ?>
										<option value="<?php echo $infoTmp['id']; ?>" <?php if(isset($filtreAdh['idClients']) && in_array($infoTmp['id'], $filtreAdh['idClients'])) { echo "selected"; } ?>> <?php echo $infoTmp['nom']; 
										?> </option>
									<?php } ?>
								</select>								
								<select id="idTypePresta" name="rechercheAdh[idTypePresta][]" class="brl_input-l selectpicker" multiple title="Prestations"><!-- RECHERCHE PRESTATION-->
									<?php foreach ($lstTypePresta as $infoTmp) { ?>
										<option value="<?php echo $infoTmp['id']; ?>" <?php if(isset($filtreAdh['idTypePresta']) && in_array($infoTmp['id'], $filtreAdh['idTypePresta'])) { echo "selected"; } ?>> <?php echo $infoTmp['nom']; ?> </option>
									<?php } ?>
								</select>
								<select id="validite" name="rechercheAdh[validite][]" class="brl_input-l selectpicker" multiple title="Validité"><!-- RECHERCHE VALIDITÉ -->
										<option value="0"> Facturé </option>
										<option value="1"> Non facturé </option>	
										<option value="2"> Inclus </option>
								</select>							
								<button type="submit" onclick="submit()" class="brl_input-s btn btn-outline-primary" style="float: right;">Rechercher</button>

							</div>
							
						</form>
						
					</div>
					
				</div>
				<!-- FIN RECHERCHE -->
				

				<div class="brl-panel-bloc100" style="padding-bottom: 0;">
					<div class="titre-bloc-b">
						<?php 
							$isFiltreArray = is_array($filtreAdh); 
							if($isFiltreArray) {
								if(count($filtreAdh) == 1 && $filtreAdh['date_commencement'] == '') {
									?>
									<div class="brl-title">Suivis Temps : <?php echo count($lstInfoSuiviTempsWithoutLimit); ?> fiches - <?= $totalHeures; ?><span style="text-transform: lowercase;">h</span> - (Non facturé : <?= $totalHeuresNonFacture; ?><span style="text-transform: lowercase;">h</span>)</div>
									<?php
								} else {
									?>
									<div class="brl-title">Suivis Temps : <?php echo count($lstInfoSuiviTempsWithoutLimit); ?> fiches - <?= $totalHeures; ?><span style="text-transform: lowercase;">h</span> - (Non facturé : <?= $totalHeuresNonFacture; ?><span style="text-transform: lowercase;">h</span>)</div>
									<?php
								}
							} else {
								?>
								<div class="brl-title">Suivis Temps : <?php echo count($lstInfoSuiviTempsWithoutLimit); ?> fiches - <?= $totalHeures; ?><span style="text-transform: lowercase;">h</span> - (Non facturé : <?= $totalHeuresNonFacture; ?><span style="text-transform: lowercase;">h</span>)</div>
								<?php
							}
						?>
						<div class="bouton3 clear" style="float: right;">
							<a href="<?php echo _URL_BASE_; ?>/suivitemps/0#contenu"><i class="fa fa-plus bleu" aria-hidden="true"></i> Ajouter</a>
							<!--<a href="#" id = "SupprAll" onclick="if(window.confirm('Voulez-vous vraiment supprimer ?')){ getElementById('actionMassive').value = 'suppression'; submitform(); }else{return false;} "><i class="fa fa-times rougevif" aria-hidden="true"></i> Supprimer </a>-->
						</div>
					</div>
					
					<div class="brl-lst">
						<form method="post" action="<?php echo _URL_BASE_; ?>/adherentActionLst" name="formMasse" id="formMasse" autocomplete="off">
							<!--<input type="hidden" value="" name="actionMassive" id="actionMassive" />-->
							<table  style="width:100%" class="brlselectable"> <!-- Affichage des fiches en tableau-->
								<tr class="brl-lst-el">
									<th> </th>
									<th> Type prestation </th>
									<th> Client </th>
									<th> Projet </th>
									<th> Date </th>
									<th> <i class="fas fa-stopwatch"></i> </th>
									<th> Description </th>
									<th> Statut </th>
								</tr>
									<?php foreach ($lstInfoSuiviTemps as $infoTmp) { ?>
									<?php $projetSingle = brl::getProjet($infoTmp['id_projet']); ?>
										<tr class="brl-lst-el main" ondblclick="location.href='<?php echo _URL_BASE_; ?>/suivitemps/<?php echo $infoTmp['id'].'#contenu'; ?>'"> <!-- ligne -->
											<td>
												<input type="checkbox" class="brl_checkbox-s checkp" name="adherent[]" value="<?php echo $infoTmp['id']; ?>">
											</td>
											<td>
												<?php
													// On récupère le nom de la prestation
													$prestaSingle = brl::getTypePresta($infoTmp['id_type_presta']);
												?>
												<a href="suivitemps/<?php echo $infoTmp['id'].'#contenu'; ?>'" target="_blank"><?php echo $prestaSingle['nom']; ?></a>
											</td>
											<td>
												<?php
													// On récupère le nom du client
													$clientSingle = brl::getClients($projetSingle['id_client']);
												?>
												<a href="clientsvue/<?php echo $projetSingle['id_client']; ?>" target="_blank"><?php echo $clientSingle['nom']; ?></a>
											</td>
											<td>
												<a href="<?php echo _URL_BASE_; ?>/projetsvue/<?php echo $projetSingle['id']; ?>"><?php echo $projetSingle['nom'];?></a>
											</td>
											<td>
												<?php
													if($infoTmp['date_debut'] != '0000-00-00 00:00:00' && $infoTmp['date_debut'] != NULL) {
														$date_debut = DateTime::createFromFormat('Y-m-d H:i:s', $infoTmp['date_debut']);
														echo $date_debut->format('d-m-Y');
													}
													else {
														echo "Pas encore commencé !";
													}
												?>
											</td>
											<td>
												<?php echo $infoTmp['temps']; ?>'
											</td>
											<td>
												<?php echo brl_tools::truncate($infoTmp['description'], 80); ?>
											</td>
											<td>
												<?php if($infoTmp['validite']=="0") { echo '<div class="badgeVert">Facturé</div>'; } 
													else { 
														if($infoTmp['validite']=="1") { echo '<div class="badgeRouge">Non facturé</div>'; } 
														if($infoTmp['validite']=="2") { echo '<div class="badgeBleu">Inclus</div>'; } 
													}
												?>
											</td>

										<div class="clear"></div>

										</tr>
									<?php }  ?>
							</table> <!--fin de l'affichage-->
						</form>
					</div>

					<div> &nbsp; </div>
					
					<script type="text/javascript">
						
						$( function() {
							//$( "#selectable" ).selectable();
							
							$('.brlselectable tr').click(function(event) {
								if (event.target.type !== 'checkbox') {
									$(':checkbox', this).trigger('click');
									if ($(':checkbox', this).prop('checked')) {
										$(this).addClass('checkedLine');
									} else {
										$(this).removeClass('checkedLine');
									}
								}
							});
							
							
						});
						
						function submitform()
						{
							
							var formOk = true;

							// Test form
							if (formOk) {
								document.formMasse.submit();
								return true;
							} else {
								return false;
							}

						}

					</script>
<?php
	$nb_total = $compteur['nbrElm'];
	brl::displayPagination($_GET['action'], $numPage, $nb_total);
	
	 //echo $_GET['action'] .' ?...? '. $numPage .' ?...? '. $compteur['nbrElm'];
?>

			</div>
<?php

}


function displayPageContenuSuiviTemps($idAdh) {

	//  paramètre pour récupérer un id_client en particulier
	$idClient = (isset($_SESSION['USER']['ID_CLIENTS'])) ? $_SESSION['USER']['ID_CLIENTS'] : '';

	$lstInfoSuiviTemps 	= brl::getSuiviTemps($idAdh);
	$lstProjets 		= brl::getLstingProjets(false, $idClient);
	$lstTypePresta 		= brl::getLstingTypePresta();

	$lstLog				= brl ::getLogSuiviTemps($idAdh);
	//echo "<pre>"; print_r($lstCtrInteret); echo "</pre>";
	
	$titre = 'Modifier la fiche suivi temps';
	if (empty($idAdh)) { 
		$titre = 'Ajouter une fiche';
		$lstInfoSuiviTemps = array();
		$lstInfoSuiviTemps['id_projet'] = '' ;
		$lstInfoSuiviTemps['date_debut'] = '' ;
		$lstInfoSuiviTemps['type_presta'] = '';
		$lstInfoSuiviTemps['temps'] = '';
		$lstInfoSuiviTemps['description'] = '';
		$lstInfoSuiviTemps['validite'] = '1';
		$lstInfoSuiviTemps['user'] = '';
	}
	
	//Récupération du droit d'administateur et d'employé
	$is_admin=$_SESSION["USER"]["IS_ADMIN"];
	$is_super_admin=$_SESSION["USER"]["IS_SUPER_ADMIN"];
	$is_client=$_SESSION["USER"]["IS_CLIENT"];

	// Récupération de la liste des utilisateurs $_SESSION['USER']['LOGIN']
	$lstUsers = BRL::getLstUsers();
?>
				<div class="brl-panel-bloc70 brl-panel-bloc-centre contenuSmall" id="ficheAdherent">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><?php echo $titre; ?></div>
							
							<div class="bouton5 clear" style="float: right;">
								<?php if (!empty($idAdh)) { ?><a href="<?php echo _URL_BASE_; ?>/suivitempsrm/<?php echo $idAdh; ?>" onclick="if(window.confirm('Voulez-vous vraiment supprimer ?')){return true;}else{return false;}"><i class="fa fa-times rougevif" aria-hidden="true"></i> Supprimer </a><?php } ?>
							</div>
						</div>
						<div id="curve_chart" style="width: 100%;">
											
						<form method="post" action="<?php echo _URL_BASE_; ?>/suivitempssav/<?php echo $idAdh; ?>" name="formAdh" id="formAdh" autocomplete="off">
							<input type="hidden" id="idAdherent" value="<?php echo $idAdh; ?>" />
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn-primary btn-rouge <?php if ($lstInfoSuiviTemps['validite'] == "1" ) { echo "active"; } ?>"><input type="radio" name="options" id="option1" value="1"   autocomplete="off" <?php if ($lstInfoSuiviTemps['validite'] == "1" ) { echo "checked"; } ?>>Non facturé</label>
								<label class="btn btn-primary btn-bleu <?php if ($lstInfoSuiviTemps['validite'] == "2" ) { echo "active"; } ?>"><input type="radio" name="options" id="option3" value="2"   autocomplete="off" <?php if ($lstInfoSuiviTemps['validite'] == "2" ) { echo "checked"; } ?>>Inclus</label>
								<label class="btn btn-primary btn-vert <?php if ($lstInfoSuiviTemps['validite'] == "0" ) { echo "active"; } ?>"><input type="radio" name="options" id="option2" value="0"   autocomplete="off" <?php if ($lstInfoSuiviTemps['validite'] == "0" ) { echo "checked"; } ?>>Facturé</label>
							</div>
							<div class="tab-content">
								<div class="bloc-gauche" style="width: 100%;">	
									<h4>Informations Générales</h4>
									<div class="col-md-7">
										<label>Date de commencement :</label><input type="text" class="brl_input-l" name="date_commencement" id="date_commencement" value="<?php if($lstInfoSuiviTemps['date_debut'] != "0000-00-00 00:00:00" && $lstInfoSuiviTemps['date_debut'] != NULL) { echo DateTime::createFromFormat('Y-m-d H:i:s', $lstInfoSuiviTemps['date_debut'])->format('d/m/Y'); } else { echo date('d/m/Y'); } ?>" placeholder="Choisir une date de début"/>
										<br />
										<label>Projet :</label><select id="idProjet" name="idProjet" class="brl_input-l">
											<option value="0">Sélectionnez un projet</option>
											<?php foreach ($lstProjets as $infoTmp) { ?>
												<?php $infoClient = brl::getClients($infoTmp['id_client']); // Servira à retourner le nom du client ?>
												<option value="<?php echo $infoTmp['id']; ?>" <?php if(isset($lstInfoSuiviTemps['id_projet']) && $lstInfoSuiviTemps['id_projet'] == $infoTmp['id']) { echo "selected"; } ?>> <?php echo $infoTmp['nom'] . ' ['. $infoClient['nom'] . ']'; ?> </option>
											<?php } ?>
										</select><br />
									</div>
									<div class="col-md-5">
										<label>Utilisateur :</label><select id="users" name="users" class="brl_input">
											<option value="0">Sélectionnez un utilisateur</option>
											<?php foreach ($lstUsers as $infoTmp) { ?>
												<option value="<?php echo $infoTmp['login']; ?>" <?php if($lstInfoSuiviTemps['user'] == '' && $infoTmp['login'] == $_SESSION['USER']['LOGIN']){ echo "selected"; } elseif(isset($lstInfoSuiviTemps['user']) && $lstInfoSuiviTemps['user'] == $infoTmp['login']) { echo "selected"; } ?>> <?php echo $infoTmp['login']; ?> </option>
											<?php } ?>
										</select>
										<br />
										<label>Type de prestation :</label><select id="idTypePresta" name="idTypePresta" class="brl_input">
											<option value="0">Sélectionnez une prestation</option>
											<?php foreach ($lstTypePresta as $infoTmp) { ?>
												<option value="<?php echo $infoTmp['id']; ?>" 
														<?php if(isset($lstInfoSuiviTemps['id_type_presta'])) { 
																if($lstInfoSuiviTemps['id_type_presta'] == $infoTmp['id']) {
																	echo "selected"; 
																}
															} elseif(2 == $infoTmp['id']) {
																echo "selected";
															} ?>>
													<?php echo $infoTmp['nom']; ?> 
												</option>
											<?php } ?>
										</select>
										<p class="clear">&nbsp;</p>	
									</div>
									<div class="col-md-12">
										<div style="display: flex;align-items: center;">
											<label>Temps :</label><input type="number" min="0" step="1" id="temps" name="temps" value="<?php echo $lstInfoSuiviTemps['temps']; ?>" class="brl_input-l" placeholder="Temps en minutes">
											<div style="display:inline;text-align: center;">
												<p id="finish" style="display: inline-block;padding: 5px 15px;color: #55a744;cursor: pointer;margin: 0;">
													<i class="fas fa-plus fa-2x"></i>
												</p>
												<p id="chrono" style="font-size: 20px;background: #008dd1;color: #fff;display: inline-block;border-radius: 4px;padding: 3px 20px;margin: 0;"><time>00:00:00</time></p>
												<p id="start" style="display: inline-block;padding: 5px 15px;color: #008dd1;cursor: pointer;margin: 0;">
													<i class="fas fa-play fa-2x"></i>
												</p>
												<p id="stop" style="display: inline-block;padding: 5px 15px;color: #008dd1;cursor: pointer;margin: 0;">
													<i class="fas fa-pause fa-2x"></i>
												</p>
											</div>
										</div>
										<br>
										<label>Description :</label><input type="text" id="description" name="description" class="brl_input-l" placeholder="Description" value="<?php echo $lstInfoSuiviTemps['description']; ?>" autocomplete="off" style="width: 806px;">
										<?php if ($is_client==0) { ?>
											<label>Commentaire interne :</label><br><textarea rows="6" id="commentaireInterne" name="commentaireInterne" class="brl_input" placeholder="Commentaire interne"  autocomplete="off" style="width: 99%;"><?php echo $lstInfoSuiviTemps['commentaire_interne']; ?></textarea><br>
										<?php } ?>
									</div>
									<p class="clear">&nbsp;</p>	
								</div>
								<div>
									<div class="bouton2 clear" style="text-align: center">
										<input type="submit" style="display:none">
										<a href="<?php echo _URL_BASE_; ?>/suivistemps"><i class="fa fa-arrow-left" aria-hidden="true"></i> ANNULER </a>
										<a href="javascript:submitform();" class="btn-green-save"><i class="fa fa-check vert" aria-hidden="true"></i> ENREGISTRER </a>
									</div>
								</div>
							</div>
							<p class="clear">&nbsp;</p>						

							<script type="text/javascript">

							var chrono = document.getElementById('chrono'),
								inputTemps = document.getElementById('temps'),
								start = document.getElementById('start'),
								stop = document.getElementById('stop'),
								finish = document.getElementById('finish'),
								seconds = 0, minutes = 0, hours = 0,
								t;

							start.style.display = "none";

							function add() {
								seconds++;
								if (seconds >= 60) {
									seconds = 0;
									minutes++;
									if (minutes >= 60) {
										minutes = 0;
										hours++;
									}
								}
								
								chrono.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);

								timer();
							}
							timer();

							function timer() {
								t = setTimeout(add, 1000);
							}

							/* Start button */
							start.onclick = function() {
								timer();
								start.style.display = "none";
								stop.style.display = "inline-block";
							}

							/* Stop button */
							stop.onclick = function() {
								clearTimeout(t);
								start.style.display = "inline-block";
								stop.style.display = "none";
							}

							/* Finish button */
							finish.onclick = function() {
								chrono.textContent = "00:00:00";
								if(inputTemps.value == "") {
									inputTemps.value = minutes;
								} else {
									inputTemps.value = parseInt(inputTemps.value) + minutes;
								}
								clearTimeout(t);
								timer();
								seconds = 0; minutes = 0; hours = 0;
								start.style.display = "none";
								stop.style.display = "inline-block";
							}
								
								
							function submitform()
							{
								var formOk = true;
														
								// Test form
								if (formOk) {
									document.formAdh.submit();
									return true;
								} else {
									alert("Informations manquantes sur la fiche");
									return false;
								}
								
							}
							</script>
						</form>
						
						</div>
					</div>
				</div>
				<p class="clear">&nbsp;</p>
				
				
				
				<p class="clear">&nbsp;</p>	
				<?php if ($is_super_admin) { ?>
					
					<div class="brl-panel-bloc70 brl-panel-bloc-centre">
						<div>
							<div class="titre-bloc-b">
								<div class="brl-title">Log</div>
							</div>
							<div id="curve_chart" style="width: 100%;" class="log-scroll">
								<div class="tab-content" style="margin-top: 20px;">
									<?php $listeLog = explode("\\n",$lstLog['logUser']); 
									// echo '<pre>'; print_r ($listeLog); echo '</pre>';
									foreach ($listeLog as $infoTmp) {
										$liste=explode("\n", $infoTmp);
										foreach ($liste as $valeur) {?>
										<label><?php echo $valeur; ?></label><br>
									<?php } 
									}?>
								</div>
							</div>
						</div>
					</div>

				<?php } ?>
<?php

}

/* Page Utilisateurs */

function displayPageContenuUsersComptes($option) {
		
	// Pagination
	if(isset($option) && !empty($option)) { 
		list($actionPage, $numPage) = explode('-', $option);
		if ($actionPage == 'page' && ((int)$numPage) > 0 ) {
			$_SESSION['PAGE']['USERS'] = $numPage;
		}
	}
	$numPage = (isset($_SESSION['PAGE']['USERS'])) ? $_SESSION['PAGE']['USERS'] : 1; 

	//compteur
	$compteur = brl::getCompteurUser();
	
	$lstInfoUser   = brl::getLstUser($numPage);
	
?>
				<!--div class="brl-panel-bloc100">
					<div class="brl-row left" style="text-align: center; padding-top: 40px;">
					</div>
				</div-->
<?php

?>
				<div class="brl-panel-bloc100">
					<div class="titre-bloc-b">
						<div class="brl-title">Liste des Utilisateurs</div>
						<div class="bouton3 clear" style="float: right;">
							<a href="<?php echo _URL_BASE_; ?>/user/0"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter</a>
						</div>
					</div>
					<div class="brl-lst">
						<table style="width:100%">
							<tr class="brl-lst-el">
								<th> Utilisateurs </th>
								<th> Rôle </th>
								<th> Statut </th>
							</tr>
							<?php foreach ($lstInfoUser as $infoTmp) { ?>
							<tr class="brl-lst-el">
								<td>
									<a href="<?php echo _URL_BASE_; ?>/user/<?php echo $infoTmp['idUser']; ?>"><?php echo $infoTmp['login']; ?></a>
								</td>
								<td style="width: 250px;">
									<?php if($infoTmp['is_admin']=="1") { 
											echo '<span class="bold rougefonce">Administrateur</span>'; 
										} elseif($infoTmp['is_employe']=="1") { 
											echo '<span class="bold bleu">Employé</span>'; 
										} elseif(($infoTmp['is_client']=="1") && ($infoTmp['id_clients'] == $infoTmp['id'])) { 
											echo '<span class="bold rouge">Client : <i>', $infoTmp['nom'], '</i></span>'; 
										} else {
											break;
										} ?>
								</td>
								<td style="width: 150px;">
									<?php if($infoTmp['is_actif']=="1") { echo '<div class="badgeVert">Actif</div>'; } else { echo '<div class="badgeRouge">Inactif</div>'; } ?>
								</td>
								<div class="clear"></div>
							</tr>
							<?php } ?>
						</table>
					</div>
					<div> &nbsp; </div>
<?php
	brl::displayPagination($_GET['action'], $numPage, $compteur['nbrElm']);
?>

				</div>
<?php

}


function displayPageContenuUser ($idUser) {

	//$lstInfoCompte = brl::getLstCompteAdWords();
	$lstInfoUser = brl::getUser($idUser);

	$lstLog		 = brl::getLogUser($idUser);
	// echo "<pre>"; print_r($lstInfoUser); echo "</pre>";exit;

	$titre = 'Modifier l\'utilisateur';
	if (empty($idUser)) { $titre = 'Ajouter l\'utilisateur'; }
	
	//Récupération du droit d'administateur
	$is_admin=$_SESSION["USER"]["IS_ADMIN"];
	$is_super_admin=$_SESSION["USER"]["IS_SUPER_ADMIN"];
	//echo "<pre>"; print_r($_SESSION); echo "</pre>";

	// Récupération de la liste des clients pour le select
	$brlBD = new brl_bd();
	$sql = "SELECT id, nom FROM `clients` WHERE type = '0' AND is_actif = '1' ORDER BY nom;";
	$result = $brlBD->query_lst($sql);
?>

				<div class="brl-panel-bloc70 brl-panel-bloc-centre">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><?php echo $titre; ?></div>
							<div class="bouton5 clear" style="float: right;">
							<?php if (!empty($idUser)) { ?><a href="<?php echo _URL_BASE_; ?>/userrm/<?php echo $idUser; ?>" onclick="if(window.confirm('Voulez-vous vraiment supprimer ?')){return true;}else{return false;}"><i class="fa fa-times" aria-hidden="true"></i> Supprimer</a><?php } ?>
							</div>
						</div>
						<div id="curve_chart" style="width: 100%;">
											
						<form method="post" action="<?php echo _URL_BASE_; ?>/usersav/<?php echo $idUser; ?>" name="form" style="padding: 20px 0;" autocomplete="off">
						
							

							<div class="tab-content" style="margin-top: 20px;">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-primary btn-vert <?php if ($lstInfoUser['is_actif'] != "0" ) { echo "active"; } ?>"><input type="radio" name="options" id="option1" value="1"   autocomplete="off" <?php if ($lstInfoUser['is_actif'] != "0" ) { echo "checked"; } ?>>Actif</label>
									<label class="btn btn-primary btn-rouge <?php if ($lstInfoUser['is_actif'] == "0" ) { echo "active"; } ?>"><input type="radio" name="options" id="option2" value="0"   autocomplete="off" <?php if ($lstInfoUser['is_actif'] == "0" ) { echo "checked"; } ?>>Inactif</label>
								</div>

								<h4>Connexion</h4>
								<div class="col-md-6">
									<label>Login :<sup class="red">*</sup></label> <input type="login" id="login" name="login" value="<?php echo $lstInfoUser['login']; ?>" class="brl_input" placeholder="login" data-error="Login incorrect" required><br>
									<label>Changer le mot de passe :</label> <input type="password" id="password" name="password" value="" class="brl_input" placeholder="************" autocomplete="off"><br>
									<label>Confirmer le mot de passe :</label> <input type="password" id="Cpassword" name="Cpassword" value="" class="brl_input" placeholder="************" autocomplete="off"><br>
								</div>
								<div class="col-md-6">
									<label>Rôle : </label> 
									<select name="selectRole" id="selectRole" class="brl_input" onchange="displaySelect(this)">
										<option <?php if($lstInfoUser['is_admin']==1)  { echo 'selected'; } ?> value="1">Administrateur</option>
										<option <?php if($lstInfoUser['is_employe']==1)  { echo 'selected'; } ?> value="2">Employé</option>
										<option <?php if($lstInfoUser['is_client']==1)  { echo 'selected'; } ?> value="3">Client</option>
									</select>
									<div id="displaySelectClient">
										<label>Client : </label> 
										<select name="selectClient" id="selectClient" class="brl_input">
											<?php foreach ($result as $infoTmp) { ?>
												<option value="<?php echo $infoTmp['id']; ?>" <?= ($infoTmp['id'] == $lstInfoUser['id_clients']) ? "selected='selected'" : ''; ?>><?php echo $infoTmp['nom']; ?>
											<?php }  ?>
										</select>
									</div>
								</div>
								
								<p class="clear">&nbsp;</p>						

							<script type="text/javascript">

							$("options").button('toggle');
								

							$(document).ready(function() {

								displaySelect(this);   

							});
							

							function displaySelect(line) {
								let elDOM = $(line);
								// console.log('eldom:',elDOM);

								if(elDOM.find("option:selected").val()==3) {
									$('#displaySelectClient').css({'display': 'block'});
								} else {
									$('#displaySelectClient').css({'display': 'none'});
								}
							}
								
							function submitform()
							{
								var formOk = true;
								var champLog = $("#login");
								champLog.css("background-color", "initial");
								var champMDP = $("#password");
								champMDP.css("background-color", "initial");
								var champMDP2 = $("#Cpassword");
								champMDP2.css("background-color", "initial");
								
								// Test champ Mail
								if (champLog.val() == "") {
									champLog.css("background-color", "red");
									alert("Erreur de saisie");
									formOk = false;
								}
								
								// Test champ password
								if (champMDP2.val() != champMDP.val()) {
									champMDP2.css("background-color", "red");
									alert("Mot de passe incorrect.")
									formOk = false;
								}
								
								// Test form
								if (formOk) {
									document.form.submit();
									return true;
								} else {
									alert("Informations manquantes sur l'utilisateur");
									return false;
								}
								
							}
							</script>
						</form>
						
							</div>
						</div>
					</div>
				</div>
				<p class="clear">&nbsp;</p>
				
				<div>
					<div class="bouton2 clear" style="text-align: center;">
						<a href="<?php echo _URL_BASE_; ?>/users">ANNULER</a>
						<a href="javascript:submitform();" class="btn-green-save">ENREGISTRER</a>
					</div>
					<input type="submit" style="display:none">
				</div>
				<p class="clear">&nbsp;</p>
					
				<?php if ($is_super_admin) { ?>
					
					
						<div>
							<div class="titre-bloc-b">
								<div class="brl-title">Log</div>
							</div>
							<div id="curve_chart" style="width: 100%;" class="log-scroll">
								<div class="tab-content" style="margin-top: 20px;">
									<?php $listeLog = explode("\\n",$lstLog['logUser']); 
									// echo '<pre>'; print_r ($listeLog); echo '</pre>';
									foreach ($listeLog as $infoTmp) {
										$liste=explode("\n", $infoTmp);
										foreach ($liste as $valeur) {?>
										<label><?php echo $valeur; ?></label><br>
									<?php } 
									}?>
								</div>
							</div>
						</div>

				<?php } ?>
						
<?php

}

/*Page Liste des Projets */
function displayPageContenuLstProjets($option) {
		
	//Recherche
	// Récupération de la recherche
	if(isset($_POST['rechercheAdh'])) {
		$_SESSION['SEARCH']['ADHERENTS'] = $_POST['rechercheAdh'];
		$option = "page-1";
	}
	$filtreAdh = (isset($_SESSION['SEARCH']['ADHERENTS'])) ? $_SESSION['SEARCH']['ADHERENTS'] : '';

	// Pagination
	if(isset($option) && !empty($option)) { 
		list($actionPage, $numPage) = explode('-', $option);
		if ($actionPage == 'page' && ((int)$numPage) > 0 ) {
			$_SESSION['PAGE']['PROJETS'] = $numPage;
		}
	}
	//echo $option.'plop';
	//echo "<pre>"; print_r($_SESSION); echo "</pre>"; //exit;
	$numPage = (isset($_SESSION['PAGE']['PROJETS'])) ? $_SESSION['PAGE']['PROJETS'] : 1; 

	//compteur
	$compteur = brl::getCompteurProjets();
	
	$lstInfoProjets	= brl::getLstProjets($numPage, $filtreAdh);
	
?>
				<!--div class="brl-panel-bloc100">
					<div class="brl-row left" style="text-align: center; padding-top: 40px;">
					</div>
				</div-->
				<!-- RECHERCHE -->
				<div class="brl-panel-bloc100" id="bloc_search">
					<div class="brl-lst">
						<form method="post" action="" id="filtreAdh">
							<div>
								<select id="validite" name="rechercheAdh[validite][]" class="brl_input-l selectpicker" title="Validité"><!-- RECHERCHE VALIDITÉ -->
										<option value="1"> ACTIF </option>	
										<option value="0"> INACTIF </option>
								</select>							
								<button type="submit" onclick="submit()" class="brl_input-s btn btn-outline-primary" style="float: right;">Rechercher</button>
							</div>
						</form>
					</div>
				</div>

				<div class="brl-panel-bloc100">
					<div class="titre-bloc-b">
						<div class="brl-title">Liste des Projets</div>
						<div class="bouton3 clear" style="float: right;">
							<a href="<?php echo _URL_BASE_; ?>/projetsvue/0"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter</a>
						</div>
					</div>
					<div class="brl-lst">
							<table style="width:100%"> <!-- Affichage des fiches en tableau-->
								<tr class="brl-lst-el">
									<th> Client </th>
									<th> Projet </th>
									<th> Type </th>
									<th> Lien </th>
									<th style="width: 150px;"> Statut </th>
								</tr>
								<?php foreach ($lstInfoProjets as $infoTmp) { ?>
								<?php 
									$infoClient = brl::getClients($infoTmp['id_client']); // Servira à retourner le nom du client
									$infoTypeProjet = brl::getTypeProjet($infoTmp['type_projet']); // Servira à retourner le type de projet  
								?>
								<tr class="brl-lst-el"> <!-- ligne -->
									<td>
										<a href="clientsvue/<?php echo $infoTmp['id_client']; ?>" target="_blank"><?php echo $infoClient['nom']; ?></a>
									</td>
									<td>
										<a href="<?php echo _URL_BASE_; ?>/projetsvue/<?php echo $infoTmp['id']; ?>"><?php echo $infoTmp['nom'];?></a>
									</td>
									<td>
										<?php echo (!empty($infoTypeProjet['nom']))? $infoTypeProjet['nom'] : '-'; ?>
									</td>
									<td>
										<?php if(!empty($infoTmp['lien_bo'])) { ?>
										<a href="<?php echo $infoTmp['lien_bo']; ?>" target="_blank"><i class="fas fa-external-link-alt"></i></a>
										<?php } else { ?>
										/
										<?php } ?>
									</td>
									<td>
										<?php if($infoTmp['is_actif']=="1") { echo '<div class="badgeVert">Actif</div>'; } else { echo '<div class="badgeRouge">Inactif</div>'; } ?>
									</td>
									<div class="clear"></div>
								</tr>
								<?php }  ?>
							</table> <!--fin de l'affichage-->
					</div>
						<div> &nbsp; </div>
						<script type="text/javascript">
						
						function submitform()
						{
							
							var formOk = true;

							// Test form
							if (formOk) {
								document.formMasse.submit();
								return true;
							} else {
								return false;
							}

						}

					</script>
<?php
	brl::displayPagination($_GET['action'], $numPage, $compteur['nbrElm']);
?>

			</div>
<?php

}


function displayPageContenuProjets ($idProjet) {

	$lstInfoProjets = brl::getProjet($idProjet);
	$lstLog 	 = brl::getLogProjet($idProjet);
	$lstTypeProjet 		= brl::getLstingTypeProjet();

	$nbre_suivi_nonfacture = brl::getCountSuiviTempsProjet($idProjet, 1);
	$nbre_suivi_facture = brl::getCountSuiviTempsProjet($idProjet, 0);
	$nbre_minutes_nonfacture = brl::getSumSuiviTempsProjet($idProjet, 1);
	$nbre_minutes_facture = brl::getSumSuiviTempsProjet($idProjet, 0);

	$titre = 'Modifier le Projet';
	if (empty($idProjet)) { $titre = 'Ajouter le Projet'; }
	
	//Récupération du droit d'administateur
	$is_admin=$_SESSION["USER"]["IS_ADMIN"];
	$is_super_admin=$_SESSION["USER"]["IS_SUPER_ADMIN"];
	//echo "<pre>"; print_r($lstInfoProjets); echo "</pre>";
	//echo "<pre>"; print_r($lstLog); echo "</pre>"; //exit;

	// Récupération de la liste des clients pour le select
	$brlBD = new brl_bd();
	$sql = "SELECT id, nom FROM `clients` WHERE type = '0' AND is_actif = '1' ORDER BY nom;";
	$result = $brlBD->query_lst($sql);
	
?>
				<div class="brl-panel-bloc70 brl-panel-bloc-centre">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><?php echo $titre; ?></div>
							<div class="bouton5 clear" style="float: right;">
							<?php if (!empty($idProjet)) { ?><a href="<?php echo _URL_BASE_; ?>/projetsrm/<?php echo $idProjet; ?>" onclick="if(window.confirm('Voulez-vous vraiment supprimer ?')){return true;}else{return false;}"><i class="fa fa-times" aria-hidden="true"></i> Supprimer</a><?php } ?>
							</div>
						</div>
						<div id="idPageContenuCtrIntere" style="width: 100%;">
											
						<form method="post" action="<?php echo _URL_BASE_; ?>/projetssav/<?php echo $idProjet; ?>" name="form" style="padding: 20px 0;" autocomplete="off">
						
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#tab1">Fiche</a></li>
								<li><a data-toggle="tab" href="#tab2">Suivi temps</a></li>
								<?php if ($is_super_admin) { ?>
									<li><a data-toggle="tab" href="#tab3">Log</a></li>
								<?php } ?>
							</ul>

							<div class="tab-content row" style="margin-top: 20px;">
								<div id="tab1" class="tab-pane fade in active">
									<div class="col-md-6">
										<div class="btn-group" data-toggle="buttons">
											<label class="btn btn-primary btn-vert <?php if ($lstInfoProjets['is_actif'] != "0" ) { echo "active"; } ?>"><input type="radio" name="options" id="option1" value="1"   autocomplete="off" <?php if ($lstInfoProjets['is_actif'] != "0" ) { echo "checked"; } ?>>Actif</label>
											<label class="btn btn-primary btn-rouge <?php if ($lstInfoProjets['is_actif'] == "0" ) { echo "active"; } ?>"><input type="radio" name="options" id="option2" value="0"   autocomplete="off" <?php if ($lstInfoProjets['is_actif'] == "0" ) { echo "checked"; } ?>>Inactif</label>
										</div>
										<h4>Projet</h4>
										<label>Nom :<sup class="red">*</sup></label> <input type="text" id="nomProjet" name="nomProjet" value="<?php echo $lstInfoProjets['nom']; ?>" class="brl_input" placeholder="Nom du projet"  autocomplete="off"><br>
										<label>Client :<sup class="red">*</sup></label> <select name="selectClients" id="selectClients" class="brl_input">
										<?php foreach ($result as $infoTmp) { ?>
											<option value="<?php echo $infoTmp['id']; ?>" <?= ($infoTmp['id'] == $lstInfoProjets['id_client']) ? "selected='selected'" : ''; ?>><?php echo $infoTmp['nom']; ?>
										<?php }  ?>
										</select> <a href="<?php echo _URL_BASE_; ?>/clientsvue/<?php echo $lstInfoProjets['id_client']; ?>" target="_blank"><i class="fa fa-user"></i></a><br>
										<label>Description :</label><br><textarea rows="6" id="descriptionProjet" name="descriptionProjet" class="brl_input" placeholder="Description du projet"  autocomplete="off"><?php echo $lstInfoProjets['description']; ?></textarea><br>
										<label>Commentaire :</label><br><textarea rows="6" id="commentaireProjet" name="commentaireProjet" class="brl_input" placeholder="Commentaire sur le projet"  autocomplete="off"><?php echo $lstInfoProjets['commentaire']; ?></textarea><br>
										<?php if ($is_admin || $is_super_admin) { ?>
											<label>Commentaire Admin :</label><br><textarea rows="6" id="commentaireAdmin" name="commentaireAdmin" class="brl_input" placeholder="Commentaire de l'administrateur sur le projet"  autocomplete="off"><?php echo $lstInfoProjets['commentaire_admin']; ?></textarea><br>
										<?php } ?>
									</div>
									<div class="col-md-6">
										<div class="btn-group" data-toggle="buttons">
											<label class="btn btn-primary btn-vert <?php if ($lstInfoProjets['is_suivi'] != "0" ) { echo "active"; } ?>"><input type="radio" name="options_suivi" id="option1" value="1"   autocomplete="off" <?php if ($lstInfoProjets['is_suivi'] != "0" ) { echo "checked"; } ?>>Suivi</label>
											<label class="btn btn-primary btn-rouge <?php if ($lstInfoProjets['is_suivi'] == "0" ) { echo "active"; } ?>"><input type="radio" name="options_suivi" id="option2" value="0"   autocomplete="off" <?php if ($lstInfoProjets['is_suivi'] == "0" ) { echo "checked"; } ?>>Non suivi</label>
										</div>
										<h4>Suivi</h4>
										<label>Temps estimé :<sup class="red">*</sup></label> <input type="number" min="0" step="1" id="nbreHeureEstime" name="nbreHeureEstime" value="<?php echo $lstInfoProjets['nbre_heure_estime']; ?>" class="brl_input" placeholder="En heures"  autocomplete="off"><br>
										<label>Pourcentage réalisé :<sup class="red">*</sup></label> <input type="number" min="0" max="100" step="1" id="pourcentageRealise" name="pourcentageRealise" value="<?php echo $lstInfoProjets['prct_avancement']; ?>" class="brl_input" placeholder="0 à 100"  autocomplete="off"><br>
										<h4>Informations projet</h4>
										<label>Type de projet :</label> <select id="idTypeProjet" name="idTypeProjet" class="brl_input">
											<option value="0">Sélectionnez un type de projet</option>
											<?php foreach ($lstTypeProjet as $infoTmp) { ?>
												<option value="<?php echo $infoTmp['id']; ?>" <?php if(isset($lstInfoProjets['type_projet']) && $lstInfoProjets['type_projet'] == $infoTmp['id']) { echo "selected"; } ?>> <?php echo $infoTmp['nom']; ?> </option>
											<?php } ?>
										</select>
										<br />
										<label>Lien du BO :</label> <input type="text" id="lienBO" name="lienBO" value="<?php echo $lstInfoProjets['lien_bo']; ?>" class="brl_input" placeholder="Lien du BO"  autocomplete="off"><br>
										<label>Lien Report :</label> <input type="text" id="report" name="report" value="<?php echo $lstInfoProjets['report']; ?>" class="brl_input" placeholder="Lien vers le fichier report"  autocomplete="off"><br>
										<label>Commentaire MAJ:</label><br><textarea rows="6" id="commentaireMAJ" name="commentaireMAJ" class="brl_input" placeholder="Commentaire sur la mise à jour du site"  autocomplete="off"><?php echo $lstInfoProjets['commentaire_maj']; ?></textarea><br>
									</div>
								</div>
								<div id="tab2" class="tab-pane fade">
									<div class="col-md-4">
										<h4>Non facturé</h4>
										<p>Nombre de suivi : <?= $nbre_suivi_nonfacture; ?></p>
										<p>Minutes : <?= $nbre_minutes_nonfacture; ?></p>
										<p>Heures : <?= round($nbre_minutes_nonfacture / 60, 2); ?></p>
									</div>
									<div class="col-md-4">
										<h4>Facturé</h4>
										<p>Nombre de suivi : <?= $nbre_suivi_facture; ?></p>
										<p>Minutes : <?= $nbre_minutes_facture; ?></p>
										<p>Heures : <?= round($nbre_minutes_facture / 60, 2); ?></p>
									</div>
									<div class="col-md-4">
										<h4>Total</h4>
										<p>Nombre de suivi : <?= $nbre_suivi_nonfacture + $nbre_suivi_facture; ?></p>
										<p>Minutes : <?= $nbre_minutes_nonfacture + $nbre_minutes_facture; ?></p>
										<p>Heures : <?= round($nbre_minutes_nonfacture / 60, 2) + round($nbre_minutes_facture / 60, 2); ?></p>
									</div>
								</div>
								<?php if ($is_super_admin) { ?>
								<div id="tab3" class="tab-pane fade">
									<div>
										<div style="width: 100%;" class="log-scroll">
											<?php $listeLog = explode("\\n",$lstLog['logUser']); 
											// echo '<pre>'; print_r ($listeLog); echo '</pre>';
											foreach ($listeLog as $infoTmp) {
												$liste=explode("\n", $infoTmp);
												foreach ($liste as $valeur) {?>
													<label><?php echo $valeur; ?></label><br>
												<?php } 
											}?>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
							<p class="clear">&nbsp;</p>						

							<script type="text/javascript">
								
							function submitform()
							{
								var formOk = true;
								var champNom = $("#nomProjet");
								champNom.css("background-color", "initial");
								
								// Test champ Mail
								if (champNom.val() == "") {
									champNom.css("background-color", "red");
									alert("Erreur de saisie");
									formOk = false;
								}
								
								// Test form
								if (formOk) {
									document.form.submit();
									return true;
								} else {
									alert("Informations manquantes sur le projet");
									return false;
								}
								
							}
							</script>
						</form>
						
						</div>
					</div>
				</div>
				<p class="clear">&nbsp;</p>
				
				<div>
					<div class="bouton2 clear" style="text-align: center;">
						<a href="<?php echo _URL_BASE_; ?>/projets">ANNULER</a>
						<a href="javascript:submitform();" class="btn-green-save">ENREGISTRER</a>
					</div>
					<input type="submit" style="display:none">
				</div>
					
					<p class="clear">&nbsp;</p>
<?php

}

/* Page Liste des prospects */
function displayPageContenuLstProspects($option) {

	//Recherche
	// Récupération de la recherche
	if(isset($_POST['rechercheAdh'])) {
		$_SESSION['SEARCH']['ADHERENTS'] = $_POST['rechercheAdh'];
		$option = "page-1";
	}
	$filtreAdh = (isset($_SESSION['SEARCH']['ADHERENTS'])) ? $_SESSION['SEARCH']['ADHERENTS'] : '';
		
	// Pagination
	if(isset($option) && !empty($option)) { 
		list($actionPage, $numPage) = explode('-', $option);
		if ($actionPage == 'page' && ((int)$numPage) > 0 ) {
			$_SESSION['PAGE']['PROSPECTS'] = $numPage;
		}
	}
	$numPage = (isset($_SESSION['PAGE']['PROSPECTS'])) ? $_SESSION['PAGE']['PROSPECTS'] : 1; 
	//compteur
	$compteur = brl::getCompteurProspects();
	
	$lstInfoProspects   = brl::getLstProspects($numPage, $filtreAdh);
	$lstStatut 				= brl::getLstStatuts();
	
	
	
?>
				<!-- RECHERCHE -->
				<div class="brl-panel-bloc100" id="bloc_search">
					<div class="brl-lst">
						<form method="post" action="" id="filtreAdh">
							<div>
								<input type="text" id="nom" name="rechercheAdh[nom][]" class="brl_input-l" placeholder="Nom du Prospect"  autocomplete="off">
								<select id="statut" name="rechercheAdh[statut][]" class="brl_input-l selectpicker" title="Statut"><!-- RECHERCHE STATUT -->
									<option value="10" <?= ((isset($filtreAdh['statut']) && $filtreAdh['statut'][0] == 10) || !isset($filtreAdh['statut']) && 1 == 10) ? 'selected' : '' ; ?>>Tout
									<?php foreach ($lstStatut as $infoTmp) { ?>
										<option value="<?php echo $infoTmp['id']; ?>" <?= ((isset($filtreAdh['statut']) && $filtreAdh['statut'][0] == $infoTmp['id']) || !isset($filtreAdh['statut']) && 1 == $infoTmp['id']) ? 'selected' : '' ; ?>><?php echo $infoTmp['intitule']; ?>
									<?php }  ?>
								</select>		
								<button type="submit" onclick="submit()" class="brl_input-s btn btn-outline-primary" style="float: right;">Rechercher</button>
							</div>
						</form>
					</div>
				</div>
				<div class="brl-panel-bloc100">
					<div class="titre-bloc-b">
						<div class="brl-title">Liste des Prospects</div>
						<div class="bouton3 clear" style="float: right;">
							<a href="<?php echo _URL_BASE_; ?>/prospectsvue/0"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter</a>
						</div>
					</div>
					<div class="brl-lst">
					<table style="width:100%" class="brlselectable"> <!-- Affichage des fiches en tableau-->
								<tr class="brl-lst-el">
									<th> Nom </th>
									<th> Statut </th>
									<th> Date </th>
								</tr>
									<?php foreach ($lstInfoProspects as $infoTmp) { ?>
										<tr class="brl-lst-el main">
											<td>
												<a href="<?php echo _URL_BASE_; ?>/prospectsvue/<?php echo $infoTmp['id']; ?>"><?php echo $infoTmp['nom']; ?></a>
											</td>
											<td>
												<?php echo brl::getStatut($infoTmp['statut']); ?>
											</td>
											<td>
												<?php echo $infoTmp['date']; ?>
											</td>

										<div class="clear"></div>

										</tr>
									<?php }  ?>
							</table> <!--fin de l'affichage-->
					</div>
						<div> &nbsp; </div>
<?php
	brl::displayPagination($_GET['action'], $numPage, $compteur['nbrElm']);
?>

			</div>
<?php

}

function displayPageContenuProspects ($idProspect) {

	$lstInfoProspect 		= brl::getProspect($idProspect);
	$lstStatut 				= brl::getLstStatuts();

	$titre = 'Modifier le Prospect';
	if (empty($idProspect)) { $titre = 'Ajouter le Prospect'; }
	
	//Récupération du droit d'administateur
	$is_admin=$_SESSION["USER"]["IS_ADMIN"];
	$is_super_admin=$_SESSION["USER"]["IS_SUPER_ADMIN"];
	
?>
				<div class="brl-panel-bloc70 brl-panel-bloc-centre">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><?php echo $titre; ?></div>
							<div class="bouton5 clear" style="float: right;">
							<?php if (!empty($idProspect)) { ?><a href="<?php echo _URL_BASE_; ?>/prospectsrm/<?php echo $idProspect; ?>" onclick="if(window.confirm('Voulez-vous vraiment supprimer ?')){return true;}else{return false;}"><i class="fa fa-times" aria-hidden="true"></i> Supprimer</a><?php } ?>
							</div>
						</div>
						<div id="idPageContenuCtrIntere" style="width: 100%;">
											
						<form method="post" action="<?php echo _URL_BASE_; ?>/prospectssav/<?php echo $idProspect; ?>" id="formProspect" name="formProspect" style="padding: 20px 0;" autocomplete="off">
						
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#tab1">Détails</a></li>
								<li><a data-toggle="tab" href="#tab2">Suivi</a></li>
							</ul>

							<div class="tab-content row" style="margin-top: 20px;">
								<div id="tab1" class="tab-pane fade in active">
									<div class="col-md-12">
										<h4>Prospect</h4>
										<div class="col-md-6">
										<label>Nom :<sup class="red">*</sup></label> <input type="text" id="nomProspect" name="nomProspect" value="<?php echo $lstInfoProspect['nom']; ?>" class="brl_input" placeholder="Nom du Prospect"  autocomplete="off"><br>
										<label>Statut :<sup class="red">*</sup></label> <select name="selectStatut" id="selectStatut" class="brl_input">
										<?php foreach ($lstStatut as $infoTmp) { ?>
											<option value="<?php echo $infoTmp['id']; ?>" <?= ($infoTmp['id'] == $lstInfoProspect['statut']) ? "selected='selected'" : ''; ?>><?php echo $infoTmp['intitule']; ?>
										<?php }  ?>
										</select><br>
										<label>Référence de devis :</label> <input type="text" id="refDevis" name="refDevis" value="<?php echo $lstInfoProspect['ref_devis']; ?>" class="brl_input" placeholder="Référence du devis"  autocomplete="off"><br>
										<label>Date :</label> <input type="date" id="date" name="date" value="<?php echo $lstInfoProspect['date']; ?>" class="brl_input"  autocomplete="off"><br>
										</div>
										<div class="col-md-6">
											<label>Date d'alerte :</label> <input type="date" id="dateRelance" name="dateRelance" value="<?php echo $lstInfoProspect['date_relance']; ?>" class="brl_input"  autocomplete="off"><br>
											<label>Commentaire alerte :</label> <input type="text" id="commentaireRelance" name="commentaireRelance" value="<?php echo $lstInfoProspect['commentaire_relance']; ?>" class="brl_input" placeholder="Commentaire relance"  autocomplete="off"><br>
										</div>
										<div class="col-md-12">
											<label>Contact :</label><br><textarea rows="6" id="contactProspect" name="contactProspect" class="brl_input" placeholder="Contact du Prospect"  autocomplete="off"><?php echo $lstInfoProspect['contact']; ?></textarea><br>
											<label>Commentaire :</label><br><textarea rows="6" id="commentaire" name="commentaire" class="brl_input" placeholder="Commentaire"  autocomplete="off"><?php echo $lstInfoProspect['commentaire']; ?></textarea><br>
										</div>
									</div>
									<div class="col-md-12">
										<?php if(!empty($idProspect) && empty(brl::getClientByIdProspect($idProspect))) { ?>
											<a href="<?php echo _URL_BASE_; ?>/prospecttoclient/<?= $idProspect; ?>" style="padding: 5px 10px;text-decoration: none;display: inline-block;margin-top: 15px;background-color: #008dd1 !important;color: #fff;">Transformer en client</a>
										<?php } else if(!empty($idProspect) && !empty(brl::getClientByIdProspect($idProspect))) { ?>
											<a href="<?php echo _URL_BASE_; ?>/clientsvue/<?= brl::getClientByIdProspect($idProspect); ?>" style="padding: 5px 10px;text-decoration: none;display: inline-block;margin-top: 15px;background-color: #008dd1 !important;color: #fff;">Voir la fiche client</a>
										<?php } ?>
									</div>
								</div>
								<div id="tab2" class="tab-pane fade">
									<?php if(!empty($idProspect)) { ?>
									<div class="col-md-12">
										<div id="formSuiviProspect">
										<?= brl_tools::returnFormSuiviProspect(''); ?>
										<?= brl_tools::returnTableSuiviProspect($idProspect); ?>
										</div>
										<!--h4>Suivi du prospect</h4>
										<label>Date :</label> <input type="date" id="dateSuiviProspect" name="dateSuiviProspect" class="brl_input"  autocomplete="off"><br>
										<label>Label :</label> <input type="text" id="labelSuiviProspect" name="labelSuiviProspect" class="brl_input" placeholder="Label"  autocomplete="off"><br>
										<label>Commentaire :</label> <input type="text" id="commentaireSuiviProspect" name="commentaireSuiviProspect" class="brl_input-l" placeholder="Commentaire" autocomplete="off"><br>
										<button id="insertSuiviProspect"><i class="fa fa-plus"></i> Ajouter le suivi</button-->
									</div>
									<?php } ?>
								</div>
							</div>
							<p class="clear">&nbsp;</p>						

							<script type="text/javascript">

							function submitform()
							{
								var formOk = true;
								var champNom = $("#nomProspect");
								champNom.css("background-color", "initial");
								
								// Test champ Mail
								if (champNom.val() == "") {
									champNom.css("background-color", "red");
									alert("Erreur de saisie");
									formOk = false;
								}
								
								// Test form
								if (formOk) {
									document.formProspect.submit();
									return true;
								} else {
									alert("Informations manquantes sur le prospect");
									return false;
								}
								
							}

							$(document).on('click','.update_row_suivi_prospect',function(e) {
								var id = $(this).attr("data-id");
								$.ajax({
										url: "<?php echo _URL_BASE_; ?>/suiviprospect-get/" + id,
										data: "",
										type: "post",
										success: function(code_html, statut){
											$( "#formSuiviProspect .inputs_form" ).empty();
											$(code_html).appendTo("#formSuiviProspect .inputs_form");
										}
								});
							});

							// Ajout suivi
							$(document).on('click','#insertSuiviProspect',function(e) {
								var data = $("#formProspect").serialize();
								e.preventDefault();
								$.ajax({
										data: data,
										type: "post",
										url: "<?php echo _URL_BASE_; ?>/suiviprospect-add/<?= $idProspect; ?>",
										success: function(code_html, statut){
											$( "#formSuiviProspect" ).empty();
											$(code_html).appendTo("#formSuiviProspect");
										}
								});
							});

							$(document).on('click','#updateSuiviProspect',function(e) {
								var data = $("#formProspect").serialize();
								var id = $(this).attr("data-id");
								e.preventDefault();
								$.ajax({
										data: data,
										type: "post",
										url: "<?php echo _URL_BASE_; ?>/suiviprospect-update/<?=  $idProspect . '&idSuiviProspect='; ?>" + id,
										success: function(code_html, statut){
											$( "#formSuiviProspect" ).empty();
											$(code_html).appendTo("#formSuiviProspect");
										}
								});
							});

							

							$(document).on('click','.delete_row_suivi_prospect',function(e) {
								if(confirm('Etes-vous sûr de vouloir supprimer ce suivi?')) {
									var id = $(this).attr("data-id");
									var data = $("#formProspect").serialize();
									$.ajax({
											url: "<?php echo _URL_BASE_; ?>/suiviprospect-rm/" + id + "&idProspect=<?= $idProspect; ?>",
											data: data,
											type: "post",
											success: function(code_html, statut){
												$( "#formSuiviProspect" ).empty();
												$(code_html).appendTo("#formSuiviProspect");
											}
									});
								}
							});
							</script>
						</form>
						
						</div>
					</div>
				</div>
				<p class="clear">&nbsp;</p>
				
				<div>
					<div class="bouton2 clear" style="text-align: center;">
						<a href="<?php echo _URL_BASE_; ?>/prospects">ANNULER</a>
						<a href="javascript:submitform();" class="btn-green-save">ENREGISTRER</a>
					</div>
					<input type="submit" style="display:none">
				</div>
					
					<p class="clear">&nbsp;</p>
<?php

}

/* Page Liste des Clients */
function displayPageContenuLstClients($option) {
		
	// Pagination
	if(isset($option) && !empty($option)) { 
		list($actionPage, $numPage) = explode('-', $option);
		if ($actionPage == 'page' && ((int)$numPage) > 0 ) {
			$_SESSION['PAGE']['CLIENTS'] = $numPage;
		}
	}
	$numPage = (isset($_SESSION['PAGE']['CLIENTS'])) ? $_SESSION['PAGE']['CLIENTS'] : 1; 
	//compteur
	$compteur = brl::getCompteurClients();
	
	$lstInfoClients   = brl::getLstClients($numPage);
	
	
	
?>
				<!--div class="brl-panel-bloc100">
					<div class="brl-row left" style="text-align: center; padding-top: 40px;">
					</div>
				</div-->

				<div class="brl-panel-bloc100">
					<div class="titre-bloc-b">
						<div class="brl-title">Liste des Clients</div>
						<div class="bouton3 clear" style="float: right;">
							<a href="<?php echo _URL_BASE_; ?>/clientsvue/0"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter</a>
						</div>
					</div>
					<div class="brl-lst">
						<table style="width:100%"> <!-- Affichage des fiches en tableau-->
							<tr class="brl-lst-el">
								<th> Client </th>
								<th style="width: 150px;"> Statut </th>
							</tr>
							<?php foreach ($lstInfoClients as $infoTmp) { ?>
							<tr class="brl-lst-el main">
								<td>
									<a href="<?php echo _URL_BASE_; ?>/clientsvue/<?php echo $infoTmp['id']; ?>"><?php echo $infoTmp['nom']; echo ($infoTmp['type'] == 0) ? " (Client)" : " (Prospect)"; ?></a>
								</td>
								<td>
									<?php if($infoTmp['is_actif']=="1") { echo '<div class="badgeVert">Actif</div>'; } else { echo '<div class="badgeRouge">Inactif</div>'; } ?>
								</td>
								<div class="clear"></div>
							</tr>
							<?php }  ?>
						</table>
					</div>
						<div> &nbsp; </div>
<?php
	brl::displayPagination($_GET['action'], $numPage, $compteur['nbrElm']);
?>

			</div>
<?php

}


function displayPageContenuClients ($idClients) {

	$lstInfoClients  	= brl::getClients($idClients);
	// echo "<pre>"; print_r($lstInfoClients);echo "</pre>";
	$lstProjetsClients  = brl::getProjetClient($idClients);
	// echo "<pre>"; print_r($lstProjetsClients);echo "</pre>";exit ;
	$lstLog 			= brl::getLogClients($idClients);

	$titre = 'Modifier le Client';
	if (empty($idClients)) { $titre = 'Ajouter le Client'; }
	
	//Récupération du droit d'administateur
	$is_admin=$_SESSION["USER"]["IS_ADMIN"];
	$is_super_admin=$_SESSION["USER"]["IS_SUPER_ADMIN"];
	//echo "<pre>"; print_r($lstInfoUser); echo "</pre>";
	
?>
				<div class="brl-panel-bloc70 brl-panel-bloc-centre">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><?php echo $titre; ?></div>
							<div class="bouton5 clear" style="float: right;">
							<?php if (!empty($idClients)) { ?><a href="<?php echo _URL_BASE_; ?>/clientsrm/<?php echo $idClients; ?>" onclick="if(window.confirm('Voulez-vous vraiment supprimer ?')){return true;}else{return false;}"><i class="fa fa-times" aria-hidden="true"></i> Supprimer</a><?php } ?>
							</div>
						</div>
						<div id="idPageContenuClients" style="width: 100%;">
											
						<form method="post" action="<?php echo _URL_BASE_; ?>/clientssav/<?php echo $idClients; ?>" name="formClients" style="padding: 20px 0;" autocomplete="off">

							<div class="tab-content" style="margin-top: 20px;">
							<div class="col-md-6">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-primary btn-vert <?php if ($lstInfoClients['is_actif'] != "0" ) { echo "active"; } ?>"><input type="radio" name="options" id="option1" value="1"   autocomplete="off" <?php if ($lstInfoClients['is_actif'] != "0" ) { echo "checked"; } ?>>Actif</label>
									<label class="btn btn-primary btn-rouge <?php if ($lstInfoClients['is_actif'] == "0" ) { echo "active"; } ?>"><input type="radio" name="options" id="option2" value="0"   autocomplete="off" <?php if ($lstInfoClients['is_actif'] == "0" ) { echo "checked"; } ?>>Inactif</label>
								</div>
								</div>
								<div class="col-md-6 brl-dispo-right">
								<?php if(!empty($lstInfoClients['id_prospect'])) { ?>
									<a href="<?php echo _URL_BASE_; ?>/prospectsvue/<?= $lstInfoClients['id_prospect'] ?>" style="padding: 5px 10px;text-decoration: none;display: inline-block;margin-top: 15px;background-color: #008dd1 !important;color: #fff;">Voir la fiche prospect</a>
								<?php } ?>
								<select name="selectProjets" id="selectProjets" class="brl_input">
									<option value="0">Liste des projets</option>
									<?php foreach ($lstProjetsClients as $infoTmp) { 
										if ($infoTmp['id_client'] == $lstInfoClients['id']) {?>
											<option value="<?php echo $infoTmp['id']; ?>"><?php echo $infoTmp['nom']; ?></option>
										<?php } 
									}  ?>
								</select>
								<a href="<?php echo _URL_BASE_; ?>/projetsvue/<?php echo $lstProjetsClients['id']; ?>" target="_blank"><?php echo $lstProjetsClients['nom'] ?></a><br>
								</div>
								<div class="col-md-6">
									<label>Label :<sup class="red">*</sup></label> <input type="text" id="nomClients" name="nomClients" value="<?php echo $lstInfoClients['nom']; ?>" class="brl_input" placeholder="Nom du client"  autocomplete="off"><br>
									<label>Contact :</label><br><textarea rows="5" id="contactClients" name="contactClients" class="brl_input" placeholder="Contact du client"  autocomplete="off" style="width: 81%;"><?php echo $lstInfoClients['contact']; ?></textarea><br>
									<label>Description :</label><br><textarea rows="5" id="descriptionClients" name="descriptionClients" class="brl_input" placeholder="Description du client"  autocomplete="off" style="width: 81%;"><?php echo $lstInfoClients['description']; ?></textarea><br>
								</div>
								<div class="col-md-6">
									<label>Société :<sup class="red">*</sup></label> <input type="text" id="nomClients" name="nomClients" value="<?php echo $lstInfoClients['nom']; ?>" class="brl_input" placeholder="Nom du client"  autocomplete="off"><br>
									<label>Adresse de facturation :</label><br><textarea rows="5" id="adresse_facturation" name="adresse_facturation" class="brl_input" placeholder="Adresse de facturation"  autocomplete="off" style="width: 81%;"><?php echo $lstInfoClients['adresse_facturation']; ?></textarea><br>
									<label>Contrat :</label> 
									<select name="selectContrat" id="selectContrat" class="brl_input">
										<option value="1" <?= ($lstInfoClients['contrat'] == '1') ? "selected='selected'" : ''; ?>>Sans contrat</option>
										<option value="2" <?= ($lstInfoClients['contrat'] == '2') ? "selected='selected'" : ''; ?>>Contrat de maintenance</option>
									</select><br>
									<label>Durée du contrat :</label> <input type="number" min="0" step="0.01" id="dureeContrat" name="dureeContrat" value="<?php echo $lstInfoClients['duree_contrat']; ?>" class="brl_input" placeholder="Durée du contrat"  autocomplete="off"><br>
									<label>Label du contrat :</label> <input type="text" id="labelContrat" name="labelContrat" value="<?php echo $lstInfoClients['label_contrat']; ?>" class="brl_input" placeholder="Label du contrat" autocomplete="off"><br>
								</div>
								<div class="col-md-12">
									<label>Commentaire :</label><br><textarea rows="12" id="commentaireClients" name="commentaireClients" class="brl_input" placeholder="Commentaire du client"  autocomplete="off"><?php echo $lstInfoClients['commentaire']; ?></textarea><br>
									<?php if ($is_admin || $is_super_admin) { ?>
										<label>Commentaire Admin :</label><br><textarea rows="6" id="commentaireAdmin" name="commentaireAdmin" class="brl_input" placeholder="Commentaire de l'administrateur sur le projet"  autocomplete="off"><?php echo $lstInfoClients['commentaire_admin']; ?></textarea><br>
									<?php } ?>
								</div>
								
							</div>
							<p class="clear">&nbsp;</p>						

							<script type="text/javascript">
								
							function submitform()
							{
								var formOk = true;
								var champNom = $("#nomClients");
								champNom.css("background-color", "initial");
								
								// Test champ Mail
								if (champNom.val() == "") {
									champNom.css("background-color", "red");
									alert("Erreur de saisie");
									formOk = false;
								}
								
								// Test form
								if (formOk) {
									document.formClients.submit();
									return true;
								} else {
									alert("Informations manquantes sur l'activité");
									return false;
								}
								
							}
							</script>
						</form>
						
						</div>
					</div>
				</div>
				<p class="clear">&nbsp;</p>
				
				<div>
					<div class="bouton2 clear" style="text-align: center;">
						<a href="<?php echo _URL_BASE_; ?>/clients">ANNULER</a>
						<a href="javascript:submitform();" class="btn-green-save">ENREGISTRER</a>
					</div>
					<input type="submit" style="display:none">
				</div>
					
					<p class="clear">&nbsp;</p>
					<?php if ($is_super_admin) { ?>
					
					<div class="brl-panel-bloc70 brl-panel-bloc-centre">
						<div>
							<div class="titre-bloc-b">
								<div class="brl-title">Log</div>
							</div>
							<div style="width: 100%;" class="log-scroll">
								<div class="tab-content" style="margin-top: 20px;">
									<?php $listeLog = explode("\\n",$lstLog['logUser']); 
									// echo '<pre>'; print_r ($listeLog); echo '</pre>';
									foreach ($listeLog as $infoTmp) {
										$liste=explode("\n", $infoTmp);
										foreach ($liste as $valeur) {?>
										<label><?php echo $valeur; ?></label><br>
									<?php } 
									}?>
								</div>
							</div>
						</div>
					</div>

				<?php } ?>
<?php

}

/*Page Liste des types de presta */
function displayPageContenuLstTypePresta($option) {
		
	// Pagination
	if(isset($option) && !empty($option)) { 
		list($actionPage, $numPage) = explode('-', $option);
		if ($actionPage == 'typepresta' && ((int)$numPage) > 0 ) {
			$_SESSION['PAGE']['TYPE_PRESTA'] = $numPage;
		}
	}
	$numPage = (isset($_SESSION['PAGE']['TYPE_PRESTA'])) ? $_SESSION['PAGE']['TYPE_PRESTA'] : 1; 

	//compteur
	$compteur = brl::getCompteurTypePresta();
	
	$lstInfoTypePresta   = brl::getLstTypePresta($numPage);
	
?>
				<!--div class="brl-panel-bloc100">
					<div class="brl-row left" style="text-align: center; padding-top: 40px;">
					</div>
				</div-->

				<div class="brl-panel-bloc100">
					<div class="titre-bloc-b">
						<div class="brl-title">Type de prestation</div>
						<div class="bouton3 clear" style="float: right;">
							<a href="<?php echo _URL_BASE_; ?>/typeprestavue/0"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter</a>
						</div>
					</div>
					<div class="brl-lst">
						<?php foreach ($lstInfoTypePresta as $infoTmp) { ?>
						<div class="brl-lst-el">
							<div class="floatleft bold">
								<a href="<?php echo _URL_BASE_; ?>/typeprestavue/<?php echo $infoTmp['id']; ?>"><?php echo $infoTmp['nom']; ?></a>
							</div>
							<div class="clear"></div>
						</div>
						<?php }  ?>
					</div>
						<div> &nbsp; </div>
<?php
	brl::displayPagination($_GET['action'], $numPage, $compteur['nbrElm']);
?>

			</div>
<?php

}


function displayPageContenuTypePresta ($idTypePresta) {

	$lstInfoTypePresta = brl::getTypePresta($idTypePresta);
	$lstLog		 = brl::getLogTypePresta($idTypePresta);

	$titre = 'Modifier le type de prestation';
	if (empty($idTypePresta)) { $titre = 'Ajouter le type de prestation'; }
	
	//Récupération du droit d'administateur
	$is_admin=$_SESSION["USER"]["IS_ADMIN"];
	$is_super_admin=$_SESSION["USER"]["IS_SUPER_ADMIN"];
	//echo "<pre>"; print_r($lstInfoUser); echo "</pre>";
	
?>
				<div class="brl-panel-bloc70 brl-panel-bloc-centre">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><?php echo $titre; ?></div>
							<div class="bouton5 clear" style="float: right;">
							<?php if (!empty($idTypePresta)) { ?><a href="<?php echo _URL_BASE_; ?>/typeprestarm/<?php echo $idTypePresta; ?>" onclick="if(window.confirm('Voulez-vous vraiment supprimer ?')){return true;}else{return false;}"><i class="fa fa-times" aria-hidden="true"></i> Supprimer</a><?php } ?>
							</div>
						</div>
						<div id="curve_chart" style="width: 100%;">
											
						<form method="post" action="<?php echo _URL_BASE_; ?>/typeprestasav/<?php echo $idTypePresta; ?>" name="form" style="padding: 20px 0;" autocomplete="off">
						


							<div class="tab-content" style="margin-top: 20px;">
								<h4>Type de prestation</h4>
								<label>Nom :<sup class="red">*</sup></label> <input type="text" id="nomTypePresta" name="nomTypePresta" value="<?php echo $lstInfoTypePresta['nom']; ?>" class="brl_input" placeholder="nom de la prestation"  autocomplete="off"><br>
							</div>
							<p class="clear">&nbsp;</p>						

							<script type="text/javascript">
								
							function submitform()
							{
								var formOk = true;
								var champNom = $("#nomTypePresta");
								champNom.css("background-color", "initial");
								
								// Test champ Mail
								if (champNom.val() == "") {
									champNom.css("background-color", "red");
									alert("Erreur de saisie");
									formOk = false;
								}
								
								// Test form
								if (formOk) {
									document.form.submit();
									return true;
								} else {
									alert("Informations manquantes sur la prestation");
									return false;
								}
								
							}
							</script>
						</form>
						
						</div>
					</div>
				</div>
				<p class="clear">&nbsp;</p>
				
				<div>
					<div class="bouton2 clear" style="text-align: center;">
						<a href="<?php echo _URL_BASE_; ?>/typepresta">ANNULER</a>
						<a href="javascript:submitform();" class="btn-green-save">ENREGISTRER</a>
					</div>
					<input type="submit" style="display:none">
				</div>
					
					
					<p class="clear">&nbsp;</p>
					<?php if ($is_super_admin) { ?>
					
					<div class="brl-panel-bloc70 brl-panel-bloc-centre">
						<div>
							<div class="titre-bloc-b">
								<div class="brl-title">Log</div>
							</div>
							<div id="curve_chart" style="width: 100%;" class="log-scroll">
								<div class="tab-content" style="margin-top: 20px;">
									<?php $listeLog = explode("\\n",$lstLog['logUser']); 
									// echo '<pre>'; print_r ($listeLog); echo '</pre>';
									foreach ($listeLog as $infoTmp) {
										$liste=explode("\n", $infoTmp);
										foreach ($liste as $valeur) {?>
										<label><?php echo $valeur; ?></label><br>
									<?php } 
									}?>
								</div>
							</div>
						</div>
					</div>

				<?php } ?>
<?php

}

/*Page Liste des types de projet */
function displayPageContenuLstTypeProjet($option) {
		
	// Pagination
	if(isset($option) && !empty($option)) { 
		list($actionPage, $numPage) = explode('-', $option);
		if ($actionPage == 'typeprojet' && ((int)$numPage) > 0 ) {
			$_SESSION['PAGE']['TYPE_PROJET'] = $numPage;
		}
	}
	$numPage = (isset($_SESSION['PAGE']['TYPE_PROJET'])) ? $_SESSION['PAGE']['TYPE_PROJET'] : 1; 

	//compteur
	$compteur = brl::getCompteurTypeProjet();
	
	$lstInfoTypeProjet   = brl::getLstTypeProjet($numPage);
	
?>
				<!--div class="brl-panel-bloc100">
					<div class="brl-row left" style="text-align: center; padding-top: 40px;">
					</div>
				</div-->

				<div class="brl-panel-bloc100">
					<div class="titre-bloc-b">
						<div class="brl-title">Type de projet</div>
						<div class="bouton3 clear" style="float: right;">
							<a href="<?php echo _URL_BASE_; ?>/typeprojetvue/0"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter</a>
						</div>
					</div>
					<div class="brl-lst">
						<?php foreach ($lstInfoTypeProjet as $infoTmp) { ?>
						<div class="brl-lst-el">
							<div class="floatleft bold">
								<a href="<?php echo _URL_BASE_; ?>/typeprojetvue/<?php echo $infoTmp['id']; ?>"><?php echo $infoTmp['nom']; ?></a>
							</div>
							<div class="clear"></div>
						</div>
						<?php }  ?>
					</div>
						<div> &nbsp; </div>
<?php
	brl::displayPagination($_GET['action'], $numPage, $compteur['nbrElm']);
?>

			</div>
<?php

}


function displayPageContenuTypeProjet ($idTypeProjet) {

	$lstInfoTypeProjet = brl::getTypeProjet($idTypeProjet);
	$lstLog		 = brl::getLogTypeProjet($idTypeProjet);

	$titre = 'Modifier le type de projet';
	if (empty($idTypeProjet)) { $titre = 'Ajouter le type de projet'; }
	
	//Récupération du droit d'administateur
	$is_admin=$_SESSION["USER"]["IS_ADMIN"];
	$is_super_admin=$_SESSION["USER"]["IS_SUPER_ADMIN"];
	//echo "<pre>"; print_r($lstInfoUser); echo "</pre>";
	
?>
				<div class="brl-panel-bloc70 brl-panel-bloc-centre">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><?php echo $titre; ?></div>
							<div class="bouton5 clear" style="float: right;">
							<?php if (!empty($idTypeProjet)) { ?><a href="<?php echo _URL_BASE_; ?>/typeprojetrm/<?php echo $idTypeProjet; ?>" onclick="if(window.confirm('Voulez-vous vraiment supprimer ?')){return true;}else{return false;}"><i class="fa fa-times" aria-hidden="true"></i> Supprimer</a><?php } ?>
							</div>
						</div>
						<div id="curve_chart" style="width: 100%;">
											
						<form method="post" action="<?php echo _URL_BASE_; ?>/typeprojetsav/<?php echo $idTypeProjet; ?>" name="form" style="padding: 20px 0;" autocomplete="off">
						


							<div class="tab-content" style="margin-top: 20px;">
								<h4>Type de projet</h4>
								<label>Nom :<sup class="red">*</sup></label> <input type="text" id="nomTypeProjet" name="nomTypeProjet" value="<?php echo $lstInfoTypeProjet['nom']; ?>" class="brl_input" placeholder="nom de la prestation"  autocomplete="off"><br>
							</div>
							<p class="clear">&nbsp;</p>						

							<script type="text/javascript">
								
							function submitform()
							{
								var formOk = true;
								var champNom = $("#nomTypeProjet");
								champNom.css("background-color", "initial");
								
								// Test champ Mail
								if (champNom.val() == "") {
									champNom.css("background-color", "red");
									alert("Erreur de saisie");
									formOk = false;
								}
								
								// Test form
								if (formOk) {
									document.form.submit();
									return true;
								} else {
									alert("Informations manquantes sur la prestation");
									return false;
								}
								
							}
							</script>
						</form>
						
						</div>
					</div>
				</div>
				<p class="clear">&nbsp;</p>
				
				<div>
					<div class="bouton2 clear" style="text-align: center;">
						<a href="<?php echo _URL_BASE_; ?>/typeprojet">ANNULER</a>
						<a href="javascript:submitform();" class="btn-green-save">ENREGISTRER</a>
					</div>
					<input type="submit" style="display:none">
				</div>
					
					
					<p class="clear">&nbsp;</p>
					<?php if ($is_super_admin) { ?>
					
					<div class="brl-panel-bloc70 brl-panel-bloc-centre">
						<div>
							<div class="titre-bloc-b">
								<div class="brl-title">Log</div>
							</div>
							<div id="curve_chart" style="width: 100%;" class="log-scroll">
								<div class="tab-content" style="margin-top: 20px;">
									<?php $listeLog = explode("\\n",$lstLog['logUser']); 
									// echo '<pre>'; print_r ($listeLog); echo '</pre>';
									foreach ($listeLog as $infoTmp) {
										$liste=explode("\n", $infoTmp);
										foreach ($liste as $valeur) {?>
										<label><?php echo $valeur; ?></label><br>
									<?php } 
									}?>
								</div>
							</div>
						</div>
					</div>

				<?php } ?>
<?php

}

/* Page fiche parametrage */
function displayPageContenuFactures() {

	// call the static function to bring all datas in configuration table
	$lstInfoPrestationDevis = brl::getConfiguration();

	$titre = 'Paramétrages';
?>
				<div class="brl-panel-bloc70 brl-panel-bloc-centre">
					<!-- TITLE -->
					<div class="titre-bloc-b">
						<div class="brl-title"><?php echo $titre; ?></div>
					</div>
					<!-- CONTENU -->
					<div id="idPageContenuFactures">	
						<!-- FORM -->
						<form method="post" action="<?php echo _URL_BASE_; ?>/facturessav/" name="formFactures" id="formFactures" autocomplete="off">
							<div class="tab-content">
								<div class="col-md-12">
									<h4>Facturation</h4>
									<div class="col-md-6">
										<label>Année de facturation :</label> <input type="text" id="annee_facturation" name="annee_facturation" value="<?php echo $lstInfoPrestationDevis['annee_facturation']['valeur']; ?>" class="brl_input" placeholder="Année de la facturation"  autocomplete="off"><br>
										<label>TVA :</label> <input type="text" id="tva" name="tva" step="0.01" class="brl_input" placeholder="TVA"  autocomplete="off" value="<?php echo $lstInfoPrestationDevis['tva']['valeur']; ?>"><br>
									</div>
									<div class="col-md-6">
										<label>Références légales :</label><br><textarea rows="5" id="references_legales" name="references_legales" class="brl_input" placeholder="Références légales"  autocomplete="off" value="<?php echo $lstInfoPrestationDevis['references_legales']['valeur']; ?>"><?php echo $lstInfoPrestationDevis['references_legales']['valeur']; ?></textarea><br>
									</div>
									<div class="col-md-6">
										<label>RIB :</label><br><textarea rows="5" id="rib" name="rib" class="brl_input" placeholder="RIB"  autocomplete="off" value="<?php echo $lstInfoPrestationDevis['rib']['valeur']; ?>"><?php echo $lstInfoPrestationDevis['rib']['valeur']; ?></textarea><br>
									</div>
									<div class="col-md-6">
										<label>Modalité de payement :</label><br><textarea rows="5" id="modalite_payement" name="modalite_payement" class="brl_input" placeholder="Modalité de payement"  autocomplete="off" value="<?php echo $lstInfoPrestationDevis['modalite_payement']['valeur']; ?>"><?php echo $lstInfoPrestationDevis['modalite_payement']['valeur']; ?></textarea><br>
									</div>
									<div class="col-md-12">
										<label>Footer de la facture :</label><br><textarea rows="5" id="footer_facture" name="footer_facture" class="brl_input" placeholder="Footer de la facture"  autocomplete="off" value="<?php echo $lstInfoPrestationDevis['footer_facture']['valeur']; ?>"><?php echo $lstInfoPrestationDevis['footer_facture']['valeur']; ?></textarea><br>
										<label>Date CGV :</label> <input type="text" id="date_cgv" name="date_cgv" class="brl_input" placeholder="Date CGV" value="<?php echo $lstInfoPrestationDevis['date_cgv']['valeur']; ?>"><br>
										<label>CGV :</label><br><textarea rows="5" id="cgv" name="cgv" class="brl_input" placeholder="CGV"  autocomplete="off" value="<?php echo $lstInfoPrestationDevis['cgv']['valeur']; ?>"><?php echo $lstInfoPrestationDevis['cgv']['valeur']; ?></textarea><br>
									</div>
								</div>
								<p class="clear">&nbsp;</p>	
								<div class="bouton2 clear">
									<a onclick="submitform()" class="btn-green-save">ENREGISTRER</a>
								</div>
								<input type="submit">				
							</div>
						</form>
					</div>
				</div>
				<p class="clear">&nbsp;</p>
				
<?php }

/* Page List of Prestations */
function displayPageContenuLstPrestations($option) {
		
	// Pagination
	if(isset($option) && !empty($option)) { 
		list($actionPage, $numPage) = explode('-', $option);
		if ($actionPage == 'page' && ((int)$numPage) > 0 ) {
			$_SESSION['PAGE']['PRESTATION'] = $numPage;
		}
	}
	$numPage = (isset($_SESSION['PAGE']['PRESTATION'])) ? $_SESSION['PAGE']['PRESTATION'] : 1; 
	//compteur
	$compteur = brl::getCompteurPrestations();
	
	$lstInfoPrestations   = brl::getLstPrestations($numPage);

?>
				<div class="brl-panel-bloc100">
					<div class="titre-bloc-b">
						<div class="brl-title">Liste des Prestations</div>
						<div class="bouton3 clear">
							<a href="<?php echo _URL_BASE_; ?>/prestationsvue/0"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter</a>
						</div>
					</div>
					<div class="brl-lst">
						<table>
							<tr class="brl-lst-el">
								<th> Label </th>
								<th> Titre </th>
								<th> Description </th>
								<th> Quantité </th>
								<th> Unité </th>
								<th> Prix Unitaire </th>
							</tr>
							<?php foreach ($lstInfoPrestations as $infoTmp) { ?>
							<tr class="brl-lst-el main">
								<td>
									<a href="<?php echo _URL_BASE_; ?>/prestationsvue/<?php echo $infoTmp['id']; ?>"><?php echo $infoTmp['label'];?></a>
								</td>
								<td>
									<?php echo $infoTmp['titre']; ?>
								</td>
								<td>
									<?php echo $infoTmp['description_presta']; ?>
								</td>
								<td>
									<?php echo nl2br($infoTmp['quantite']); ?>
								</td>
								<td>
									<?php echo $infoTmp['unite']; ?>
								</td>
								<td>
									<?php echo $infoTmp['prix_unitaire'];?> €
								</td>
								<div class="clear"></div>
							</tr>
							<?php }  ?>
						</table>
					</div>
						<div> &nbsp; </div>
<?php
	brl::displayPagination($_GET['action'], $numPage, $compteur['nbrElm']);
?>

			</div>
<?php

}

/* Page Fiche des Prestations */
function displayPageContenuPrestations($idPrestation) {
	
	// call the static function who return the list of prestations of the right id
	$lstInfoPrestations  = brl::getPrestations($idPrestation);

	$titre = 'Modifier la Prestation';
	if (empty($idPrestation)) { $titre = 'Ajouter la Prestation'; }
	
?>
	<div class="brl-panel-bloc70 brl-panel-bloc-centre">
		<div>
			<!-- TITLE -->
			<div class="titre-bloc-b">
				<div class="brl-title"><?php echo $titre; ?></div>
				<!-- BUTTON DELETE -->
				<div class="bouton5 clear">
				<?php if (!empty($idPrestation)) { ?><a href="<?php echo _URL_BASE_; ?>/prestationsrm/<?php echo $idPrestation; ?>" onclick="if(window.confirm('Voulez-vous vraiment supprimer ?')){return true;}else{return false;}"><i class="fa fa-times" aria-hidden="true"></i> Supprimer</a><?php } ?>
				</div>
			</div>
			<div id="idPageContenuPrestations">		
				<!-- FORM -->
				<form method="post" action="<?php echo _URL_BASE_; ?>/prestationssav/<?php echo $idPrestation; ?>" name="formPrestation" id="formPrestation" autocomplete="off">
					<div class="tab-content">
						<h4>Prestation</h4>
						<label>Label :<sup class="red">*</sup></label> <input type="text" id="label" name="label" value="<?php echo $lstInfoPrestations['label']; ?>" class="brl_input" placeholder="Label de la prestation"  autocomplete="off"><br>
						<label>Titre :</label> <input type="text" id="titre" name="titre" value="<?php echo $lstInfoPrestations['titre']; ?>" class="brl_input" placeholder="Titre de la prestation"  autocomplete="off"><br>
						<label>Description :</label><br><textarea rows="5" id="description_presta" name="description_presta" class="brl_input" placeholder="Description de la Prestation"  autocomplete="off"><?php echo $lstInfoPrestations['description_presta']; ?></textarea><br>
						<label>Quantité :</label> <input type="number" id="quantite" name="quantite" step="0.1" value="<?php echo $lstInfoPrestations['quantite']; ?>" class="brl_input" placeholder="Quantité"  autocomplete="off"><br>
						<label>Unité :</label> <input type="text" id="unite" name="unite" value="<?php echo $lstInfoPrestations['unite']; ?>" class="brl_input" placeholder="Unité de la prestation"  autocomplete="off"><br>
						<label>Prix Unitaire (en €) :</label> <input type="text" id="prix_unitaire" name="prix_unitaire" value="<?php echo $lstInfoPrestations['prix_unitaire']; ?>" class="brl_input" placeholder="Prix Unitaire"  autocomplete="off"><br>
					</div>
					<p class="clear">&nbsp;</p>						

					<script type="text/javascript">
						
					function submitform()
					{
						var formOk = true;
						var champNom = $("#label");
						champNom.css("background-color", "initial");
						
						// Test champ Nom
						if (champNom.val() == "") {
							champNom.css("background-color", "red");
							alert("Erreur de saisie");
							formOk = false;
						}
						
						// Test form
						if (formOk) {
							document.getElementById("formPrestation").submit();
							return true;
						} 
					}
					</script>
				</form>
			</div>
		</div>
	</div>
	<p class="clear">&nbsp;</p>
	<div>
		<div class="bouton2 clear">
			<a href="<?php echo _URL_BASE_; ?>/prestations">ANNULER</a>
			<a onclick="submitform()" class="btn-green-save">ENREGISTRER</a>
		</div>
		<input type="submit">
	</div>					
<?php } 

/* Page List of Devis */
function displayPageContenuLstDevis($option) {
	
	$lstInfoDevis   = brl::getLstDevis();
?>

	<div class="brl-panel-bloc100">
		<div class="titre-bloc-b">
			<div class="brl-title">Liste des Devis</div>
			<div class="bouton3 clear">
				<a href="<?php echo _URL_BASE_; ?>/devisvue/0"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter</a>
			</div>
		</div>
		<div class="brl-lst">
			<table>
				<tr class="brl-lst-el">
					<th> Label </th>
					<th> Commentaire </th>
					<th> Référence </th>
					<th> Date du devis </th>
					<th> Date de validité </th>
					<th> Modalité de payement </th>
					<th> Date CGV </th>
					<th> CGV </th>
				</tr>
				<?php foreach ($lstInfoDevis as $infoTmp) { ?>
				<tr class="brl-lst-el main">
					<td>
						<a href="<?php echo _URL_BASE_; ?>/devisvue/<?php echo $infoTmp['id']; ?>"><?php echo $infoTmp['label_devis'];?></a>
					</td>
					<td>
						<?php echo $infoTmp['commentaire_devis']; ?>
					</td>
					<td>
						<?php echo $infoTmp['ref_devis']; ?>
					</td>
					<td>
						<?php echo $infoTmp['date_devis']; ?>
					</td>
					<td>
						<?php echo $infoTmp['date_validite_devis']; ?>
					</td>
					<td>
						<?php echo $infoTmp['modalite_payement_devis'];?> 
					</td>
					<td>
						<?php echo $infoTmp['date_cgv_devis']; ?>
					</td>
					<td>
						<?php echo $infoTmp['cgv_devis'];?> 
					</td>
					<div class="clear"></div>
				</tr>
				<?php }  ?>
			</table>
		</div>
			<div> &nbsp; </div>
	</div>
<?php

}

/* Page Fiche des Devis */
function displayPageContenuDevis($idDevis) {
	
	$lstInfoDevis  = brl::getDevis($idDevis);
	$lstInfoPrestationDevis = brl::getConfiguration();

	$titre = 'Modifier le Devis';
	if (empty($idDevis)) { $titre = 'Ajouter le Devis'; }

	// Récupération de la liste des clients pour le select
	$brlBD = new brl_bd();
	$sql = "SELECT id, nom FROM `clients` ORDER BY nom;";
	$result = $brlBD->query_lst($sql);
	
?>
				<div class="brl-panel-bloc70 brl-panel-bloc-centre">
					<div>
						<div class="titre-bloc-b">
							<div class="brl-title"><?php echo $titre; ?></div>
							<div class="bouton5 clear" style="float: right;">
							<?php if (!empty($idDevis)) { ?><a href="<?php echo _URL_BASE_; ?>/devisrm/<?php echo $idDevis; ?>" onclick="if(window.confirm('Voulez-vous vraiment supprimer ?')){return true;}else{return false;}"><i class="fa fa-times" aria-hidden="true"></i> Supprimer</a><?php } ?>
							</div>
						</div>
						<div id="idPageContenuDevis" style="width: 100%;">
											
						<form method="post" action="<?php echo _URL_BASE_; ?>/devissav/<?php echo $idDevis; ?>" id="formDevis" name="formDevis" style="padding: 20px 0;" autocomplete="off">

							<div class="tab-content" style="margin-top: 20px;">
								<h4>Devis</h4>
								<div class="row">
									<div class="col-md-6">
										<label>Label :<sup class="red">*</sup></label> <input type="text" id="label_devis" name="label_devis" value="<?php echo $lstInfoDevis['label_devis']; ?>" class="brl_input" placeholder="Label du Devis"  autocomplete="off"><br>
										<label>Client :</label> <select name="selectClients" id="selectClients" class="brl_input">
											<?php foreach ($result as $infoTmp) { ?>
												<option value="<?php echo $infoTmp['id']; ?>" <?= ($infoTmp['id'] == $lstInfoDevis['id_clients']) ? "selected='selected'" : ''; ?>><?php echo $infoTmp['nom']; ?>
											<?php }  ?>
										</select> <a href="<?php echo _URL_BASE_; ?>/clientsvue/<?php echo $lstInfoDevis['id_clients']; ?>" target="_blank"><i class="fa fa-user"></i></a><br>
										<label>Référence :</label></label> <input type="text" id="ref_devis" name="ref_devis" value="<?php echo $lstInfoDevis['ref_devis']; ?>" class="brl_input" placeholder="Référence du Devis"  autocomplete="off"><br>
										<label>Date du devis :</label> <input type="date" id="date_devis" name="date_devis" value="<?php echo $lstInfoDevis['date_devis']; ?>" class="brl_input" placeholder="Date du Devis"  autocomplete="off"><br>
										<label>Date de validité du devis :</label> <input type="date" id="date_validite_devis" name="date_validite_devis" value="<?php echo $lstInfoDevis['date_validite_devis']; ?>" class="brl_input" placeholder="Date de validité du Devis"  autocomplete="off"><br>
									</div>
									<div class="col-md-6">
										<label>Modalité de payement du devis :</label><br><textarea rows="5" id="modalite_payement_devis" name="modalite_payement_devis" class="brl_input" placeholder="Modalité de payement"  autocomplete="off"><?php echo $lstInfoPrestationDevis['modalite_payement']['valeur']; ?></textarea><br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<label>Commentaire :</label><br><textarea rows="5" id="commentaire_devis" name="commentaire_devis" class="brl_input" placeholder="Commentaire sur le Devis"  autocomplete="off"><?php echo $lstInfoDevis['commentaire_devis']; ?></textarea><br>
									</div>
									<div class="col-md-6">
										<label>Commentaire interne :</label><br><textarea rows="5" id="commentaire_interne_devis" name="commentaire_interne_devis" class="brl_input" placeholder="Commentaire interne sur le Devis"  autocomplete="off"><?php echo $lstInfoDevis['commentaire_interne_devis']; ?></textarea><br>
									</div>
								</div>
							</div>
							<?php if(!empty($idDevis)) { ?>
							<p class="clear">&nbsp;</p>
							<div class="tab-content">
								<h4>Prestations</h4>
								<div class="col-md-12">
									<div id="formPrestationDevis">
									<?= brl_tools::returnTablePrestationsDevis($idDevis); ?>
									<?= brl_tools::returnFormPrestationsDevis(''); ?>
								</div>
							</div>
							<?php } ?>
						</div>						
						<p class="clear">&nbsp;</p>
					
							<script type="text/javascript">

							function submitform()
							{
								var formOk = true;
								var champNom = $("#label_devis");
								champNom.css("background-color", "initial");
								
								// Test champ Nom
								if (champNom.val() == "") {
									champNom.css("background-color", "red");
									alert("Erreur de saisie");
									formOk = false;
								}
								
								// Test form
								if (formOk) {
									document.formDevis.submit();
									return true;
								} else {
									alert("Informations manquantes sur le prospect");
									return false;
								}
								
							}

							$(document).on('click','.update_row_prestationsdevis',function(e) {
								var id = $(this).attr("data-id");
								$.ajax({
										url: "<?php echo _URL_BASE_; ?>/prestationsdevis-get/" + id,
										data: "",
										type: "post",
										success: function(code_html, statut){
											$("#formPrestationDevis .inputs_form").empty();
											$(code_html).appendTo("#formPrestationDevis .inputs_form");
										}
								});
							});

							// Ajout prestation
							$(document).on('click','#insertPrestationsDevis',function(e) {
								var data = $("#formDevis").serialize();
								e.preventDefault();
								$.ajax({
										data: data,
										type: "post",
										url: "<?php echo _URL_BASE_; ?>/prestationsdevis-add/<?= $idDevis; ?>",
										success: function(code_html, statut){
											$("#formPrestationDevis").empty();
											$(code_html).appendTo("#formPrestationDevis");
										}
								});
							});

							$(document).on('click','#updatePrestationsDevis',function(e) {
								var id = $(this).attr("data-id");
								var data = $("#formDevis").serialize();
								e.preventDefault();
								$.ajax({
										data: data,
										type: "post",
										url: "<?php echo _URL_BASE_; ?>/prestationsdevis-update/<?=  $idDevis . '&idPrestationDevis='; ?>" + id,
										success: function(code_html, statut){
											$("#formPrestationDevis").empty();
											$(code_html).appendTo("#formPrestationDevis");
										}
								});
							});

							

							$(document).on('click','.delete_row_prestationsdevis',function(e) {
								if(confirm('Etes-vous sûr de vouloir supprimer ce suivi?')) {
									var id = $(this).attr("data-id");
									var data = $("#formDevis").serialize();
									$.ajax({
											url: "<?php echo _URL_BASE_; ?>/prestationsdevis-rm/" + id + "&idDevis=<?= $idDevis; ?>",
											data: data,
											type: "post",
											success: function(code_html, statut){
												$("#formPrestationDevis").empty();
												$(code_html).appendTo("#formPrestationDevis");
											}
									});
								}
							});
							</script>
						</form>
						
						</div>
					</div>
				</div>
				<p class="clear">&nbsp;</p>
				
				<div>
					<div class="bouton2 clear" style="text-align: center;">
						<a href="<?php echo _URL_BASE_; ?>/devis">ANNULER</a>
						<a href="javascript:submitform();" class="btn-green-save">ENREGISTRER</a>
						<form method="post" action="<?php echo _URL_BASE_; ?>/devisgenerate/<?php echo $idDevis; ?>" id="formGenerateDevis" name="formGenerateDevis">  
							<input type="submit" name="create_pdf" class="boutonGenerate" value="Générer le devis" formtarget="_blank" />  
						</form> 
					</div>
					<input type="submit" style="display:none">
				</div>
				<p class="clear">&nbsp;</p>
<?php }

function displayPageGenerateDevis($idDevis) {

	$lstInfoPrestationDevis = brl::getConfiguration();
	$lstInfoDevis  			= brl::getDevis($idDevis);
	$lstPrestationsDevis 	= brl::getLstPrestationsDevis($idDevis);
	$DevisClient 			= brl::getDevisClient($idDevis);
	$THTPrestationsDevis	= brl::getTHTPrestationsDevis($idDevis);
	// echo "<pre>"; print_r($THTPrestationsDevis);echo "</pre>"; exit ;

	$prixTVA = ($THTPrestationsDevis['somme']/100)*$lstInfoPrestationDevis["tva"]["valeur"];
	$prixTTC = (($THTPrestationsDevis['somme']/100)*$lstInfoPrestationDevis["tva"]["valeur"]) + $THTPrestationsDevis['somme'];

	$date = $lstInfoDevis['date_devis'];
	$date_devis = date('d/m/Y', strtotime($date));
	$date_val = $lstInfoDevis['date_validite_devis'];
	$date_validite_devis = date('d/m/Y', strtotime($date_val));

	if (ISSET($_POST['create_pdf'])) {
		// Include the main TCPDF library (search for installation path).
		require_once('tcpdf/tcpdf.php');
		// Extend the TCPDF class to create custom Header and Footer
		class TCPDFDEVIS extends TCPDF {
	
			//Page header
			public function Header() {
				// Logo
				// $image_file = K_PATH_IMAGES.'logo-autour-du-digital.png';
				$img = $this->Image('images/logo-autour-du-digital.png', 15, 7, '', 20, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
				// Set font
				$this->SetFont('helvetica', 'B', 15);
				// Title
				$this->Text(110, 10, 'VOTRE AGENCE DIGITAL', false, false, true, 0, 2, '', false, '', 0, false, 'T', 'M', false);
				// Set font
				$this->SetFont('helvetica', '', 10);
				// Subtitle
				$this->Text(120, 17, 'SITE, BOUTIQUE & MOBILE', false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false);
			}
	
			// Page footer
			public function Footer() {
				$lstInfoPrestationDevis = brl::getConfiguration();
				// Position at 15 mm from bottom
				$this->SetY(-25);
				// Set font
				$this->SetFont('helvetica', 'B', 8);
				 // set color for text
				$this->SetTextColor(123, 130, 132);
				// Paraph
				$this->Cell(0, 10, 'Paraphe: ', 0, false, 'L', 0, '', 0, false, 'T', 'M');
				// Set font
				$this->SetFont('helvetica', 'B', 8);
				// Page number
				$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
				$this->ln();
				// Set font
				$this->SetFont('helvetica', '', 8);
				$adress = $lstInfoPrestationDevis["footer_facture"]["valeur"];
				// Adress
				$this->MultiCell(0, 5, $adress, 0, 'C', false, 1, '', '', true);
			}
		}
	
		// create new PDF document
		$pdf = new TCPDFDEVIS(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Emmanuel Berlese');
		$pdf->SetTitle('Devis');
		$pdf->SetSubject('Devis in PDF');
		$pdf->SetKeywords('Devis, PDF');
	
		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
	
		// ---------------------------------------------------------
	
		$pdf->SetMargins(17, 40, 17, true);
		// set font
		$pdf->SetFont('helvetica', '', 9);
	
		// add a page
		$pdf->AddPage();
	
		// set some text to print
		$txt .= '<!-- CSS STYLE -->
				<style>
					.tableOne {
						padding: 10px;
						border-spacing: 0px 20px;
					}
					.tdFirst {
						border: 1px, solid, #1b8ee4;
						width: 250px;
					}
					.h1Devis {
						color: #1b8ee4;
					}
					.tdSecond {
						width: 120px;
					}
					.trTableTwo {
						background-color: #1b8ee4; 
						font-size: 9rem;
						color: white;
						font-weight: bold;
					}
					th, td {
						padding: 15px;
					}
					.th1 {
						width: 360px;
						text-align: center;
					}
					.th2 {
						width: 80px;
					}
					.th3 {
						width: 80px;
					}
					.th4 {
						width: 80px;
					}
					.ssTitreDevis {
						font-size: 9rem;
					}
				</style>
				<!-- HTML -->
				<table class="tableOne">
					<tr>
						<td class="tdFirst">
							<h1 class="h1Devis">DEVIS</h1>
							<p>Réf : <b>'.$lstInfoDevis['ref_devis'].'</b></p> 
							<p>Le : <b>'.$date_devis.'</b><br>
							Validité : '.$date_validite_devis.'</p>
						</td>
						<td class="tdSecond"></td>
						<td>
							<p>'.$DevisClient['nom'].'</p>
							<p>'.$DevisClient['adresse_facturation'].'</p>
							<p>...</p>
						</td>
						<td></td>
					</tr>
				</table>
				<table class="tableTwo">
					<tr class="trTableTwo">
						<th class="th1">
							Désignation
						</th>
						<th class="th2">
							Quantité
						</th>
						<th class="th3">
							P.U.
						</th>
						<th class="th4">
							Prix H.T.
						</th>
					</tr>';
				foreach ($lstPrestationsDevis as $infoTmp) {
					$txt .= '<tr>
						<td>';
						if ($infoTmp['type'] == 1) {
							$txt .= '<h2>'.$infoTmp['titre'].'</h2>';
						} else if ($infoTmp['type'] == 2) {
							$txt .= '<h2>'.$infoTmp['titre'].'</h2>';
						} else if ($infoTmp['type'] == 3) {
							$txt .= '<h3>'.$infoTmp['titre'].'</h3>';
						} else if ($infoTmp['type'] == 10) {
							$txt .= '<p>'.$infoTmp['titre'].'</p>';
						} else if ($infoTmp['type'] == 11) {
							$txt .= '<p style="text-indent: 30%;">'.$infoTmp['titre'].'</p>';
						} else if ($infoTmp['type'] == 12) {
							$txt .= '<p>'.$infoTmp['titre'].'</p>';
						} else if ($infoTmp['type'] == 20) {
							$txt .= '<p style="color: red;">'.$infoTmp['titre'].'</p>';
						} else {
							$txt .= '<p>'.$infoTmp['titre'].'</p>';
						}
					$txt .= '</td>
						<td class="td2">';
						if ($infoTmp['quantite'] == 0) {
							$txt .= '';
						} else {
							$txt .= '<p>'.$infoTmp['quantite'].' '.$infoTmp['unite'].'</p>';
						}
						$txt .= '</td>
						<td class="td3">';
						if ($infoTmp['prix_unitaire'] == 0) {
							$txt .= '';
						} else {
							$txt .= '<p>'.$infoTmp['prix_unitaire'].' €</p>';
						}
						$txt .= '</td>
						<td class="td4">';
						if (($infoTmp['prix_unitaire']*$infoTmp['quantite']) == 0) {
							$txt .= '';
						} else {
							$txt .= '<p>'.$prixHT = $infoTmp['prix_unitaire']*$infoTmp['quantite'].' €</p>';
						}
						$txt .= '</td>
					</tr>';
				}
					$txt .= '<tr style="line-height:30px;">
						<td>
							<h3>Titre commentaire</h3>
						</td>
					</tr>
					<tr>
						<td>
						<p class="ssTitreDevis">'.$lstInfoDevis['commentaire_devis'].'</p>
						</td>
					</tr>
				</table>
				<div id="conteneur">
					<div class="blockRight">
						<table style="width: 600px;">
							<tr class="trTableTwo">
								<th style="width: 300px; background-color: white;">	
								</th>
								<th style="width: 100px;">
									TOTAL HT
								</th>
								<th style="width: 100px;">
									TVA
								</th>
								<th style="width: 100px;">
									TOTAL TTC
								</th>
							</tr>
							<tr style="line-height:30px;"><td style="width: 300px; background-color: white;"></td>
								<td style="width: 100px;">
									<p>'.$THTPrestationsDevis['somme'].' €</p>
								</td>
								<td style="width: 100px;">
									<p>'.$prixTVA.' €</p>
								</td>
								<td style="width: 100px;">
									<p>'.$prixTTC.' €</p>
								</td>
							</tr>
							</table>
						<table style="width: 300px;">
							<tr>
								<td style="width: 300px;">
									<p>Le devis est valable sous réserve …</p>
								</td>
								<td style="width: 200px;" class="trTableTwo">
									NET A PAYER
								</td>
								<td>
									<p><b>'.$prixTTC.' €</b></p>
								</td>
							</tr>
							<tr>
								<td style="width: 300px;"></td>
								<td style="width: 200px;"></td>
							</tr>
							<tr>
								<td style="width: 300px;">
									<p>Signature précédée de la mention<br>
									« Bon pour commande »</p>
								</td>
								<td style="width: 200px;">
									<p><b>Exigibilité du règlement :</b><br>
									'.$lstInfoDevis['modalite_payement_devis'].'</p>
								</td>
							</tr>
						</table>
					</div>
				</div>';
	
		// print a block of text using Write()
		$pdf->WriteHTML($txt);
		ob_clean();
	
		$pdf->SetMargins(17, 30, 17, true);
		// set font
		$pdf->SetFont('helvetica', '', 7);
		// add a page
		$pdf->AddPage();

		// set some text to print
		$txtP2 .= '<style>
		table {
			border-spacing: 0px 20px;
			text-align:justify;
		}
		</style>
		'.$lstInfoPrestationDevis["cgv"]["valeur"].'';

		// print a block of text using Write()
		$pdf->WriteHTML($txtP2);
		ob_clean();
		// ---------------------------------------------------------
	
		//Close and output PDF document
		$pdf->Output('devis.pdf', 'I');
	
		//============================================================+
		// END OF FILE
		//============================================================+
	
	
	}

}

function displayPageFooter () {
	
?>
				</div>
			</div>
		</div>
	</body>
</html>

<?php 
}?>