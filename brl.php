<?php

/**
 * Class brl | fichier brl.php
 *
 * Description of the class: 
 * - make all the requests we need and put them in differents static (for the majority) functions.
 *
 * @package CRM Autour du Digital Project
 * @author Emmanuel Berlese - Jessy and Natacha
 * @copyright BRL entreprise
 * @version Test v1.0
 */

class brl {
	
	/**
     * public $brlBD is used to store all datas needed 
     * @var string
     */
	public $brlBD = '';
	
	/**
	 * init new object of the brl_bd class
	 */
	function __construct() {
		
		$this->brlBD = new brl_bd();
		
	}
	

	/**
	 * @param mixed $idClient=0
	 * for the module Suivi Temps
	 * @return void
	 */
	static function getLstingClients($idClient=0) { 

		$brlBD = new brl_bd();

		//  if the user is a client
		if($idClient!=0) {
			$sql = "SELECT BRLR.*
					FROM `clients` BRLR
					WHERE BRLR.id = '$idClient'
					ORDER BY BRLR.nom";
		// else don't include the id
		} else {
			$sql = "SELECT BRLR.*
					FROM `clients` BRLR
					ORDER BY BRLR.nom";
		}
		return $brlBD->query_lst($sql);
	}

	
	
/*Connexion */
/**
 * @param mixed $login
 * @param mixed $password
 * 
 * @return void
 */
	function connexion($login, $password) {
		$date = date("Y-m-d H:i:s");

		$sql = "SELECT * FROM `users` WHERE login = '$login' ";

		$row = $this->brlBD->query_elm($sql);

		if (md5($password) == $row['mdp']){
			session_unset();

			$_SESSION["USER"]["ID_USER"] 		= $row['id'];
			$_SESSION["USER"]["LOGIN"]			= $row['login'];
			$_SESSION["USER"]["IS_ADMIN"]		= $row['is_admin'];
			$_SESSION["USER"]["IS_SUPER_ADMIN"]	= $row['is_super_admin'];
			$_SESSION["USER"]["IS_CLIENT"]		= $row['is_client'];
			$_SESSION["USER"]["IS_EMPLOYE"]		= $row['is_employe'];
			$_SESSION["USER"]["ID_CLIENTS"]		= $row['id_clients'];



			// Log connexion OK
			brl_tools::logInsert('users', $row['id'], 'Connexion');

			return 1;
		} else {
			// Log connexion KO
			brl_tools::logInsert('users', $row['id'], 'Erreur Connexion');

			session_unset();
			return 0;
		}
	}

	/**
	 * STATISTIQUES
	 */
/**
 * @return void
 */
	static function statsClients() {
		$brlBD = new brl_bd();
	
		$sql="SELECT COUNT(BRLCI.id) as 'nbrElm'
				FROM `clients` BRLCI
				WHERE BRLCI.type = '0'
				AND BRLCI.is_actif = '1';";
	
		return $brlBD->query_elm($sql);
	}

/**
 * @return void
 */
	static function statsProjets() {
		$brlBD = new brl_bd();
	
		$sql="SELECT COUNT(BRLCI.id) as 'nbrElm'
				FROM `projets` BRLCI
				WHERE BRLCI.is_actif = '1';";
	
		return $brlBD->query_elm($sql);
	}

/**
 * @return void
 */
	static function statsSuiviTemps() {
		$brlBD = new brl_bd();
	
		$sql="SELECT SUM(BRLCI.temps) as 'tempsTotal'
				FROM `suivi_temps` BRLCI
				WHERE BRLCI.validite = '1';";
	
		return $brlBD->query_elm($sql);
	}


	/* FOR THE CLIENT ACCESS */

	/**
	 * Method static statsTickets()
	 *
	 * count the number of tickets of the client in the current year
	 */
	static function statsTickets($idClient) {
		$brlBD = new brl_bd();
	
		$sql="SELECT COUNT(BRLCI.id) as nbreTickets, MONTH(CURRENT_DATE) as nbreMois
				FROM `suivi_temps` BRLCI
				INNER JOIN `projets` BRLP ON BRLP.id = BRLCI.id_projet
				INNER JOIN `clients` BRLC ON BRLC.id = BRLP.id_client
				WHERE BRLC.id = '$idClient'
                AND YEAR(BRLCI.date_debut) >= YEAR(CURRENT_DATE);";
	
		return $brlBD->query_elm($sql);
	}
	/**
	 * Method static statsTicketsNF()
	 *
	 * count the number of tickets not charged yet
	 */
	static function statsTicketsNF($idClient) {
		$brlBD = new brl_bd();
	
		$sql="SELECT COUNT(BRLCI.validite) as nbreTicketsNF, SUM(BRLCI.temps) as tempsTicketsNF
				FROM `suivi_temps` BRLCI
				INNER JOIN `projets` BRLP ON BRLP.id = BRLCI.id_projet
				INNER JOIN `clients` BRLC ON BRLC.id = BRLP.id_client
				WHERE BRLC.id = '$idClient'
				AND BRLCI.validite = 1;";
	
		return $brlBD->query_elm($sql);
	}
	/**
	 * Method static nbreMinutesProjetsClient()
	 *
	 * time spent on projects in the current year
	 */
	static function nbreMinutesProjetsClient($idClient) {
		$brlBD = new brl_bd();
	
		$sql="SELECT SUM(BRLCI.temps) as 'tempsTotal', MONTH(CURRENT_DATE) as nbreMois
				FROM `suivi_temps` BRLCI
				INNER JOIN `projets` BRLP ON BRLP.id = BRLCI.id_projet
				INNER JOIN `clients` BRLC ON BRLC.id = BRLP.id_client
				WHERE BRLC.id = '$idClient'
                AND YEAR(BRLCI.date_debut) >= YEAR(CURRENT_DATE);";
	
		return $brlBD->query_elm($sql);
	}
	/**
	 * Method static statutProjetsClient()
	 *
	 * time spent on projects in the current year
	 */
	static function statutProjetsClient($idClient) {
		$brlBD = new brl_bd();
	
		$sql="SELECT BRLCI.validite, DATE_Format(BRLCI.date_debut, '%d/%m/%Y') as date_debut
				FROM `suivi_temps` BRLCI
				INNER JOIN `projets` BRLP ON BRLP.id = BRLCI.id_projet
				INNER JOIN `clients` BRLC ON BRLC.id = BRLP.id_client
				WHERE BRLC.id = '$idClient'
                ORDER BY date(date_debut) DESC;";
	
		return $brlBD->query_lst($sql);
	}
	/**
	 * Method static nbreTempsRestant()
	 *
	 * time spent on projects in the current year and get the name of the project
	 */
	static function nbreTempsRestant($idClient) {
		$brlBD = new brl_bd();
	
		$sql="SELECT BRLP.nom, BRLP.nbre_heure_estime, SUM(BRLCI.temps) as tempsTotal
				FROM `suivi_temps` BRLCI
				INNER JOIN `projets` BRLP ON BRLP.id = BRLCI.id_projet
				INNER JOIN `clients` BRLC ON BRLC.id = BRLP.id_client
				WHERE  BRLC.id = '$idClient'
				GROUP BY BRLP.nbre_heure_estime, BRLP.nom;";
	
		return $brlBD->query_lst($sql);
	}
	/**
	 * Method static statsSuiviTempsMonth()
	 *
	 * recover last 12 month for suivi temps
	 */
	static function statsSuiviTempsMonth($idClient) {
		$brlBD = new brl_bd();
		
		$sql= "SELECT SUM(BRLCI.temps) as temps, MONTH(BRLCI.date_debut) as mois, YEAR(BRLCI.date_debut) as annees
				FROM `suivi_temps` BRLCI
				INNER JOIN `projets` BRLP ON BRLP.id = BRLCI.id_projet
				INNER JOIN `clients` BRLC ON BRLC.id = BRLP.id_client
				WHERE BRLCI.date_debut >= SUBDATE(CURRENT_DATE, INTERVAL 12 month)
				AND  BRLC.id = '$idClient'
				GROUP BY MONTH(BRLCI.date_debut), YEAR(BRLCI.date_debut)
				ORDER BY annees DESC, MONTH(BRLCI.date_debut) DESC;";

		return $brlBD->query_lst($sql);
	}
	/**
	 * Method static statsMonth()
	 *
	 * recover last 12 month in letters for graphique
	 */
	static function statsMonth($idClient) {
		$brlBD = new brl_bd();

		$sql= "SELECT distinct( MONTH(BRLCI.date_debut)),
				(CASE  MONTH(BRLCI.date_debut)
						WHEN 1 THEN 'janvier'
						WHEN 2 THEN 'février'
						WHEN 3 THEN 'mars'
						WHEN 4 THEN 'avril'
						WHEN 5 THEN 'mai'
						WHEN 6 THEN 'juin'
						WHEN 7 THEN 'juillet'
						WHEN 8 THEN 'août'
						WHEN 9 THEN 'septembre'
						WHEN 10 THEN 'octobre'
						WHEN 11 THEN 'novembre'
						ELSE 'décembre'
				END) as mois, YEAR(BRLCI.date_debut) as annees
				FROM `suivi_temps` BRLCI
				INNER JOIN `projets` BRLP ON BRLP.id = BRLCI.id_projet
				INNER JOIN `clients` BRLC ON BRLC.id = BRLP.id_client
				WHERE BRLCI.date_debut >= SUBDATE(CURRENT_DATE, INTERVAL 12 month)
				AND  BRLC.id = '$idClient'
                ORDER BY annees DESC, MONTH(BRLCI.date_debut) DESC;";

		return $brlBD->query_lst($sql);
	}
	/**
	 * Method static priceSuiviTemps()
	 *
	 * get price of suivi temps
	 */
	static function priceSuiviTemps($idClient) {
		$brlBD = new brl_bd();
		
		$sql= "SELECT BRLP.prix_horaire
				FROM `suivi_temps` BRLCI
				INNER JOIN `projets` BRLP ON BRLP.id = BRLCI.id_projet
				INNER JOIN `clients` BRLC ON BRLC.id = BRLP.id_client
				WHERE BRLC.id = '$idClient'
                AND YEAR(BRLCI.date_debut) >= YEAR(CURRENT_DATE);";
	
		return $brlBD->query_elm($sql);
	}
	/**
	 * Method static getChargeServer()
	 *
	 * get the right charge server
	 */
	static function getChargeServer() {
		$file = file_get_contents('https://www.autour-du-digital.fr/brlinfoserveur_089976.php');
		$lstInfo = explode("§", $file);

		$chargeUsed = ($lstInfo[1]/$lstInfo[0])*100;

		return $chargeUsed;
	}

	/* SUIVI PROJETS */
/**
 * @param mixed $suivi
 * 
 * @return void
 */
	static function getLstSuiviProjets($suivi) {
		$brlBD = new brl_bd();

		$sql = "SELECT BRLP.id, BRLP.nom, BRLP.id_client, BRLP.nbre_heure_estime, BRLP.prct_avancement
				FROM `projets` BRLP
				WHERE BRLP.is_suivi = '$suivi'
				ORDER BY BRLP.nom;";
		return $brlBD->query_lst($sql);
	}

/**
 * @param mixed $idProjet
 * 
 * @return void
 */
	static function nbreMinutesProjet($idProjet) {
		$brlBD = new brl_bd();
	
		$sql="SELECT SUM(BRLCI.temps) as 'tempsTotal'
				FROM `suivi_temps` BRLCI
				WHERE BRLCI.id_projet = '$idProjet';";
	
		return $brlBD->query_elm($sql);
	}

	
	/**
	 * Static method getConfiguration()
	 * get datas of configuration table
	 * return an array with the column 'cle' in indice
	 */
	static function getConfiguration() {

		$brlBD = new brl_bd();

		$sql = "SELECT *
				FROM `configuration`";
		return $brlBD->query_lst($sql, 'cle');
	}

	/**
	 * Static method savConfiguration()
	 * function who call the static function setConfiguration
	 * this function give right parameters for the update
	 */
	static function savConfiguration() {
		self::setConfiguration('annee_facturation', $_POST['annee_facturation']);
		self::setConfiguration('tva', $_POST['tva']);
		self::setConfiguration('rib', $_POST['rib']);
		self::setConfiguration('references_legales', $_POST['references_legales']);
		self::setConfiguration('modalite_payement', $_POST['modalite_payement']);
		self::setConfiguration('footer_facture', $_POST['footer_facture']);
		self::setConfiguration('date_cgv', $_POST['date_cgv']);
		self::setConfiguration('cgv', $_POST['cgv']);
	}

	/**
	 * Static method setConfiguration()
	 * function with 2 parameters, one for the 'cle', and an other for the 'valeur'
	 * An update request to give the correct values
	 */
	static function setConfiguration($cle, $valeur) {
		$brlBD = new brl_bd();
		$sql = "UPDATE `configuration` 
				SET `valeur` = '".brl_tools::pSqlTextarea($valeur)."' 
				WHERE cle = '$cle'";
		$brlBD->query($sql);
	}

	/**
	 * Prestations
	 */
/**
 * @return void
 */
	static function getCompteurPrestations() {
		$brlBD = new brl_bd();

		$sql="SELECT COUNT(id) as 'nbrElm'
				FROM `prestations`";

		return $brlBD->query_elm($sql);
	}

/**
 * @param mixed $numPage
 * 
 * @return void
 */
	static function getLstPrestations($numPage) {

		$brlBD = new brl_bd();

		$nbrElmPage = BRLConfig::$nbr_elm_page;
		$offset 	= ($numPage - 1) * $nbrElmPage;

		$sql = "SELECT *
				FROM `prestations` 
				ORDER BY label
				LIMIT $nbrElmPage OFFSET $offset";
		return $brlBD->query_lst($sql);
	}

	/**
	 * get list of datas where the id is the right id
	 */
/**
 * @param mixed $idPrestation
 * 
 * @return void
 */
	static function getPrestations($idPrestation) {

		$brlBD = new brl_bd();

		$sql = "SELECT `id`, `label`, `titre`, `description_presta`, `quantite`, `unite`, `prix_unitaire`
				FROM `prestations`
				WHERE id = '$idPrestation'";
		return $brlBD->query_elm($sql);
	}

	/**
	 * adding a prestation
	 */
/**
 * @param mixed $idPrestation
 * 
 * @return void
 */
	static function addPrestations($idPrestation) {
		$label				= (isset($_POST['label']))? $_POST['label'] : '';
		$titre				= (isset($_POST['titre']))? $_POST['titre'] : '';
		$descriptionPresta 	= (isset($_POST['description_presta']))? $_POST['description_presta'] : '';
		$quantite 			= (isset($_POST['quantite']))? $_POST['quantite'] : '';
		$unite				= (isset($_POST['unite']))? $_POST['unite'] : '';
		$prixUnitaire		= (isset($_POST['prix_unitaire']))? $_POST['prix_unitaire'] : '';

		$brlBD = new brl_bd();
		$date = date("Y-m-d H:i:s");

		// if there is no idPrestation yet, do an insert
		if (empty($idPrestation)) {
			$sql = "INSERT IGNORE INTO `prestations`(`label`, `titre`, `description_presta`, `quantite`, `unite`, `prix_unitaire`) VALUES ('".brl_tools::pSql($label)."', '".brl_tools::pSql($titre)."', '".brl_tools::pSqlTextarea($descriptionPresta)."', '$quantite', '$unite', '$prixUnitaire');";
			$brlBD->query($sql);
			$idFacture = $brlBD->getInsertId();
		// else do an update
		} else {
			$sql = "UPDATE `prestations` SET label = '".brl_tools::pSql($label)."', titre = '".brl_tools::pSql($titre)."', description_presta = '".brl_tools::pSqlTextarea($descriptionPresta)."', quantite = '$quantite', unite = '$unite', prix_unitaire = '$prixUnitaire' WHERE id = '$idPrestation';";
			$brlBD->query($sql);
		}

		$_SESSION['msg_confirmation'] = "Enregistrement réussi";
		return $brlBD->getInsertId();
	}

	/**
	 * delete a prestation
	 */
/**
 * @param mixed $idPrestation
 * @param boolean $all
 * 
 * @return void
 */
	static function rmPrestations($idPrestation, $all = false) {
		$brlBD = new brl_bd();

		if ($all) {
			$sql = "DELETE FROM `prestations` WHERE `id` = '$idPrestation'";
			$brlBD->query($sql);
		}
	}

	/**
	 * Devis
	 */
/**
 * @return void
 */
	static function getCompteurDevis() {
		$brlBD = new brl_bd();

		$sql="SELECT COUNT(id) as 'nbrElm'
				FROM `devis`";

		return $brlBD->query_elm($sql);
	}
	
	/**
	 * Static method getLstDevis()
	 * list all devis
	 */
	static function getLstDevis($numPage) {

		$brlBD = new brl_bd();

		$nbrElmPage = BRLConfig::$nbr_elm_page;
		$offset 	= ($numPage - 1) * $nbrElmPage;

		$sql = "SELECT `id`, `id_clients`, `label_devis`, `commentaire_devis`, `ref_devis`, DATE_Format(`date_devis`, '%d-%m-%Y') as `date_devis`, DATE_Format(`date_validite_devis`, '%d-%m-%Y') as `date_validite_devis`, `modalite_payement_devis`, DATE_Format(`date_cgv_devis`, '%d-%m-%Y') as `date_cgv_devis`, `cgv_devis`
				FROM `devis` 
				ORDER BY label_devis
				LIMIT $nbrElmPage OFFSET $offset";
		return $brlBD->query_lst($sql);
	}

/**
 * @param mixed $idDevis
 * 
 * @return void
 */
	static function getDevis($idDevis) {

		$brlBD = new brl_bd();

		$sql = "SELECT `id`, `id_clients`, `label_devis`, `commentaire_devis`, `ref_devis`, `date_devis`, `date_validite_devis`, `modalite_payement_devis`, DATE_Format(`date_cgv_devis`, '%d-%m-%Y') as `date_cgv_devis`, `cgv_devis` 
				FROM `devis` 
				WHERE id = '$idDevis'";
		return $brlBD->query_elm($sql);
	}

/**
 * @param mixed $idDevis
 * 
 * @return void
 */
	static function addDevis($idDevis) {
		
		$selectClient 		= (isset($_POST['selectClients']))? $_POST['selectClients'] : '';
		$label				= (isset($_POST['label_devis']))? $_POST['label_devis'] : '';
		$commentaire		= (isset($_POST['commentaire_devis']))? $_POST['commentaire_devis'] : '';
		$ref 				= (isset($_POST['ref_devis']))? $_POST['ref_devis'] : '';
		$dateDevis 			= (isset($_POST['date_devis']))? $_POST['date_devis'] : '';
		$dateValidite		= (isset($_POST['date_validite_devis']))? $_POST['date_validite_devis'] : '';
		$modalitePayement	= (isset($_POST['modalite_payement_devis']))? $_POST['modalite_payement_devis'] : '';
		$dateCgv			= (isset($_POST['date_cgv_devis']))? $_POST['date_cgv_devis'] : '';
		$cgv 				= (isset($_POST['cgv_devis']))? $_POST['cgv_devis'] : '';

		$brlBD = new brl_bd();
		$date = date("Y-m-d H:i:s");

		if (empty($idDevis)) {
			$sql = "INSERT IGNORE INTO `devis`(`id_clients`, `label_devis`, `commentaire_devis`, `ref_devis`, `date_devis`, `date_validite_devis`, `modalite_payement_devis`, `date_cgv_devis`, `cgv_devis`) VALUES ('$selectClient', '".brl_tools::pSql($label)."', '".brl_tools::pSql($commentaire)."', '$ref', '$dateDevis', '$dateValidite', '".brl_tools::pSql($modalitePayement)."', '2012-12-27', 'cgv');";
			$brlBD->query($sql);
			$idFacture = $brlBD->getInsertId();
		} else {
			$sql = "UPDATE `devis` SET id_clients= '$selectClient', label_devis = '".brl_tools::pSql($label)."', commentaire_devis = '".brl_tools::pSql($commentaire)."', ref_devis = '$ref', date_devis = '$dateDevis', date_validite_devis = '$dateValidite', modalite_payement_devis = '".brl_tools::pSqlTextarea($modalitePayement)."', date_cgv_devis = '2012-12-27', cgv_devis = 'cgv' WHERE id = '$idDevis';";
			$brlBD->query($sql);
		}

		$_SESSION['msg_confirmation'] = "Enregistrement réussi";

		return $brlBD->getInsertId();
	}
	
/**
 * @param mixed $idDevis
 * @param boolean $all
 * 
 * @return void
 */
	static function rmDevis($idDevis, $all = false) {
		$brlBD = new brl_bd();

		if ($all) {
			$sql = "DELETE FROM `devis` WHERE `id` = '$idDevis'";
			$brlBD->query($sql);
		}
	}

	/**
	 * Devis__Prestations
	 */
/**
 * @param mixed $idDevis
 * 
 * @return void
 */
	static function getLstPrestationsDevis($idDevis) {

		$brlBD = new brl_bd();

		$sql = "SELECT *
				FROM `devis_prestations`
				WHERE id_devis = '$idDevis'";
		return $brlBD->query_lst($sql);
	}

/**
 * @param mixed $idPrestationDevis
 * 
 * @return void
 */
	static function getPrestationsDevis($idPrestationDevis) {

		$brlBD = new brl_bd();

		$sql = "SELECT *
				FROM `devis_prestations` 
				WHERE id = '$idPrestationDevis'";
		return $brlBD->query_elm($sql);
	}

/**
 * @param mixed $idDevis
 * 
 * @return void
 */
	static function addPrestationsDevis($idDevis) {

		$titre			= (isset($_POST['titre']))? $_POST['titre'] : '';
		$quantite		= (isset($_POST['quantite']))? $_POST['quantite'] : '';
		$unite			= (isset($_POST['unite']))? $_POST['unite'] : '';
		$prixUnitaire	= (isset($_POST['prix_unitaire']))? $_POST['prix_unitaire'] : '';

		$brlBD = new brl_bd();

		if (!empty($idDevis) && is_numeric($idDevis)) {
			$sql = "INSERT IGNORE INTO `devis_prestations` (`id_devis`, `titre`, `quantite`, `unite`, `prix_unitaire`) VALUES ('$idDevis', '".brl_tools::pSql($titre)."', '$quantite', '".brl_tools::pSql($unite)."', '$prixUnitaire');";
			// echo "<pre>"; print_r($_POST);echo "</pre>";exit;
			// echo $sql;
			$brlBD->query($sql);
			$idPresta = $brlBD->getInsertId();
		}

		$_SESSION['msg_confirmation'] = "Enregistrement réussi";

		return $brlBD->getInsertId();
	}

/**
 * @param mixed $idPrestationDevis
 * 
 * @return void
 */
	static function updatePrestationsDevis($idPrestationDevis) {

		$titre			= (isset($_POST['titre']))? $_POST['titre'] : '';
		$quantite		= (isset($_POST['quantite']))? $_POST['quantite'] : '';
		$unite			= (isset($_POST['unite']))? $_POST['unite'] : '';
		$prixUnitaire	= (isset($_POST['prix_unitaire']))? $_POST['prix_unitaire'] : '';

		$brlBD = new brl_bd();

		if (!empty($idPrestationDevis) && is_numeric($idPrestationDevis)) {
			$sql = "UPDATE `devis_prestations` SET `titre` = '".brl_tools::pSql($titre)."', `quantite` = '$quantite', `unite` = '".brl_tools::pSql($unite)."', `prix_unitaire` = '$prixUnitaire' WHERE id = '$idPrestationDevis';";
			// echo "<pre>"; print_r($_POST);echo "</pre>";
			// echo $sql;exit;
			$brlBD->query($sql);
		}

		$_SESSION['msg_confirmation'] = "Enregistrement réussi";

		return true;
	}

/**
 * @param mixed $idPrestationsDevis
 * 
 * @return void
 */
	static function rmPrestationsDevis($idPrestationsDevis) {
		$brlBD = new brl_bd();
		$sql = "DELETE FROM `devis_prestations` WHERE `id` = '$idPrestationsDevis'";
		$brlBD->query($sql);
	}
	
}