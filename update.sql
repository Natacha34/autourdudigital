-- ajouter une colonne date dans la table prospects
ALTER TABLE `prospects` ADD `date` DATE; 

-- select tous les champs de prospects en affichant la date au bon format
SELECT `id`, `nom`, `statut`, `contact`, `ref_devis`, `date_relance`, `commentaire_relance`, `commentaire`, DATE_FORMAT(`date`, '%d-%m-%Y') as date 
FROM `prospects` BRLP;

-- ajouter une colonne adresse_facturation dans la table clients
ALTER TABLE `clients` ADD `adresse_facturation` VARCHAR(300);

-- création d'une nouvelle table: configuration
CREATE TABLE configuration(
        cle                Varchar (100) NOT NULL ,
        valeur             Varchar (100) NOT NULL 
	,CONSTRAINT configuration_PK PRIMARY KEY (cle)
)

-- insertion manuelle des cles dans la table configuration
INSERT INTO `configuration`(`cle`, `valeur`) VALUES ('annee_facturation', '2020')
INSERT INTO `configuration`(`cle`, `valeur`) VALUES ('tva', '20')
INSERT INTO `configuration`(`cle`, `valeur`) VALUES ('rib', 'rib')
INSERT INTO `configuration`(`cle`, `valeur`) VALUES ('references_legales', 'ref')
INSERT INTO `configuration`(`cle`, `valeur`) VALUES ('modalite_payement', '30% à la commande<br> 70% à la réception')
INSERT INTO `configuration`(`cle`, `valeur`) VALUES ('footer_facture', "EURL « Berlese technologies » – 429 Rue de l'Industrie, 34000 Montpellier – www.autour-du-digital.fr<br>
Siret : 75322902000015 – RCS Montpellier – Tél. : 0411932498 – contact@autour-du-digital.fr")
INSERT INTO `configuration`(`cle`, `valeur`) VALUES ('date_cgv', '27-07-2012')
INSERT INTO `configuration`(`cle`, `valeur`) VALUES ('cgv', 'cgv')

-- création d'une nouvelle table: prestations
CREATE TABLE prestations(
        id                 Int  Auto_increment  NOT NULL ,
        label              Varchar (250) NOT NULL ,
        titre              Varchar (100) NULL ,
        description_presta Text NULL ,
        quantite           Decimal (10,1) NULL ,
        unite              Varchar (10) NULL ,
        prix_unitaire      Float (5,2) NULL
	,CONSTRAINT facturation_PK PRIMARY KEY (id)
)

-- création d'une nouvelle table: devis avec clef étrangère: clients.
CREATE TABLE devis(
        id                      Int  Auto_increment  NOT NULL ,
        id_clients              Int NOT NULL,
        label_devis             Varchar (100) NOT NULL ,
        commentaire_devis       Text NOT NULL ,
        ref_devis               Int NOT NULL ,
        date_devis              Date NOT NULL ,
        date_validite_devis     Date NOT NULL ,
        modalite_payement_devis Text NOT NULL ,
        date_cgv_devis          Date NOT NULL ,
        cgv_devis               Text NOT NULL 
	,CONSTRAINT devis_PK PRIMARY KEY (id)

	,CONSTRAINT devis_clients_FK FOREIGN KEY (id_clients) REFERENCES clients(id)
)ENGINE=InnoDB;

-- création d'une nouvelle table: devis_prestations => relation avec table devis.
CREATE TABLE devis_prestations(
        id            Int  Auto_increment  NOT NULL ,
        id_devis      Int NOT NULL,
        type          Smallint NULL ,
        titre         Varchar (100) NOT NULL ,
        quantite      Decimal NOT NULL ,
        unite         Varchar (10) NOT NULL ,
        prix_unitaire Float NOT NULL 
	,CONSTRAINT devis_prestations_PK PRIMARY KEY (id)

	,CONSTRAINT devis_prestations_devis_FK FOREIGN KEY (id_devis) REFERENCES devis(id)
)ENGINE=InnoDB;

-- création d'une nouvelle table: configuration pour la section facturation
CREATE TABLE configuration(
        id                 Int  Auto_increment  NOT NULL ,
        cle  Varchar (100) NOT NULL ,
        valeur Varchar (600) NOT NULL 
	,CONSTRAINT configuration_PK PRIMARY KEY (id)
)

-- ajouter deux colonnes is_client et is_employe dans la table users
ALTER TABLE `users` ADD `is_client` TINYINT(1) NOT NULL AFTER `is_super_admin`;
ALTER TABLE `users` ADD `is_employe` TINYINT(1) NOT NULL AFTER `is_super_admin`;

-- ajouter l'id clients dans la table users
ALTER TABLE `users` ADD `id_clients` INT(11) NOT NULL AFTER `id`;
ALTER TABLE `users`
  ADD KEY `users_clients_FK` (`id_clients`);

-- ajouter une colonne commentaire de l'admin pour la fiche projet
ALTER TABLE `projets` ADD `commentaire_admin` TEXT NULL AFTER `commentaire_maj`;

-- ajouter une colonne commentaire de l'admin pour la fiche client
ALTER TABLE `clients` ADD `commentaire_admin` TEXT NULL AFTER `id_prospect`;

-- ajouter une colonne commentaire interne pour la fiche suivi temps
ALTER TABLE `suivi_temps` ADD `commentaire_interne` TEXT NULL AFTER `user`;
