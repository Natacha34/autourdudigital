 
$( document ).ready(function() {
	
	/* Autocomplétion du champs de recherche 
		- Module SUIVI & ETABLISSEMENT & PRESTATAIRES  */
		
	//si c'est le module des adhérents
	if( $("#rechercheAllAdh") ) { 
		var chemin =base_URL+ "/searchAjaxAdh";
		getModuleRecherche($("#rechercheAllAdh"),chemin, $("#filtreAdh"));
	} 
	if( $("#rechercheNomAdh") ) { //si seulement nom prenom
		var chemin =base_URL+ "/searchAjaxAdhPrecis/adh_nom";
		getModuleRecherche($("#rechercheNomAdh"),chemin, $("#filtreAdh"));
	} 
	 if( $("#rechercheRegionPays") ) { //Module de suivi feuille facture
        var chemin = base_URL+ "/searchAjaxAdhPrecis/id_region";
        getModuleRecherche($("#rechercheRegionPays"),chemin, $("#formRegion"), $("#idRechercheRegionPays"));
    }
	/* Calendrier */
	if ($("#date_derniere_modif")) {
		$( function() {
			//var dateFormat = "MM yyyy",
			
			/*from = $( "#date_derniere_modif" ).datepicker({
					changeMonth: true,
					changeYear: true,
					clearBtn: true,
					numberOfMonths: 1,
					todayHighlight: true,
					language: "fr",
					onSelect: function(dateText, inst){ 
			            $("#date_derniere_modif").val(dateText); 
						//$("#formAdh").submit();
			        }
					
				})
				.on( "change", function() {
					from.datepicker( "option", "maxDate", getDate( this ) );
				});
			function getDate( element ) {
				var date;
				try {
					date = $.datepicker.parseDate( dateFormat, element.value );
				} catch( error ) {
					date = null;
				}
				return date;
			} */
			
				// Pour la partie Suivi temps
				$( function() {
					$( "#date_commencement" ).datepicker();
				} );
				
				$("#date_derniere_modif" ).datepicker({
					changeMonth: true,
					changeYear: true,
					dateFormat: 'dd MM yy',
					showButtonPanel: true,
					onClose: function() {
						var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
						var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
						$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
					},
					beforeShow: function() {
						if ((selDate = $(this).val()).length > 0) {
							iYear = selDate.substring(selDate.length - 4, selDate.length);
							iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5),
													$(this).datepicker('option', 'monthNames'));
							$(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
							$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
						}
					}
				});
				//console.log($("#date_derniere_modif"));
				if ($("#idAdherent").val() == '0') {
					$("#date_derniere_modif").datepicker('setDate', new Date());
				}
						
		} );
	}
	
	/*Alerte Doublon */
	$(document).on('keyup', '#email', function() {
		var idAdh		= $("#idAdherent").val();
		var emailAdh	= $("#email").val();
		var colorN		= $("#email");
		
		if (emailAdh.length <= 2) { 
			$("#emailAdhErreur").show().html("");
			$("#emailAdhSpinner").show().html("");
			colorN.css("color", "initial");
			return;
		}
		
		colorN.css("color", "orange");
		$("#emailAdhSpinner").show().html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
		
		$.ajax({
			type: "POST",
			url: base_URL+"/getAlertAdhEmail",
			data: {
				idAdh : idAdh,
				emailAdh : emailAdh
				},
			//dataType: 'html',
			success: function(reponse)
				{
					console.log(reponse);
					if (!reponse) {
						$("#emailAdhSpinner").show().html("");
						$("#emailAdhErreur").show().html("");
						colorN.css("color", "green");
					} else {
						$("#emailAdhSpinner").show().html("");
						$("#emailAdhErreur").show().html("Email déjà utilisé !");
						colorN.css("color", "red");
					}
				},
			error: function(reponse, textStatus, errorThrown)
				{
					// Handle errors here
					console.log('ERRORS: ' + textStatus);
				}
		});

	});
	
	$(document).on('keyup', '#nom', function() {
		checkNomPrenom();
	});
	$(document).on('keyup', '#prenom', function() {
		checkNomPrenom();
	});
	
	function checkNomPrenom() {
		var idAdh		= $("#idAdherent").val();
		var nomAdh		= $("#nom").val();
		var prenomAdh	= $("#prenom").val();
		var colorN		= $("#nom");
		var colorP		= $("#prenom");
		
		if (nomAdh.length <= 2 || prenomAdh.length <= 2) { 
			$("#nomAdhErreur").show().html("");
			$("#nomAdhSpinner").show().html("");
			colorN.css("color", "initial");
			colorP.css("color", "initial");
			return;
		}
		
		colorN.css("color", "orange");
		colorP.css("color", "orange");
		$("#nomAdhSpinner").show().html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
		
		$.ajax({
			type: "POST",
			url: base_URL+"/getAlertAdhNom",
			data: {
				idAdh : idAdh,
				nomAdh : nomAdh,
				prenomAdh : prenomAdh
				},
			//dataType: 'html',
			success: function(reponse)
				{
					console.log(reponse);
					if (!reponse) {
						$("#nomAdhSpinner").show().html("");
						$("#nomAdhErreur").show().html("");
						colorN.css("color", "green");
						colorP.css("color", "green");
					} else {
						$("#nomAdhSpinner").show().html("");
						$("#nomAdhErreur").show().html("Nom déjà utilisé !");
						colorN.css("color", "red");
						colorP.css("color", "red");
					}
				},
			error: function(reponse, textStatus, errorThrown)
				{
					// Handle errors here
					console.log('ERRORS: ' + textStatus);
				}
		});

	}

});



function getModuleRecherche(champsAutomatique, chemin, formAutomatique, champsIdAutomatique=null) {
	//console.log(champsAutomatique);
	champsAutomatique.autocomplete({ //selection du module de recherche

		
		source: function(request, response) {
			$.ajax({
				url: chemin ,
				type: 'POST',
				dataType: 'json',
				data: {
					// term: request.term
					recherche: champsAutomatique.val(),
					maxRows: 10
				},
				success: function(lstRecherche, textStatus, jqXHR) 
				{
					response($.map(lstRecherche, function (object) {
						return object; //récupère le label au lieu de value
					}));

				},
				 error: function(jqXHR, textStatus, errorThrown)
				{
					// Handle errors here
					console.log('ERRORS: ' + textStatus);
				}
			});
		},
		position: { //position de la liste
			my : 'left top',
			at : 'bottom',
			collision : "flipfit"
		}, 
		minLength: 1, //taille à partir de quand on recherche
		select: function(event, ui) { 
			
			if (champsIdAutomatique) {
				champsIdAutomatique.val(ui.item.id);
				champsAutomatique.val(ui.item.value);
			} else {
				champsAutomatique.val(ui.item.value); //Recupère le label et le garde inscrit
				formAutomatique.submit(); //envoi du formulaire dès que l'on clique sur un élément de liste
			}
		}
	});

	
}
